package edu.mst.util;

public abstract class Constants {

    public static final int uninitializedIntVal = -1;
    public static final int defaultThreadCallablesBatchSize = 24;
    public static final char tabChar = '\t';
    public static final String tabStr = "\t";
    public static final char commaChar = ',';
    public static final String commaStr = ",";
    public static final char spaceChar = ' ';
    public static final char semiColonChar = ';';
    public static final String semiColonStr = ";";
    public static final char periodChar = '.';
    public static final char questionChar = '?';
    public static final String nullText = "null";
    public static final String DIR_DELIM_LINUX = "/";
    public static final char fileExtenDelim = '.';

}
