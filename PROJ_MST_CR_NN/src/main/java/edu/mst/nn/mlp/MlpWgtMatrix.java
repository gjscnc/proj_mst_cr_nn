/**
 * 
 */
package edu.mst.nn.mlp;

import org.nd4j.linalg.api.ndarray.INDArray;

import edu.mst.util.Constants;

/**
 * @author George
 *
 */
public class MlpWgtMatrix implements Comparable<MlpWgtMatrix> {

    private int layerNbr = Constants.uninitializedIntVal;
    private MlpWgtType type = null;
    private INDArray wgtMatrix = null;
    private boolean isBias = false;

    @Override
    public int compareTo(MlpWgtMatrix matrix) {
	int i = Integer.compare(layerNbr, matrix.layerNbr);
	if (i != 0)
	    return i;
	return type.compareTo(matrix.type);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof MlpWgtMatrix) {
	    MlpWgtMatrix matrix = (MlpWgtMatrix) obj;
	    return layerNbr == matrix.layerNbr && type == matrix.type;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(layerNbr) + 31 * type.hashCode();
    }

    public int getLayerNbr() {
	return layerNbr;
    }

    public void setLayerNbr(int layerNbr) {
	this.layerNbr = layerNbr;
    }

    public MlpWgtType getType() {
	return type;
    }

    public void setType(MlpWgtType type) {
	this.type = type;
    }

    public INDArray getWgtMatrix() {
	return wgtMatrix;
    }

    public void setWgtMatrix(INDArray wgtMatrix) {
	this.wgtMatrix = wgtMatrix;
    }

    public boolean isBias() {
        return isBias;
    }

    public void setBias(boolean isBias) {
        this.isBias = isBias;
    }

}
