/**
 * 
 */
package edu.mst.nn.mlp;

import java.util.Random;

import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.eval.Evaluation;
//import org.nd4j.evaluation.classification.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import edu.mst.conceptrecogn.train.BaseTrainingData;

/**
 * Abstract MLP common to any MLP application.
 * <p>
 * Extend this class when instantiating a MLP for a specific application. If an
 * override of the default configuration is desired, do this when instantiating
 * the child class that implements this class, and/or override the
 * {@link #init()} method in this abstract class.
 * </p>
 * 
 * @author George
 *
 */
public abstract class BaseMlp {

    public static final int defaultNbrHiddenNodes = 10;
    public static final int defaultNbrHiddenLayers = 1;
    public static final int defaultNbrEpochs = 500;
    public static final int defaultMaxEpochsScoreImprovementTerminationCondition = 50;
    public static final double defaultScoreImprovementTerminationCondition = 0.001;
    public static final int defaultSeed = 123;
    public static final Random defaultRng = new Random(defaultSeed);
    public static final double defaultLearningRate = 0.01d;
    public static final double defaultMomentum = 0.9;
    public static final int defaultBatchSize = 100;
    public static final double defaultTargetMSE = 0.50d;
    public static final Activation defaultHiddenLayersActivationType = Activation.SIGMOID;
    public static final Activation defaultOutputLayerActivationType = Activation.SOFTMAX;
    public static final OptimizationAlgorithm defaultOptimizationAlgorithm = OptimizationAlgorithm.CONJUGATE_GRADIENT;
    public static final Updater defaultUpdater = Updater.NESTEROVS;
    public static final LossFunctions.LossFunction defaultLossFunction = LossFunctions.LossFunction.MSE;
    public static final WeightInit defaultWeightInitializer = WeightInit.XAVIER;
    public static final double defaultBiasInit = 0.0d;
    public static final boolean defaultUseMiniBatch = true;
    public static final int defaultIterations = 1;
    public static final boolean defaultTrainMultEpochs = true;

    protected int seed = defaultSeed;
    protected Random rng = defaultRng;
    protected double biasInit = defaultBiasInit;
    protected boolean useMiniBatch = defaultUseMiniBatch;
    protected int iterations = defaultIterations;
    protected double learningRate = defaultLearningRate;
    protected double momentum = defaultMomentum;
    protected int batchSize = defaultBatchSize;
    protected int nbrEpochs = defaultNbrEpochs;
    protected int maxEpochsScoreImprovementTerminationCondition = defaultMaxEpochsScoreImprovementTerminationCondition;
    protected double scoreImprovementTerminationCondition = defaultScoreImprovementTerminationCondition;
    protected double targetMSE = defaultTargetMSE;
    protected int scorePrintInterval = 250;
    protected Activation hiddenLayersActivationFunction = defaultHiddenLayersActivationType;
    protected Activation outputLayerActivationFunction = Activation.IDENTITY;
    protected OptimizationAlgorithm optimizationAlgorithm = defaultOptimizationAlgorithm;
    protected Updater updater = defaultUpdater;
    protected LossFunctions.LossFunction lossFunction = defaultLossFunction;
    protected WeightInit weightInitializer = defaultWeightInitializer;

    protected int nbrInputs = 0;
    protected int nbrOutputs = 0;
    protected int nbrHiddenNodes = 0;

    protected Evaluation trainEvaluation = null;
    protected Evaluation testEvaluation = null;

    protected MultiLayerConfiguration conf = null;
    protected EarlyStoppingTrainer trainer = null;
    protected MultiLayerNetwork mlp = null;
    protected MultiLayerNetwork bestMlpAllSlices = null;

    protected String[] columnNames = null;
    
    /**
     * Initialize the neural network - instantiate configuration file and the
     * neural net.
     * <p>
     * MLP is configured to have one hidden layer. Modify the MLP architecture
     * by overriding this method.
     * </p>
     */
    public void init() {

	conf = new NeuralNetConfiguration.Builder().seed(seed)
		.optimizationAlgo(optimizationAlgorithm)
		.updater(new Nesterovs(learningRate, 0.98))
		.l2(learningRate * 0.005) // regularize learning model
		.weightInit(weightInitializer).list()
		.layer(0, new DenseLayer.Builder().nIn(nbrInputs)
			.nOut(nbrHiddenNodes)
			.activation(hiddenLayersActivationFunction).build())
		.layer(1, new DenseLayer.Builder().nIn(nbrHiddenNodes)
			.nOut(nbrHiddenNodes)
			.activation(hiddenLayersActivationFunction).build())
		.layer(2,
			new OutputLayer.Builder(lossFunction)
				.weightInit(weightInitializer)
				.activation(outputLayerActivationFunction)
				.nIn(nbrHiddenNodes).nOut(nbrOutputs).build())
		.build();

	mlp = new MultiLayerNetwork(conf);
	mlp.init();

	/*
	 * Print score after parameter updates. Interval for printing parameters
	 * is per specified printing interval
	 */
	// mlp.setListeners(new ScoreIterationListener(scorePrintInterval));
    }

    /**
     * Marshal columns names into array, used as reference when converting MLP
     * data into form suitable for saving in SPS database.
     * @throws Exception 
     */
    protected abstract void marshalColumnNames() throws Exception;

    /**
     * Method for training the MLP.
     * <p>
     * The parameter for training data should extend {@link BaseTrainingData}.
     * The code that implements this abstract method
     * {@link #train(BaseTrainingData)} will include casting
     * {@link BaseTrainingData} to the class used to marshal training data.
     * </p>
     * 
     * @param trainingData
     * @throws Exception
     */
    public abstract void train(BaseTrainingData trainingData) throws Exception;

    public int getSeed() {
	return seed;
    }

    public Random getRng() {
	return rng;
    }

    public double getBiasInit() {
	return biasInit;
    }

    public boolean isUseMiniBatch() {
	return useMiniBatch;
    }

    public int getNbrIterPerEpoch() {
	return iterations;
    }

    public double getLearningRate() {
	return learningRate;
    }

    public double getMomentum() {
	return momentum;
    }

    public int getBatchSize() {
	return batchSize;
    }

    public int getNbrEpochs() {
	return nbrEpochs;
    }

    public double getTargetMSE() {
	return targetMSE;
    }

    public int getScorePrintInterval() {
	return scorePrintInterval;
    }

    public Activation getHiddenLayersActivationFunction() {
	return hiddenLayersActivationFunction;
    }

    public Activation getOutputLayerActivationFunction() {
	return outputLayerActivationFunction;
    }

    public OptimizationAlgorithm getOptimizationAlgorithm() {
	return optimizationAlgorithm;
    }

    public Updater getUpdater() {
	return updater;
    }

    public LossFunctions.LossFunction getLossFunction() {
	return lossFunction;
    }

    public WeightInit getWeightInitializer() {
	return weightInitializer;
    }

    public int getNumInputs() {
	return nbrInputs;
    }

    public int getNumOutputs() {
	return nbrOutputs;
    }

    public int getNumHiddenNodes() {
	return nbrHiddenNodes;
    }

    public MultiLayerConfiguration getConf() {
	return conf;
    }

    public MultiLayerNetwork getMlp() {
	return mlp;
    }

    public Evaluation getTrainEvaluation() {
	return trainEvaluation;
    }

    public Evaluation getTestEvaluation() {
	return testEvaluation;
    }

    public String[] getColumnNames() {
	return columnNames;
    }

}
