/**
 * 
 */
/**
 * Components for persisting and retrieving training results including neural
 * network weights.
 * 
 * @author gjs
 *
 */
package edu.mst.nn.mlp.data;