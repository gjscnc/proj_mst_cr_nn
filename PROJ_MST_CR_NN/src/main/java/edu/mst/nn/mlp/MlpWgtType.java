/**
 * 
 */
package edu.mst.nn.mlp;

/**
 * Used for identifying the weight type for weights stored in the SPS database,
 * i.e, is it a bias weight or a neural connection weight.
 * 
 * @author George
 *
 */
public enum MlpWgtType {
    W, b
}
