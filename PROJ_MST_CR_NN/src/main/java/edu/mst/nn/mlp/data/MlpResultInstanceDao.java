/**
 * 
 */
package edu.mst.nn.mlp.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.nn.mlp.MlpResultInstance;

/**
 * @author gjs
 *
 */
public class MlpResultInstanceDao {

    public static final String allFields = "featureCombo, sourceCorpus, isOutputSingleNeuron, datetime, "
	    + "pmid, tagId, isTruePositive, isFalsePositive, isTrueNegative, isFalseNegative, conceptNameUid, conceptUid, trainingDataId";

    public static final String tableName = "conceptrecogn.mlp_results_byinstance";

    public static final String deleteExistingSql = "truncate table "
	    + tableName;

    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") values(?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String marshalAllSql = "select " + allFields + " from "
	    + tableName;

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteExistingSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(MlpResultInstance instance, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(persistStmt, instance);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<MlpResultInstance> instances,
	    JdbcConnectionPool connPool) throws Exception {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		Iterator<MlpResultInstance> instanceIter = instances.iterator();
		while (instanceIter.hasNext()) {
		    MlpResultInstance instance = instanceIter.next();
		    setPersistValues(persistStmt, instance);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(PreparedStatement persistStmt,
	    MlpResultInstance instance) throws SQLException {
	/*
	 * 1-featureCombo, 2-sourceCorpus, 3-isOutputSingleNeuron, 4-datetime,
	 * 5-pmid, 6-tagId, 7-isTruePositive, 8-isFalsePositive,
	 * 9-isTrueNegative, 10-isFalseNegative, 11-conceptNameUid,
	 * 12-conceptUid, 13-trainingDataId
	 */
	persistStmt.setString(1, instance.getFeatureCombo());
	persistStmt.setString(2, instance.getSourceCorpus());
	persistStmt.setBoolean(3, instance.isOutputSingleNeuron());
	persistStmt.setString(4, instance.getDatetime());
	persistStmt.setInt(5, instance.getPmid());
	persistStmt.setString(6, instance.getTagId());
	persistStmt.setBoolean(7, instance.isTruePositive());
	persistStmt.setBoolean(8, instance.isFalsePositive());
	persistStmt.setBoolean(9, instance.isTrueNegative());
	persistStmt.setBoolean(10, instance.isFalseNegative());
	persistStmt.setLong(11, instance.getConceptNameUid());
	persistStmt.setLong(12, instance.getConceptUid());
	persistStmt.setInt(13, instance.getTrainingDataId());
    }

    public SortedSet<MlpResultInstance> marshal(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<MlpResultInstance> instances = new TreeSet<MlpResultInstance>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalAllSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			MlpResultInstance instance = instantiateFromResultSet(
				rs);
			instances.add(instance);
		    }
		}
	    }
	}
	return instances;
    }

    private MlpResultInstance instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	MlpResultInstance instance = new MlpResultInstance();
	/*
	 * 1-featureCombo, 2-sourceCorpus, 3-isOutputSingleNeuron, 4-datetime,
	 * 5-pmid, 6-tagId, 7-isTruePositive, 8-isFalsePositive,
	 * 9-isTrueNegative, 10-isFalseNegative, 11-conceptNameUid,
	 * 12-conceptUid, 13-trainingDataId
	 */
	instance.setFeatureCombo(rs.getString(1));
	instance.setSourceCorpus(rs.getString(2));
	instance.setOutputSingleNeuron(rs.getBoolean(3));
	instance.setDatetime(rs.getString(4));
	instance.setPmid(rs.getInt(5));
	instance.setTagId(rs.getString(6));
	instance.setTruePositive(rs.getBoolean(7));
	instance.setFalsePositive(rs.getBoolean(8));
	instance.setTrueNegative(rs.getBoolean(9));
	instance.setFalseNegative(rs.getBoolean(10));
	instance.setConceptNameUid(rs.getLong(11));
	instance.setConceptUid(rs.getLong(12));
	instance.setTrainingDataId(rs.getInt(13));
	return instance;
    }

}
