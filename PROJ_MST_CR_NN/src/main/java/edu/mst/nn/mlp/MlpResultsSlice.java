/**
 * 
 */
package edu.mst.nn.mlp;

import java.util.Comparator;

/**
 * MLP training results for one slice of data.
 * 
 * @author gjs
 *
 */
public class MlpResultsSlice extends MlpResults {

    private int sliceNbr = 0;
    private int nTrain = 0;

    public static class Comparators {
	public static final Comparator<MlpResultsSlice> byFeatureCombo = (
		MlpResultsSlice slice1,
		MlpResultsSlice slice2) -> slice1.featureCombo
			.compareTo(slice2.featureCombo);
	public static final Comparator<MlpResultsSlice> bySliceNbr = (
		MlpResultsSlice slice1, MlpResultsSlice slice2) -> Integer
			.compare(slice1.sliceNbr, slice2.sliceNbr);
	public static final Comparator<MlpResultsSlice> byFeatureCombo_Slice = (
		MlpResultsSlice slice1,
		MlpResultsSlice slice2) -> byFeatureCombo
			.thenComparing(bySliceNbr).compare(slice1, slice2);
    }

    /**
     * Compares fields used in database keys.
     * 
     */
    @Override
    public int compareTo(MlpResults mlpResult) {
	return Comparators.byFeatureCombo_Slice.compare(this,
		(MlpResultsSlice) mlpResult);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof MlpResultsSlice) {
	    MlpResultsSlice slice = (MlpResultsSlice) obj;
	    return super.equals(slice) && sliceNbr == slice.sliceNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return super.hashCode() + 31 * Integer.hashCode(sliceNbr);
    }

    public int getSliceNbr() {
	return sliceNbr;
    }

    public void setSliceNbr(int sliceNbr) {
	this.sliceNbr = sliceNbr;
    }

    public int getNTrain() {
	return nTrain;
    }

    public void setNTrain(int nTrain) {
	this.nTrain = nTrain;
    }

}
