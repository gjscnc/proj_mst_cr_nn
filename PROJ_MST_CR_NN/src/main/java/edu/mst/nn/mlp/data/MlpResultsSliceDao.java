/**
 * 
 */
package edu.mst.nn.mlp.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.nn.mlp.MlpResultsSlice;

/**
 * Results for each slice of test data in training collection.
 * 
 * @author gjs
 *
 */
public class MlpResultsSliceDao {

    public static final String allFields = "featureCombo, sourceCorpus, isOutputSingleNeuron, datetime, sliceNbr, "
    	+ "p, r, f1, a, mcc, nTrain, nTest, tp, fn, fp, tn";

    public static final String tableName = "conceptrecogn.mlp_results_byslice";

    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String lastTimestampCorpusSql = "select max(datetime) from "
	    + tableName + " where sourceCorpus = ?";

    public static final String lastTimestampCorpusFeatureComboSql = lastTimestampCorpusSql
	    + " and featureCombo=?";

    public static final String marshalCorpusSql = "select " + allFields
	    + " from " + tableName
	    + " where sourceCorpus = ? and datetime = ? order by featureCombo, sliceNbr";

    public static final String marshalCorpusFeatureComboSql = "select "
	    + allFields + " from " + tableName
	    + " where sourceCorpus = ? and featureCombo=? and datetime = ? "
	    + " order by featureCombo, sliceNbr";

    public static final String marshalAllSql = "select " + allFields + " from "
	    + tableName + " order by featureCombo, sliceNbr";

    public void persist(MlpResultsSlice slice, JdbcConnectionPool connPool)
	    throws SQLException, Exception {
	/*
	 * 1-featureCombo, 2-sourceCorpus, 3-isOutputSingleNeuron, 4-datetime,
	 * 5-sliceNbr, 6-p, 7-r, 8-f1, 9-a, 10-mcc, 11-nTrain, 12-nTest, 13-tp,
	 * 14-fn, 15-fp, 16-tn
	 */
	if (slice.getAccuracy() == Double.NaN || slice.getF1() == Double.NaN
		|| slice.getMcc() == Double.NaN
		|| slice.getPrecision() == Double.NaN
		|| slice.getRecall() == Double.NaN) {
	    return;
	}
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		persistStmt.setString(1, slice.getFeatureCombo());
		persistStmt.setString(2, slice.getSourceCorpus().toString());
		persistStmt.setBoolean(3, slice.isOutputSingleNeuron());
		persistStmt.setString(4, slice.getDatetime());
		persistStmt.setInt(5, slice.getSliceNbr());
		persistStmt.setDouble(6, slice.getPrecision());
		persistStmt.setDouble(7, slice.getRecall());
		persistStmt.setDouble(8, slice.getF1());
		persistStmt.setDouble(9, slice.getAccuracy());
		persistStmt.setDouble(10, slice.getMcc());
		persistStmt.setInt(11, slice.getNTrain());
		persistStmt.setInt(12, slice.getNTest());
		persistStmt.setInt(13, slice.getTp());
		persistStmt.setInt(14, slice.getFn());
		persistStmt.setInt(15, slice.getFp());
		persistStmt.setInt(16, slice.getTn());
		try {
		    persistStmt.executeUpdate();
		} catch (SQLException sqlEx) {
		    String errMsg = String.format(
			    "SQL error persisting slice: corpus = '%1$s', "
				    + "datetime = '%2$s', slice nbr = '%3$d'",
			    slice.getSourceCorpus(), slice.getDatetime(),
			    slice.getSliceNbr());
		    throw new SQLException(errMsg, sqlEx);
		} catch (Exception ex) {
		    String errMsg = String.format(
			    "Error persisting slice: corpus = '%1$s', "
				    + "datetime = '%2$s', slice nbr = '%3$d'",
			    slice.getSourceCorpus(), slice.getDatetime(),
			    slice.getSliceNbr());
		    throw new Exception(errMsg, ex);
		}
	    }
	}
    }

    public List<MlpResultsSlice> marshalLast(Corpus sourceCorpus,
	    JdbcConnectionPool connPool) throws SQLException, Exception {

	String lastDateTime = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement lastTimestampStmt = conn
		    .prepareStatement(lastTimestampCorpusSql)) {

		lastTimestampStmt.setString(1, sourceCorpus.toString());
		try (ResultSet rs = lastTimestampStmt.executeQuery()) {
		    if (rs.next()) {
			lastDateTime = rs.getString(1);
		    }
		}
	    }
	}

	return marshal(sourceCorpus, lastDateTime, connPool);
    }

    public List<MlpResultsSlice> marshalLast(FeatureCombinationInputs featureCombo,
	    Corpus sourceCorpus, JdbcConnectionPool connPool)
	    throws SQLException {

	String lastDateTime = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement lastTimestampStmt = conn
		    .prepareStatement(lastTimestampCorpusFeatureComboSql)) {

		lastTimestampStmt.setString(1, sourceCorpus.toString());
		lastTimestampStmt.setString(2, featureCombo.toString());
		try (ResultSet rs = lastTimestampStmt.executeQuery()) {
		    if (rs.next()) {
			lastDateTime = rs.getString(1);
		    }
		}
	    }
	}

	return marshal(featureCombo, sourceCorpus, lastDateTime, connPool);
    }

    public List<MlpResultsSlice> marshal(FeatureCombinationInputs featureCombo,
	    Corpus sourceCorpus, String datetime, JdbcConnectionPool connPool)
	    throws SQLException {
	List<MlpResultsSlice> slices = new ArrayList<MlpResultsSlice>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalCorpusFeatureComboSql)) {
		marshalStmt.setString(1, sourceCorpus.toString());
		marshalStmt.setString(2, featureCombo.toString());
		marshalStmt.setString(3, datetime);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			MlpResultsSlice slice = instantiateFromResultSet(rs);
			slices.add(slice);
		    }
		}
	    }
	}

	return slices;
    }

    public List<MlpResultsSlice> marshal(Corpus sourceCorpus, String datetime,
	    JdbcConnectionPool connPool) throws SQLException {
	List<MlpResultsSlice> slices = new ArrayList<MlpResultsSlice>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalCorpusSql)) {
		marshalStmt.setString(1, sourceCorpus.toString());
		marshalStmt.setString(2, datetime);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			MlpResultsSlice slice = instantiateFromResultSet(rs);
			slices.add(slice);
		    }
		}
	    }
	}

	return slices;
    }
    
    public List<MlpResultsSlice> marshal(JdbcConnectionPool connPool) throws SQLException{
	
	List<MlpResultsSlice> slices = new ArrayList<MlpResultsSlice>();
	
	try(Connection conn = connPool.getConnection()){
	    try(PreparedStatement marshalStmt = conn.prepareStatement(marshalAllSql)){
		try(ResultSet rs = marshalStmt.executeQuery()){
		    while(rs.next()) {
			MlpResultsSlice slice = instantiateFromResultSet(rs);
			slices.add(slice);
		    }
		}
	    }
	}
	
	slices.sort(MlpResultsSlice.Comparators.byFeatureCombo_Slice);
	
	return slices;
	
    }

    private MlpResultsSlice instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-featureCombo, 2-sourceCorpus, 3-isOutputSingleNeuron, 4-datetime,
	 * 5-sliceNbr, 6-p, 7-r, 8-f1, 9-a, 10-mcc, 11-nTrain, 12-nTest, 13-tp,
	 * 14-fn, 15-fp, 16-tn
	 */
	MlpResultsSlice slice = new MlpResultsSlice();
	slice.setFeatureCombo(rs.getString(1));
	slice.setSourceCorpus(Corpus.valueOf(rs.getString(2)));
	slice.setOutputSingleNeuron(rs.getBoolean(3));
	slice.setDatetime(rs.getString(4));
	slice.setSliceNbr(rs.getInt(5));
	slice.setPrecision(rs.getDouble(6));
	slice.setRecall(rs.getDouble(7));
	slice.setF1(rs.getDouble(8));
	slice.setAccuracy(rs.getDouble(9));
	slice.setMcc(rs.getDouble(10));
	slice.setNTrain(rs.getInt(11));
	slice.setNTest(rs.getInt(12));
	slice.setTp(rs.getInt(13));
	slice.setFn(rs.getInt(14));
	slice.setFp(rs.getInt(15));
	slice.setTn(rs.getInt(16));
	return slice;
    }

}
