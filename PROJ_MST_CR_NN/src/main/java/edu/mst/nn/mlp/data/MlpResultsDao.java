/**
 * 
 */
package edu.mst.nn.mlp.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.nn.mlp.MlpResults;
import edu.mst.nn.mlp.MlpResultsSlice;

/**
 * Data access object for MLP test results.
 * 
 * @author gjs
 *
 */
public class MlpResultsDao {

    public static final String allFields = "featureCombo, sourceCorpus, isOutputSingleNeuron, datetime, "
    	+ "p, r, f1, a, mcc, nTest, tp, fn, fp, tn";

    public static final String tableName = "conceptrecogn.mlp_results";

    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String marshalSql = "select " + allFields + " from "
	    + tableName
	    + " where featureCombo=? and sourceCorpus = ? and datetime = ?";

    public static final String marshalAllSql = "select " + allFields + " from "
	    + tableName;

    public static final String latestTimestampSql = "select max(datetime) from "
	    + tableName + "where featureCombo=? and sourceCorpus = ?";

    public void persist(MlpResults mlpResults, JdbcConnectionPool connPool)
	    throws SQLException, Exception {

	if (mlpResults.getAccuracy() == Double.NaN
		|| mlpResults.getF1() == Double.NaN
		|| mlpResults.getMcc() == Double.NaN
		|| mlpResults.getPrecision() == Double.NaN
		|| mlpResults.getRecall() == Double.NaN) {
	    return;
	}

	/*
	 * 1-featureCombo, 2-sourceCorpus, 3-isOutputSingleNeuron, 4-datetime,
	 * 5-p, 6-r, 7-f1, 8-a, 9-mcc, 10-nTest, 11-tp, 12-fn, 13-fp, 14-tn
	 */
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		persistStmt.setString(1, mlpResults.getFeatureCombo());
		persistStmt.setString(2,
			mlpResults.getSourceCorpus().toString());
		persistStmt.setBoolean(3, mlpResults.isOutputSingleNeuron());
		persistStmt.setString(4, mlpResults.getDatetime());
		persistStmt.setDouble(5, mlpResults.getPrecision());
		persistStmt.setDouble(6, mlpResults.getRecall());
		persistStmt.setDouble(7, mlpResults.getF1());
		persistStmt.setDouble(8, mlpResults.getAccuracy());
		persistStmt.setDouble(9, mlpResults.getMcc());
		persistStmt.setInt(10, mlpResults.getNTest());
		persistStmt.setInt(11, mlpResults.getTp());
		persistStmt.setInt(12, mlpResults.getFn());
		persistStmt.setInt(13, mlpResults.getFp());
		persistStmt.setInt(14, mlpResults.getTn());
		persistStmt.executeUpdate();
	    }
	}

    }

    public MlpResults marshalLatest(FeatureCombinationInputs featureCombo,
	    Corpus sourceCorpus, JdbcConnectionPool connPool)
	    throws SQLException, Exception {

	/*
	 * Use of the max function for the datetime string should work since the
	 * format of datetime in the schema is defined such that lexographic
	 * sort of datetime is consistent with the numeric date sort
	 */
	String lastDatetime = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement latestTimestampStmt = conn
		    .prepareStatement(latestTimestampSql)) {
		latestTimestampStmt.setString(1, featureCombo.toString());
		latestTimestampStmt.setString(2, sourceCorpus.toString());
		try (ResultSet rs = latestTimestampStmt.executeQuery()) {
		    if (rs.next()) {
			lastDatetime = rs.getString(1);
		    } else {
			return null;
		    }
		}
	    }
	}

	return marshal(featureCombo, sourceCorpus, lastDatetime, connPool);

    }

    public MlpResults marshal(FeatureCombinationInputs featureCombo,
	    Corpus sourceCorpus, String datetime, JdbcConnectionPool connPool)
	    throws SQLException, Exception {
	/*
	 * 1-featureCombo, 2-sourceCorpus, 3-datetime, 4-p, 5-r, 6-f1, 7-a,
	 * 8-mcc, 9-nTest, 10-tp, 11-fn, 12-fp, 13-tn
	 */
	MlpResults mlpResults = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getMlpResultsStmt = conn
		    .prepareStatement(marshalSql)) {
		getMlpResultsStmt.setString(1, featureCombo.toString());
		getMlpResultsStmt.setString(2, sourceCorpus.toString());
		getMlpResultsStmt.setString(3, datetime);
		try (ResultSet rs = getMlpResultsStmt.executeQuery()) {
		    if (rs.next()) {
			mlpResults = instantiateFromResultSet(rs);
		    }
		}
	    }
	}

	if (mlpResults == null) {
	    return null;
	}

	MlpResultsSliceDao sliceDao = new MlpResultsSliceDao();
	List<MlpResultsSlice> slices = sliceDao.marshal(sourceCorpus, datetime,
		connPool);
	mlpResults.setSlices(slices);

	return mlpResults;

    }

    public List<MlpResults> marshal(JdbcConnectionPool connPool)
	    throws SQLException, Exception {

	List<MlpResults> results = new ArrayList<MlpResults>();

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalAllSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			MlpResults result = instantiateFromResultSet(rs);
			results.add(result);
		    }
		}
	    }
	}

	return results;
    }
    
    private MlpResults instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-featureCombo, 2-sourceCorpus, 3-isOutputSingleNeuron, 4-datetime,
	 * 5-p, 6-r, 7-f1, 8-a, 9-mcc, 10-nTest, 11-tp, 12-fn, 13-fp, 14-tn
	 */
	MlpResults mlpResults = new MlpResults();
	mlpResults.setFeatureCombo(rs.getString(1));
	mlpResults.setSourceCorpus(Corpus.valueOf(rs.getString(2)));
	mlpResults.setOutputSingleNeuron(rs.getBoolean(3));
	mlpResults.setDatetime(rs.getString(4));
	mlpResults.setPrecision(rs.getDouble(5));
	mlpResults.setRecall(rs.getDouble(6));
	mlpResults.setF1(rs.getDouble(7));
	mlpResults.setAccuracy(rs.getDouble(8));
	mlpResults.setMcc(rs.getDouble(9));
	mlpResults.setNTest(rs.getInt(10));
	mlpResults.setTp(rs.getInt(11));
	mlpResults.setFn(rs.getInt(12));
	mlpResults.setFp(rs.getInt(13));
	mlpResults.setTn(rs.getInt(14));
	return mlpResults;
    }

}
