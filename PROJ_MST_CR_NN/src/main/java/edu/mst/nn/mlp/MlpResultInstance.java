/**
 * 
 */
package edu.mst.nn.mlp;

import java.util.Comparator;

/**
 * Test results for a training record.
 * 
 * @author gjs
 *
 */
public class MlpResultInstance implements Comparable<MlpResultInstance> {

    private String featureCombo;
    private String sourceCorpus;
    private boolean isOutputSingleNeuron;
    private String datetime;
    private int pmid;
    private String tagId;
    private boolean isTruePositive = false;
    private boolean isFalsePositive = false;
    private boolean isTrueNegative = false;
    private boolean isFalseNegative = false;
    private long conceptNameUid;
    private long conceptUid;
    private int trainingDataId;

    public static class Comparators {
	public static final Comparator<MlpResultInstance> byFeatureCombo = (
		MlpResultInstance instance1,
		MlpResultInstance instance2) -> instance1.featureCombo
			.compareTo(instance2.featureCombo);
	public static final Comparator<MlpResultInstance> bySourceCorpus = (
		MlpResultInstance instance1,
		MlpResultInstance instance2) -> instance1.sourceCorpus
			.compareTo(instance2.sourceCorpus);
	public static final Comparator<MlpResultInstance> byIsOutputSingleNeuron = (
		MlpResultInstance instance1,
		MlpResultInstance instance2) -> Boolean.compare(
			instance1.isOutputSingleNeuron,
			instance2.isOutputSingleNeuron);
	public static final Comparator<MlpResultInstance> byDateTime = (
		MlpResultInstance instance1,
		MlpResultInstance instance2) -> instance1.datetime
			.compareTo(instance2.datetime);
	public static final Comparator<MlpResultInstance> byFeature_Corpus_SingleNeuron_DateTime = (
		MlpResultInstance instance1,
		MlpResultInstance instance2) -> byFeatureCombo
			.thenComparing(bySourceCorpus)
			.thenComparing(byIsOutputSingleNeuron)
			.thenComparing(byDateTime)
			.compare(instance1, instance2);;
	public static final Comparator<MlpResultInstance> byTrainingDataId = (
		MlpResultInstance instance1,
		MlpResultInstance instance2) -> Integer.compare(
			instance1.trainingDataId, instance2.trainingDataId);
    }

    @Override
    public int compareTo(MlpResultInstance instance) {
	return Comparators.byFeature_Corpus_SingleNeuron_DateTime.compare(this,
		instance);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof MlpResultInstance) {
	    MlpResultInstance instance = (MlpResultInstance) obj;
	    return featureCombo.equals(instance.featureCombo)
		    && sourceCorpus.equals(instance.sourceCorpus)
		    && isOutputSingleNeuron == instance.isOutputSingleNeuron
		    && datetime.equals(instance.datetime);
	}
	return false;
    }

    @Override
    public int hashCode() {
	return featureCombo.hashCode() + 7 * sourceCorpus.hashCode()
		+ 31 * Boolean.hashCode(isOutputSingleNeuron)
		+ 47 * datetime.hashCode();
    }

    public String getFeatureCombo() {
	return featureCombo;
    }

    public void setFeatureCombo(String featureCombo) {
	this.featureCombo = featureCombo;
    }

    public String getSourceCorpus() {
	return sourceCorpus;
    }

    public void setSourceCorpus(String sourceCorpus) {
	this.sourceCorpus = sourceCorpus;
    }

    public boolean isOutputSingleNeuron() {
	return isOutputSingleNeuron;
    }

    public void setOutputSingleNeuron(boolean isOutputSingleNeuron) {
	this.isOutputSingleNeuron = isOutputSingleNeuron;
    }

    public String getDatetime() {
	return datetime;
    }

    public void setDatetime(String datetime) {
	this.datetime = datetime;
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public String getTagId() {
	return tagId;
    }

    public void setTagId(String tagId) {
	this.tagId = tagId;
    }

    public boolean isTruePositive() {
	return isTruePositive;
    }

    public void setTruePositive(boolean isTruePositive) {
	this.isTruePositive = isTruePositive;
    }

    public boolean isFalsePositive() {
	return isFalsePositive;
    }

    public void setFalsePositive(boolean isFalsePositive) {
	this.isFalsePositive = isFalsePositive;
    }

    public boolean isTrueNegative() {
	return isTrueNegative;
    }

    public void setTrueNegative(boolean isTrueNegative) {
	this.isTrueNegative = isTrueNegative;
    }

    public boolean isFalseNegative() {
	return isFalseNegative;
    }

    public void setFalseNegative(boolean isFalseNegative) {
	this.isFalseNegative = isFalseNegative;
    }

    public long getConceptNameUid() {
        return conceptNameUid;
    }

    public void setConceptNameUid(long conceptNameUid) {
        this.conceptNameUid = conceptNameUid;
    }

    public long getConceptUid() {
	return conceptUid;
    }

    public void setConceptUid(long conceptUid) {
	this.conceptUid = conceptUid;
    }

    public int getTrainingDataId() {
	return trainingDataId;
    }

    public void setTrainingDataId(int trainingDataId) {
	this.trainingDataId = trainingDataId;
    }

}
