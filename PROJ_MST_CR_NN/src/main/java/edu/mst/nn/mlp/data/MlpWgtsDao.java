/**
 * 
 */
package edu.mst.nn.mlp.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import edu.mst.conceptrecogn.ConceptRecognMlp;
import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureCombinationOutputs;
import edu.mst.conceptrecogn.FeatureName;
import edu.mst.conceptrecogn.RecognitionLevel;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.nn.mlp.MlpWgtData;
import edu.mst.nn.mlp.MlpWgtMatrix;
import edu.mst.nn.mlp.MlpWgtType;

/**
 * Data access object for persisting and retrieving MLP weight data to/from the
 * SPS database.
 * 
 * @author George
 *
 */
public class MlpWgtsDao {

    public static final String allFields = "layerNbr, rowNbr, colNbr, wgtType, wgt, sourceCorpus, featureCombo";

    public static final String tableName = "conceptrecogn.mlp_weights";

    public static final String deleteExisting = "delete from " + tableName
	    + " where sourceCorpus=? and featureCombo=?";

    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") values (?,?,?,?,?,?,?)";

    private Corpus corpus = null;
    private FeatureCombinationInputs featureComboInputs = null;
    private FeatureCombinationOutputs featureComboOutputs = null;
    private FeatureName[] orderedFeatureComboInputs = null;
    private String featureComboStr = null;
    private JdbcConnectionPool connPool = null;

    public MlpWgtsDao(Corpus corpus, FeatureName[] orderedFeatureComboInputs,
	    FeatureCombinationOutputs featureComboOutputs,
	    JdbcConnectionPool connPool) {
	this.corpus = corpus;
	this.orderedFeatureComboInputs = orderedFeatureComboInputs;
	this.featureComboStr = FeatureName
		.orderedFeatureComboToStr(orderedFeatureComboInputs);
	this.featureComboOutputs = featureComboOutputs;
	this.connPool = connPool;
    }

    public MlpWgtsDao(Corpus corpus,
	    FeatureCombinationInputs featureComboInputs,
	    FeatureCombinationOutputs featureComboOutputs,
	    JdbcConnectionPool connPool) {
	this.corpus = corpus;
	this.featureComboInputs = featureComboInputs;
	this.featureComboStr = featureComboInputs.toString();
	this.featureComboOutputs = featureComboOutputs;
	this.connPool = connPool;
    }

    public void persistMLP(MultiLayerNetwork mlp) throws Exception {

	System.out.printf("%n%nPersisting weights to database%n");

	MlpWgtData mlpData = marshalDataFromMlp(mlp);

	try (Connection conn = connPool.getConnection()) {
	    /*
	     * Delete existing records
	     */
	    try (PreparedStatement delWgtsStmt = conn
		    .prepareStatement(deleteExisting)) {
		delWgtsStmt.setString(1, corpus.toString());
		delWgtsStmt.setString(2, featureComboStr);
		int nbrDel = delWgtsStmt.executeUpdate();
		System.out.printf("Deleted %1$d existing weight records %n",
			nbrDel);
	    }
	    /*
	     * Persist weights
	     */
	    try (PreparedStatement insertWgtStmt = conn
		    .prepareStatement(persistSql)) {
		List<MlpWgtMatrix> wgtMatrices = mlpData.getWgtMatrices();
		for (MlpWgtMatrix wgtMatrix : wgtMatrices) {
		    int layerNbr = wgtMatrix.getLayerNbr();
		    MlpWgtType wgtType = wgtMatrix.getType();
		    INDArray wgtData = wgtMatrix.getWgtMatrix();
		    int nbrRows = wgtData.rows();
		    int nbrCol = wgtData.columns();
		    int nbrWgtsSaved = 0;
		    for (int row = 0; row < nbrRows; row++) {
			for (int col = 0; col < nbrCol; col++) {
			    double wgt = wgtData.getDouble(row, col);
			    insertWgtStmt.setInt(1, layerNbr);
			    insertWgtStmt.setInt(2, row);
			    insertWgtStmt.setInt(3, col);
			    insertWgtStmt.setString(4, wgtType.toString());
			    insertWgtStmt.setDouble(5, wgt);
			    insertWgtStmt.setString(6, corpus.toString());
			    insertWgtStmt.setString(7, featureComboStr);
			    insertWgtStmt.executeUpdate();
			    nbrWgtsSaved++;
			}
		    }
		    System.out.printf(
			    "Total of %1$d weights of type %2$s "
				    + "saved in layer #%3$d %n",
			    nbrWgtsSaved, wgtType.toString(), layerNbr);
		}
	    }
	}
    }

    /**
     * Retrieves and converts data in the MLP to a form suitable for persisting
     * in the SPS database.
     * 
     * @param mlp
     * @return
     */
    public MlpWgtData marshalDataFromMlp(MultiLayerNetwork mlp) {

	MlpWgtData mlpData = new MlpWgtData();

	Layer[] layers = mlp.getLayers();
	int nbrLayers = layers.length;

	for (int mlpLayerNbr = 0; mlpLayerNbr < nbrLayers; mlpLayerNbr++) {
	    Layer layer = layers[mlpLayerNbr];
	    Map<String, INDArray> paramMap = layer.paramTable();
	    for (String paramKey : paramMap.keySet()) {
		MlpWgtMatrix wgtMatrix = new MlpWgtMatrix();
		wgtMatrix.setLayerNbr(mlpLayerNbr);
		wgtMatrix.setType(MlpWgtType.valueOf(paramKey));
		wgtMatrix.setWgtMatrix(layer.getParam(paramKey));
		mlpData.getWgtMatrices().add(wgtMatrix);
	    }
	}

	return mlpData;
    }

    /**
     * 
     * @param featureComboInputs
     * @return
     * @throws Exception
     */
    public ConceptRecognMlp marshalMlpFromDatabase(
	    RecognitionLevel recognitionLevel)
	    throws Exception {

	ConceptRecognMlp mlp = null;
	if (featureComboInputs != null) {
	    mlp = new ConceptRecognMlp(featureComboInputs, featureComboOutputs,
		    recognitionLevel);
	} else if (orderedFeatureComboInputs != null) {
	    mlp = new ConceptRecognMlp(orderedFeatureComboInputs,
		    featureComboOutputs, recognitionLevel);
	} else {
	    throw new Exception("Feature names not provided");
	}

	mlp.init();

	List<Integer> layerNbrs = new ArrayList<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    String getLayerNbrsSql = "SELECT DISTINCT layerNbr FROM "
		    + tableName
		    + " where sourceCorpus=? and featureCombo=? and recognitionLevel=? ORDER BY layerNbr";
	    try (PreparedStatement getLayerNbrsStmt = conn
		    .prepareStatement(getLayerNbrsSql)) {
		getLayerNbrsStmt.setString(1, corpus.toString());
		getLayerNbrsStmt.setString(2, featureComboStr);
		getLayerNbrsStmt.setString(3, recognitionLevel.toString());
		try (ResultSet rs = getLayerNbrsStmt.executeQuery()) {
		    while (rs.next()) {
			layerNbrs.add(rs.getInt(1));
		    }
		}
	    }
	}
	if (layerNbrs.size() == 0) {
	    throw new Exception("No weight data found in database");
	}

	/*
	 * Populate collection of maps that contain the number of rows and
	 * columns for each layer and layer type
	 */
	Map<Integer, Map<String, Integer>> nbrOfRowByLayerAndType = new HashMap<Integer, Map<String, Integer>>();
	Map<Integer, Map<String, Integer>> nbrOfColByLayerAndType = new HashMap<Integer, Map<String, Integer>>();

	try (Connection conn = connPool.getConnection()) {
	    String getNbrRowsSql = "SELECT max(rowNbr) "
		    + "FROM (SELECT DISTINCT rowNbr FROM " + tableName
		    + " WHERE layerNbr = ? AND wgtType = ? "
		    + "AND sourceCorpus=? and featureCombo=? and recognitionLevel=?) AS r";
	    String getNbrColSql = "SELECT max(col) "
		    + "FROM (SELECT DISTINCT colNbr FROM " + tableName
		    + " WHERE layerNbr = ? AND wgtType = ? "
		    + "AND sourceCorpus=? and featureCombo=? and recognitionLevel=?) AS c";
	    try (PreparedStatement getNbrRowsStmt = conn
		    .prepareStatement(getNbrRowsSql)) {
		for (int layerNbr : layerNbrs) {
		    for (MlpWgtType type : MlpWgtType.values()) {
			getNbrRowsStmt.setInt(1, layerNbr);
			getNbrRowsStmt.setString(2, type.toString());
			getNbrRowsStmt.setString(3, corpus.toString());
			getNbrRowsStmt.setString(4, featureComboStr);
			getNbrRowsStmt.setString(5,
				recognitionLevel.toString());
			try (ResultSet rs = getNbrRowsStmt.executeQuery()) {
			    if (rs.next()) {
				int nbrRows = rs.getInt(1) + 1;
				if (!nbrOfRowByLayerAndType
					.containsKey(layerNbr)) {
				    nbrOfRowByLayerAndType.put(layerNbr,
					    new HashMap<String, Integer>());
				}
				nbrOfRowByLayerAndType.get(layerNbr)
					.put(type.toString(), nbrRows);
			    }
			}
		    }
		}
	    }
	    try (PreparedStatement getNbrColStmt = conn
		    .prepareStatement(getNbrColSql)) {
		for (int layerNbr : layerNbrs) {
		    for (MlpWgtType type : MlpWgtType.values()) {
			getNbrColStmt.setInt(1, layerNbr);
			getNbrColStmt.setString(2, type.toString());
			getNbrColStmt.setString(3, corpus.toString());
			getNbrColStmt.setString(4, featureComboStr);
			getNbrColStmt.setString(5, recognitionLevel.toString());
			try (ResultSet rs = getNbrColStmt.executeQuery()) {
			    if (rs.next()) {
				int nbrCol = rs.getInt(1) + 1;
				if (!nbrOfColByLayerAndType
					.containsKey(layerNbr)) {
				    nbrOfColByLayerAndType.put(layerNbr,
					    new HashMap<String, Integer>());
				}
				nbrOfColByLayerAndType.get(layerNbr)
					.put(type.toString(), nbrCol);
			    }
			}
		    }
		}
	    }
	}

	/*
	 * Populate collection of weight arrays to be used to marshal weights
	 * into the MLP neural network
	 */
	Map<Integer, Map<String, INDArray>> wgtByLayerAndType = new HashMap<Integer, Map<String, INDArray>>();

	try (Connection conn = connPool.getConnection()) {
	    String getWgtSql = "SELECT wgt FROM " + tableName
		    + " WHERE layerNbr = ? AND wgtType = ? "
		    + "AND rowNbr = ? AND colNbr = ? "
		    + "and sourceCorpus=? and featureCombo=? and recognitionLevel=?";
	    try (PreparedStatement getWgtStmt = conn
		    .prepareStatement(getWgtSql)) {
		for (int layerNbr : layerNbrs) {
		    if (!wgtByLayerAndType.containsKey(layerNbr)) {
			wgtByLayerAndType.put(layerNbr,
				new HashMap<String, INDArray>());
		    }
		    for (MlpWgtType type : MlpWgtType.values()) {
			int nbrRows = nbrOfRowByLayerAndType.get(layerNbr)
				.get(type.toString());
			int nbrCols = nbrOfColByLayerAndType.get(layerNbr)
				.get(type.toString());
			if (!wgtByLayerAndType.get(layerNbr)
				.containsKey(type.toString())) {
			    INDArray wgts = Nd4j
				    .zeros(new int[] { nbrRows, nbrCols });
			    wgtByLayerAndType.get(layerNbr).put(type.toString(),
				    wgts);
			}
			for (int rowNbr = 0; rowNbr < nbrRows; rowNbr++) {
			    for (int colNbr = 0; colNbr < nbrCols; colNbr++) {
				getWgtStmt.setInt(1, layerNbr);
				getWgtStmt.setString(2, type.toString());
				getWgtStmt.setInt(3, rowNbr);
				getWgtStmt.setInt(4, colNbr);
				getWgtStmt.setString(5, corpus.toString());
				getWgtStmt.setString(6, featureComboStr);
				getWgtStmt.setString(7,
					recognitionLevel.toString());
				try (ResultSet rs = getWgtStmt.executeQuery()) {
				    if (rs.next()) {
					double wgt = rs.getDouble(1);
					wgtByLayerAndType.get(layerNbr)
						.get(type.toString())
						.put(rowNbr, colNbr, wgt);
				    }
				}
			    }
			}
		    }
		}
	    }
	}

	/*
	 * Marshal weights into the MLP neural net
	 */
	Layer[] layers = mlp.getMlp().getLayers();
	for (int layerNbr = 0; layerNbr < layers.length; layerNbr++) {
	    Layer layer = layers[layerNbr];
	    for (MlpWgtType type : MlpWgtType.values()) {
		INDArray wgts = wgtByLayerAndType.get(layerNbr)
			.get(type.toString());
		layer.setParam(type.toString(), wgts);
	    }
	}

	return mlp;
    }

}
