/**
 * 
 */
package edu.mst.nn.mlp;

import java.util.ArrayList;
import java.util.List;

/**
 * @author George
 *
 */
public class MlpWgtData {

    private List<MlpWgtMatrix> wgtMatrices = new ArrayList<MlpWgtMatrix>();

    public List<MlpWgtMatrix> getWgtMatrices() {
        return wgtMatrices;
    }

    public void setWgtMatrices(List<MlpWgtMatrix> wgtMatrices) {
        this.wgtMatrices = wgtMatrices;
    }
    
}
