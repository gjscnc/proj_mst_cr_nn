/**
 * 
 */
package edu.mst.nn.mlp;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import edu.mst.db.nlm.corpora.Corpus;

/**
 * Results from testing the MLP.
 * <p>
 * The results are typically averaged across test slices (i.e.,
 * cross-validation). Results for a slice are instantiated in
 * {@link MlpResultsSlice};
 * </p>
 * 
 * @author gjs
 *
 */
public class MlpResults implements Comparable<MlpResults> {

    protected String featureCombo = null;
    protected Corpus sourceCorpus = null;
    protected boolean isOutputSingleNeuron = false;
    protected String datetime = null;
    protected double precision = 0.0d;
    protected double recall = 0.0d;
    protected double f1 = 0.0d;
    protected double accuracy = 0.0d;
    protected double mcc = 0.0d;
    protected int nTest = 0;
    protected int tp = 0;
    protected int fn = 0;
    protected int fp = 0;
    protected int tn = 0;
    protected List<MlpResultsSlice> slices = new ArrayList<MlpResultsSlice>();

    public static class Comparators {
	public static final Comparator<MlpResults> byFeatureCombo = (
		MlpResults result1, MlpResults result2) -> result1.featureCombo
			.compareTo(result2.featureCombo);
	public static final Comparator<MlpResults> bySingleNeuronOutput = (
		MlpResults result1, MlpResults result2) -> Boolean.compare(
			result1.isOutputSingleNeuron,
			result2.isOutputSingleNeuron);
	public static final Comparator<MlpResults> byFeatureCombo_isSingleNeuron = (
		MlpResults result1, MlpResults result2) -> byFeatureCombo
			.thenComparing(bySingleNeuronOutput)
			.compare(result1, result2);
	public static final Comparator<MlpResults> byDatetime = (
		MlpResults result1, MlpResults result2) -> result1.datetime
			.compareTo(result2.datetime);
    }

    @Override
    public int compareTo(MlpResults mlpResults) {
	return datetime.compareTo(mlpResults.datetime);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof MlpResults) {
	    MlpResults mlpResults = (MlpResults) obj;
	    return datetime.equals(mlpResults.datetime);
	}
	return false;
    }

    @Override
    public int hashCode() {
	return datetime.hashCode();
    }

    public String getFeatureCombo() {
	return featureCombo;
    }

    public void setFeatureCombo(String featureCombo) {
	this.featureCombo = featureCombo;
    }

    public Corpus getSourceCorpus() {
	return sourceCorpus;
    }

    public void setSourceCorpus(Corpus sourceCorpus) {
	this.sourceCorpus = sourceCorpus;
    }

    public boolean isOutputSingleNeuron() {
        return isOutputSingleNeuron;
    }

    public void setOutputSingleNeuron(boolean isOutputSingleNeuron) {
        this.isOutputSingleNeuron = isOutputSingleNeuron;
    }

    public String getDatetime() {
	return datetime;
    }

    public void setDatetime(String datetime) {
	this.datetime = datetime;
    }

    public double getPrecision() {
	return precision;
    }

    public void setPrecision(double precision) {
	this.precision = precision;
    }

    public double getRecall() {
	return recall;
    }

    public void setRecall(double recall) {
	this.recall = recall;
    }

    public double getF1() {
	return f1;
    }

    public void setF1(double f1) {
	this.f1 = f1;
    }

    public double getAccuracy() {
	return accuracy;
    }

    public void setAccuracy(double accuracy) {
	this.accuracy = accuracy;
    }

    public List<MlpResultsSlice> getSlices() {
	return slices;
    }

    public void setSlices(List<MlpResultsSlice> slices) {
	this.slices = slices;
    }

    public double getMcc() {
	return mcc;
    }

    public void setMcc(double mcc) {
	this.mcc = mcc;
    }

    public int getNTest() {
	return nTest;
    }

    public void setNTest(int nTest) {
	this.nTest = nTest;
    }

    public int getTp() {
	return tp;
    }

    public void setTp(int tp) {
	this.tp = tp;
    }

    public int getFn() {
	return fn;
    }

    public void setFn(int fn) {
	this.fn = fn;
    }

    public int getFp() {
	return fp;
    }

    public void setFp(int fp) {
	this.fp = fp;
    }

    public int getTn() {
	return tn;
    }

    public void setTn(int tn) {
	this.tn = tn;
    }

}
