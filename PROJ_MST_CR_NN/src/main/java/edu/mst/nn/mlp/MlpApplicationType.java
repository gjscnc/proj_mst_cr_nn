/**
 * 
 */
package edu.mst.nn.mlp;

/**
 * Provides name used in the SPS database for separating MLP weights by
 * application type.
 * 
 * @author George
 *
 */
public enum MlpApplicationType {
    
    IOC_TAGGER, COGNITIVE_SEARCH_RELEVANCE

}
