/**
 * 
 */
/**
 * Neural network components, limited to multi-layer perceptron at this time.
 * 
 * @author gjs
 *
 */
package edu.mst.nn.mlp;