/**
 * 
 */
package edu.mst.conceptrecogn.phraselevel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;

import org.nd4j.linalg.api.ndarray.INDArray;

import edu.mst.conceptrecogn.ConceptRecognMlp;
import edu.mst.conceptrecogn.phraselevel.data.PhraseLevelFeatures;
import edu.mst.conceptrecogn.phraselevel.data.PhraseLevelNnData;
import edu.mst.conceptrecogn.phraselevel.data.PhraselevelFeatureCalculator;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhrase;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseWord;
import edu.mst.db.ontol.names.ConceptName;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Recognizer for working at the phrase level
 * 
 * @author gjs
 *
 */
public class PhraseLevelRecognizer {

    protected ConceptRecognMlp mlp;
    protected JdbcConnectionPool connPool;
    protected PhraselevelFeatureCalculator calculator;
    protected ConceptNameDao nameDao = new ConceptNameDao();
    protected ConceptNameWordDao nameWordDao = new ConceptNameWordDao();

    public PhraseLevelRecognizer(ConceptRecognMlp mlp,
	    JdbcConnectionPool connPool) {
	this.mlp = mlp;
	this.connPool = connPool;
	calculator = new PhraselevelFeatureCalculator(connPool);
	PhraseLevelFeatures.setFeatureCombination(mlp.getFeatureComboInputs());
    }

    public long[] tag(GoldStdPhrase phrase) throws Exception {

	int pmid = phrase.getPmid();
	int sentNbr = phrase.getSentNbr();
	int phraseNbr = phrase.getSentPhraseNbr();

	int nbrWordsInText = phrase.getWords().size();

	long[] mappedConceptUids = new long[nbrWordsInText];
	Arrays.fill(mappedConceptUids, 0L);

	int[] lexWordIds = new int[nbrWordsInText];
	{
	    List<GoldStdPhraseWord> phraseWords = phrase.getWords();
	    phraseWords.sort(
		    GoldStdPhraseWord.Comparators.byPmid_SentNbr_SentPhraseNbr_PhraseWordNbr);
	    Iterator<GoldStdPhraseWord> wordIter = phraseWords.iterator();
	    while (wordIter.hasNext()) {
		GoldStdPhraseWord word = wordIter.next();
		lexWordIds[word.getPhraseWordNbr()] = word.getLexWordId();
	    }
	}

	boolean[] taggedPositions = new boolean[nbrWordsInText];
	Arrays.fill(taggedPositions, false);

	for (int i = nbrWordsInText - 1; i <= 0; i--) {

	    if (taggedPositions[i]) {
		continue;
	    }

	    int lexWordId = lexWordIds[i];
	    // copy mapped positions to avoid losing status during feature
	    // extraction
	    boolean[] taggedPositionsLastIteration = Arrays
		    .copyOf(taggedPositions, nbrWordsInText);

	    SortedSet<Long> candidateNameUids = nameWordDao
		    .getNameIdsWithWord(lexWordId, connPool);

	    List<PhraseLevelFeatures> candidateFeatures = new ArrayList<PhraseLevelFeatures>();
	    Iterator<Long> nameUidIter = candidateNameUids.iterator();
	    while (nameUidIter.hasNext()) {

		long nameUid = nameUidIter.next();
		ConceptName name = nameDao.marshal(nameUid, connPool);
		long conceptUid = name.getConceptUid();

		PhraseLevelFeatures features = new PhraseLevelFeatures(pmid,
			sentNbr, phraseNbr, lexWordIds, taggedPositions,
			nameUid, conceptUid, connPool);
		calculator.compute(features);
		candidateFeatures.add(features);
	    }

	    PhraseLevelNnData phraseLevelData = new PhraseLevelNnData(0,
		    candidateFeatures, mlp.getFeatureComboInputs(),
		    mlp.getFeatureComboOutputs());

	    INDArray results = mlp.getMlp()
		    .output(phraseLevelData.getNnData().dataIter);

	    SortedMap<Double, PhraseLevelFeatures> sortedResults = new TreeMap<Double, PhraseLevelFeatures>();
	    for (int rowNbr = 0; rowNbr < results.rows(); rowNbr++) {
		INDArray resultRow = results.getRow(rowNbr);
		double probGoodFit = resultRow.getDouble(0);
		double probPoorFit = resultRow.getDouble(1);
		boolean isPredictedGoodFit = probGoodFit > probPoorFit;
		if (isPredictedGoodFit) {
		    PhraseLevelFeatures features = candidateFeatures
			    .get(rowNbr);
		    sortedResults.put(probGoodFit, features);
		}
	    }

	    Double bestFitValue = sortedResults.lastKey();
	    PhraseLevelFeatures bestFitFeatures = sortedResults
		    .get(bestFitValue);

	    taggedPositions = bestFitFeatures.getTextWordPosMapped();
	    for (int j = 0; j < nbrWordsInText; j++) {
		if (taggedPositions[j]) {
		    if (!taggedPositionsLastIteration[j]) {
			mappedConceptUids[j] = bestFitFeatures.getConceptUid();
		    }
		}
	    }

	}

	return mappedConceptUids;

    }

}
