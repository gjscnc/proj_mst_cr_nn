package edu.mst.conceptrecogn.phraselevel.data;

import java.util.ArrayList;
import java.util.List;

import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.javatuples.Pair;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;
import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureCombinationOutputs;
import edu.mst.conceptrecogn.FeatureName;
import edu.mst.conceptrecogn.taglevel.data.TagLevelFeatures;
import edu.mst.conceptrecogn.train.BaseDataRecord;
import edu.mst.conceptrecogn.train.BaseTrainingData;

public class PhraseLevelNnData extends BaseTrainingData {

    private List<PhraseLevelFeatures> features;
    private FeatureCombinationInputs featureComboInputs = null;
    private FeatureCombinationOutputs featureComboOutputs = null;
    private NnData nnData;

    public PhraseLevelNnData(int batchSize, List<PhraseLevelFeatures> features,
	    FeatureCombinationInputs featureComboInputs,
	    FeatureCombinationOutputs featureComboOutputs) {
	super(batchSize);
	this.features = features;
	this.featureComboInputs = featureComboInputs;
	this.featureComboOutputs = featureComboOutputs;
    }

    @Override
    public void marshalNnTrainingData() throws Exception {
	DoubleMatrix2D rawTestDataMatrix = convertDataForNn(features);
	Pair<DataSet, ListDataSetIterator<DataSet>> dataAndIter = getModelDataSet(
		rawTestDataMatrix);
	NnData nnData = new NnData();
	nnData.rawData = features;
	nnData.rawDataMatrix = rawTestDataMatrix;
	nnData.data = dataAndIter.getValue0();
	nnData.dataIter = dataAndIter.getValue1();
    }

    @Override
    protected DoubleMatrix2D convertDataForNn(
	    List<? extends BaseDataRecord> dataRecords) throws Exception {
	int nbrRows = dataRecords.size();
	int nbrColumns = TagLevelFeatures.getNbrOfColumns();
	DoubleMatrix2D dataArray = DoubleFactory2D.dense.make(nbrRows,
		nbrColumns);
	FeatureName[] featureNames = null;
	if (featureComboInputs != null) {
	    featureNames = featureComboInputs.orderedFeatureNames();
	} else if (orderedFeatureNames != null) {
	    featureNames = orderedFeatureNames;
	} else {
	    throw new Exception("Missing feature names");
	}
	for (int row = 0; row < nbrRows; row++) {
	    PhraseLevelFeatures dataRecord = (PhraseLevelFeatures) dataRecords
		    .get(row);
	    int column = 0;
	    for (FeatureName featureName : featureNames) {
		double value = 0.0d;
		switch (featureName) {
		case FRACT_CONCEPT_WORDS_MATCHED:
		    value = dataRecord.getFractConceptWordsMatched();
		    break;
		case FRACT_TEXT_WORDS_MATCHED:
		    value = dataRecord.getFractTextWordsMatched();
		    break;
		case FRACT_CONCEPT_ONTOL_IOC:
		    value = dataRecord.getFractConceptIoc();
		    break;
		case FRACT_TEXT_ONTOL_IOC:
		    value = dataRecord.getFractTextIoc();
		    break;
		case FRACT_ABSTR_CORPUS_COGENCY:
		    value = dataRecord.getFractAbstrCogency();
		    break;
		case FRACT_SENT_CORPUS_COGENCY:
		    value = dataRecord.getFractSentCogency();
		    break;
		case IS_CONCEPT_SINGLE_WORD:
		    value = dataRecord.isConceptSingleWord();
		    break;
		case IS_TEXT_SINGLE_WORD:
		    value = dataRecord.isTextSingleWord();
		    break;
		default:
		    break;
		}
		dataArray.set(row, column, value);
		column++;
	    }
	    dataArray.set(row, column, dataRecord.isGoodMatch());
	    column++;
	    dataArray.set(row, column, dataRecord.isPoorMatch());
	}
	return dataArray;
    }

    private Pair<DataSet, ListDataSetIterator<DataSet>> getModelDataSet(
	    DoubleMatrix2D data) throws Exception {

	int nbrRows = data.rows();
	int nbrInputColumns = 0;
	nbrInputColumns = featureComboInputs.orderedFeatureNames().length;

	int nbrOutputColumns = featureComboOutputs.nbrOfFeatures();

	DoubleMatrix2D inputDataView = data.viewPart(0, 0, nbrRows,
		nbrInputColumns);
	double[][] inputDataArray = inputDataView.toArray();

	DoubleMatrix2D outputDataView = data.viewPart(0, nbrInputColumns,
		nbrRows, nbrOutputColumns);
	double[][] outputDataArray = outputDataView.toArray();

	INDArray featuresNDArray = Nd4j.create(inputDataArray);
	INDArray labelsNDArray = Nd4j.create(outputDataArray);

	DataSet dataSet = new DataSet(featuresNDArray, labelsNDArray);

	List<String> labelNames = new ArrayList<String>();
	for (int i = 0; i < nbrOutputColumns; i++) {
	    labelNames.add(
		    featureComboOutputs.orderedFeatureNames()[i].featureName());
	}
	dataSet.setLabelNames(labelNames);
	ListDataSetIterator<DataSet> listDataSetIter = new ListDataSetIterator<DataSet>(
		dataSet.asList(), batchSize);
	return new Pair<DataSet, ListDataSetIterator<DataSet>>(dataSet,
		listDataSetIter);
    }

    public class NnData {
	public List<PhraseLevelFeatures> rawData = null;
	public DoubleMatrix2D rawDataMatrix = null;
	public DataSet data = null;
	public ListDataSetIterator<DataSet> dataIter = null;
    }

    public NnData getNnData() {
	return nnData;
    }

}
