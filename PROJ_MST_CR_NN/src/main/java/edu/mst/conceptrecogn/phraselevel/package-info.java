/**
 * 
 */
/**
 * Components for training a phrase-level concept recognizer.
 * 
 * @author gjs
 *
 */
package edu.mst.conceptrecogn.phraselevel;