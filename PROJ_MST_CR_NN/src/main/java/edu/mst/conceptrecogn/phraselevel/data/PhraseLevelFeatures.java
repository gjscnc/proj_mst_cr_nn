/**
 * 
 */
package edu.mst.conceptrecogn.phraselevel.data;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.javatuples.Pair;

import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureCombinationOutputs;
import edu.mst.conceptrecogn.FeatureName;
import edu.mst.conceptrecogn.Features;
import edu.mst.conceptrecogn.train.BaseDataRecord;
import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.util.Constants;

/**
 * Input features for concept recognition multi-layer perceptron (MLP).
 * 
 * @author George
 *
 */
public class PhraseLevelFeatures extends BaseDataRecord
	implements Features, Comparable<PhraseLevelFeatures> {

    public static final String pmidTagIdDelim = "-";

    protected int id = 0;
    protected String goldStdPmidTagId = null;
    protected long conceptUid = 0L;
    protected long conceptNameUid = 0L;
    protected String cui = null;
    protected Corpus corpus = null;

    protected double isConceptSingleWord = 0.0d;
    protected double isTextSingleWord = 0.0d;
    protected double fractConceptWordsMatched = 0.0d;
    protected double fractTextWordsMatched = 0.0d;
    protected double isConceptSynonym = 0.0d;
    protected double fractConceptIoc = 0.0d;
    protected double fractTextIoc = 0.0d;
    protected double fractAbstrCogency = 0.0d;
    protected double fractSentCogency = 0.0d;
    protected double isGoodMatch = 0.0d;
    protected double isPoorMatch = 0.0d;

    public static final double defaultTrue = 1.0d;
    public static final double defaultFalse = 0.0d;
    public static final char wordIdsArrayDelim = ',';

    private int pmid;
    private int sentNbr;
    private int phraseNbr;
    private String conceptName;
    private JdbcConnectionPool connPool;

    /**
     * This array stores the gold standard lexicon word IDs
     */
    private int[] textLexWordIds;
    private int nbrTextWords = 0;
    private int nbrTextWordsMatched = 0;

    /**
     * Word ID in text matched to the concept word, by pos in the text
     */
    private int[] conceptVarSubstWordIds;
    private int nbrConceptWords = 0;
    private int nbrConceptWordsMatched = 0;

    /**
     * This array captures whether or not the word in the phrase is mapped to a
     * word in the phrase.
     */
    private boolean[] textWordPosMapped;

    /**
     * Word position of the text in the document sentence.
     */
    private int[] textSentWordPos;

    /**
     * This array stores the lexicon word IDs for the candidate concept.
     */
    private int[] conceptLexWordIds;

    /**
     * This array captures whether or not the word in the concept is mapped to a
     * word in the phrase.
     */
    private boolean[] conceptWordPosMapped;

    private int textPredWordPos = Constants.uninitializedIntVal;
    private int textPredWordId = Constants.uninitializedIntVal;
    private int conceptPredWordPos = Constants.uninitializedIntVal;
    private int conceptPredWordId = Constants.uninitializedIntVal;

    private int nbrWordsMapped = 0;
    private double textMaxIoc = 0.0d;
    private double textMatchedIoc = 0.0d;
    private double conceptMaxIoc = 0.0d;
    private double conceptMatchedIoc = 0.0d;
    private double maxDeltaAbstrCogency = 0.0d;
    private double maxSentDeltaCogency = 0.0d;
    private double matchedAbstrDeltaCogency = 0.0d;
    private double matchedSentDeltaCogency = 0.0d;
    private String conceptWordIdsAsStr = null;
    private String conceptVarSubstWordIdsAsStr = null;
    private String conceptMatchPosAsStr = null;
    private String textWordIdsAsStr = null;
    private String textMatchPosAsStr = null;

    private String mappedCui = null;

    private String errMsg;
    private boolean isErrorExists = false;
    private Exception exception = null;
    private ConceptNameDao conceptNameDao = new ConceptNameDao();
    private ConceptNameWordDao conceptNameWordDao = new ConceptNameWordDao();
    private LexWordVariantDao variantDao = new LexWordVariantDao();

    protected static FeatureCombinationInputs featureCombo = null;
    protected static FeatureCombinationOutputs featureComboOutputs = null;
    protected static FeatureName[] orderedFeatureNames = null;
    protected static int nbrFeatures = 0;
    protected static FeatureName[] orderedOutputNames = null;
    protected static int nbrOutputs = 0;
    protected static String[] columnNames = null;
    protected static int nbrOfColumns = 0;

    public PhraseLevelFeatures(int pmid, int sentNbr, int phraseNbr,
	    int[] phraseLexWordIds, boolean[] wordsMapped, long conceptNameUid,
	    long conceptUid, JdbcConnectionPool connPool) throws Exception {
	this.pmid = pmid;
	this.sentNbr = sentNbr;
	this.phraseNbr = phraseNbr;
	textLexWordIds = phraseLexWordIds;
	textWordPosMapped = wordsMapped;
	this.conceptNameUid = conceptNameUid;
	this.conceptUid = conceptUid;
	this.connPool = connPool;
	marshalMappings();
    }

    private void marshalMappings() throws Exception {

	nbrTextWords = textLexWordIds.length;

	Map<Integer, Set<Integer>> variantsByTextLexWordId = null;
	{
	    Set<Integer> textLexWordIdSet = new HashSet<Integer>();
	    for (int lexWordId : textLexWordIds) {
		textLexWordIdSet.add(lexWordId);
	    }
	    variantsByTextLexWordId = variantDao
		    .marshalVariantsByLexWordId(textLexWordIdSet, connPool);
	}

	try {
	    List<ConceptNameWord> nameWords = conceptNameWordDao
		    .marshalWordsForName(conceptNameUid, connPool);
	    nbrConceptWords = nameWords.size();
	    conceptLexWordIds = new int[nameWords.size()];
	    conceptVarSubstWordIds = new int[nameWords.size()];
	    Arrays.fill(conceptLexWordIds, 0);
	    Arrays.fill(conceptVarSubstWordIds, 0);
	    int pos = 0;
	    for (ConceptNameWord nameWord : nameWords) {
		conceptLexWordIds[pos] = nameWord.getLexWordId();
		conceptVarSubstWordIds[pos] = nameWord.getLexWordId();
		pos++;
	    }
	} catch (SQLException ex) {
	    isErrorExists = true;
	    errMsg = ex.getLocalizedMessage();
	    exception = new Exception(ex);
	    return;
	}
	isConceptSingleWord = nbrConceptWords == 1 ? defaultTrue : defaultFalse;
	conceptWordPosMapped = new boolean[conceptLexWordIds.length];
	Arrays.fill(conceptWordPosMapped, false);

	try {
	    conceptName = conceptNameDao.marshal(conceptNameUid, connPool)
		    .getNameText();
	} catch (SQLException ex) {
	    isErrorExists = true;
	    errMsg = ex.getLocalizedMessage();
	    exception = new Exception(ex);
	    return;
	}
	if (isErrorExists) {
	    return;
	}

	/*
	 * Iterates over text word IDs and concept word IDs, beginning at the
	 * right-most side, to find the first predicate word. Then, scan text
	 * words and concept words to identify which words are matched.
	 */
	// get predicate word id and position
	boolean isPredFound = false;
	for (int i = nbrTextWords - 1; i >= 0; i--) {
	    // if text word previously mapped, continue
	    if(textWordPosMapped[i]) {
		continue;
	    }
	    int textWordId = textLexWordIds[i];
	    for (int j = nbrConceptWords - 1; j >= 0; j--) {
		int conceptWordId = conceptLexWordIds[j];
		if (textWordId == conceptWordId || variantsByTextLexWordId
			.get(textWordId).contains(conceptWordId)) {
		    textPredWordId = textWordId;
		    textPredWordPos = i;
		    conceptPredWordPos = j;
		    conceptPredWordId = conceptWordId;
		    isPredFound = true;
		    break;
		}
	    }
	    if (isPredFound) {
		break;
	    }
	}

	if (!isPredFound) {
	    String errMsg = String.format(
		    "Predicate word not found. concept name = %1$s",
		    conceptName.toString());
	    throw new Exception(errMsg);
	}

	/*
	 * Scan the text words, leftward starting at the predicate word, and
	 * match words in concept that match words in text
	 */
	for (int i = textPredWordPos; i >= 0; i--) {
	    // if text word previously mapped, do not map again
	    if(textWordPosMapped[i]) {
		continue;
	    }
	    int textWordId = textLexWordIds[i];
	    for (int j = conceptPredWordPos; j >= 0; j--) {
		int conceptWordId = conceptLexWordIds[j];
		if (conceptWordPosMapped[j]) {
		    continue;
		}
		boolean isConceptWordMatched = textWordId == conceptWordId;
		boolean isTextVariantMatched = variantsByTextLexWordId
			.get(textWordId).contains(conceptWordId);
		if (isConceptWordMatched || isTextVariantMatched) {
		    if (isTextVariantMatched) {
			conceptVarSubstWordIds[j] = textWordId;
		    }
		    conceptWordPosMapped[j] = true;
		    nbrConceptWordsMatched++;
		    textWordPosMapped[i] = true;
		    nbrTextWordsMatched++;
		}
	    }
	}

	isTextSingleWord = nbrTextWords == 1 ? defaultTrue : defaultFalse;

	nbrWordsMapped = nbrConceptWordsMatched;

	conceptWordIdsAsStr = intArrayToStr(conceptLexWordIds);
	conceptVarSubstWordIdsAsStr = intArrayToStr(conceptVarSubstWordIds);
	conceptMatchPosAsStr = boolArrayToStr(conceptWordPosMapped);
	textWordIdsAsStr = intArrayToStr(textLexWordIds);
	textMatchPosAsStr = boolArrayToStr(textWordPosMapped);

    }

    private String boolArrayToStr(boolean[] bools) {
	StringBuilder strBuilder = new StringBuilder();
	for (int i = 0; i < bools.length; i++) {
	    strBuilder.append(bools[i] ? '1' : '0');
	    if (i != bools.length - 1) {
		strBuilder.append(wordIdsArrayDelim);
	    }
	}
	return strBuilder.toString();
    }

    private String intArrayToStr(int[] lexWordIds) {
	StringBuilder strBuilder = new StringBuilder();
	for (int i = 0; i < lexWordIds.length; i++) {
	    strBuilder.append(lexWordIds[i]);
	    if (i != lexWordIds.length - 1) {
		strBuilder.append(wordIdsArrayDelim);
	    }
	}
	return strBuilder.toString();
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("goldStdPmidTagId = ").append(goldStdPmidTagId)
		.append(", ");
	builder.append("conceptNameUid = ").append(conceptNameUid).append(", ");
	builder.append("fractConceptWordsMatched = ")
		.append(fractConceptWordsMatched).append(", ");
	builder.append("fractTextWordsMatched = ").append(fractTextWordsMatched)
		.append(", ");
	builder.append("fractConceptIoc = ").append(fractConceptIoc)
		.append(", ");
	builder.append("fractTextIoc = ").append(fractTextIoc).append(", ");
	builder.append("fractAbstrCogency = ").append(fractAbstrCogency)
		.append(", ");
	builder.append("fractSentCogency = ").append(fractSentCogency)
		.append(", ");
	builder.append("isConceptSingleWord = ").append(isConceptSingleWord)
		.append(", ");
	builder.append("isTextSingleWord = ").append(isTextSingleWord)
		.append(", ");
	builder.append("isGoodMatch = ").append(isGoodMatch).append(", ");
	builder.append("isPoorMatch = ").append(isPoorMatch).append(", ");
	return builder.toString();
    }

    public static void setFeatureCombination(
	    FeatureName[] orderedFeatureNames) {
	PhraseLevelFeatures.orderedFeatureNames = orderedFeatureNames;
	nbrFeatures = orderedFeatureNames.length;
	int totalNbrColumns = nbrFeatures + nbrOutputs;
	columnNames = new String[totalNbrColumns];
	for (int i = 0; i < totalNbrColumns; i++) {
	    if (i < nbrFeatures)
		columnNames[i] = orderedFeatureNames[i].featureName();
	    if (i >= nbrFeatures)
		columnNames[i] = orderedOutputNames[i - nbrFeatures]
			.featureName();
	}
	nbrOfColumns = columnNames.length;
    }

    public static void setFeatureCombination(FeatureCombinationInputs featureCombo) {
	PhraseLevelFeatures.featureCombo = featureCombo;
	nbrFeatures = featureCombo.nbrOfFeatures();
	int totalNbrColumns = nbrFeatures + nbrOutputs;
	columnNames = new String[totalNbrColumns];
	for (int i = 0; i < totalNbrColumns; i++) {
	    if (i < nbrFeatures)
		columnNames[i] = featureCombo.orderedFeatureNames()[i]
			.featureName();
	    if (i >= nbrFeatures)
		columnNames[i] = orderedOutputNames[i - nbrFeatures]
			.featureName();
	}
	nbrOfColumns = columnNames.length;
    }

    public static class Comparators {
	public static final Comparator<PhraseLevelFeatures> byId = (
		PhraseLevelFeatures record1,
		PhraseLevelFeatures record2) -> Integer.compare(record1.id,
			record2.id);
	public static final Comparator<PhraseLevelFeatures> byGoldStdPmidTagId = (
		PhraseLevelFeatures record1,
		PhraseLevelFeatures record2) -> record1.goldStdPmidTagId
			.compareTo(record2.goldStdPmidTagId);
	public static final Comparator<PhraseLevelFeatures> byConceptNameUid = (
		PhraseLevelFeatures record1,
		PhraseLevelFeatures record2) -> Long.compare(
			record1.conceptNameUid, record2.conceptNameUid);
	public static final Comparator<PhraseLevelFeatures> byConceptUid = (
		PhraseLevelFeatures record1,
		PhraseLevelFeatures record2) -> Long.compare(record1.conceptUid,
			record2.conceptUid);
	public static final Comparator<PhraseLevelFeatures> byGoldStdPmidTagId_ConceptNameUid = (
		PhraseLevelFeatures record1,
		PhraseLevelFeatures record2) -> byGoldStdPmidTagId
			.thenComparing(byConceptNameUid)
			.compare(record1, record2);
    }

    public int compareTo(PhraseLevelFeatures record) {
	return Comparators.byId.compare(this, record);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof PhraseLevelFeatures) {
	    PhraseLevelFeatures record = (PhraseLevelFeatures) obj;
	    return id == record.id;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(id);
    }

    public static FeatureCombinationInputs getFeatureCombo() {
	return featureCombo;
    }

    public static String[] getColumnNames() {
	return columnNames;
    }

    public static int getNbrOfColumns() {
	return nbrOfColumns;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getGoldStdPmidTagId() {
	return goldStdPmidTagId;
    }

    public Pair<Integer, String> parseGoldStdPmidTagId() {
	String[] parts = goldStdPmidTagId.split(pmidTagIdDelim);
	Integer pmid = Integer.valueOf(parts[0]);
	String tagId = parts[1];
	return new Pair<Integer, String>(pmid, tagId);
    }

    public void setGoldStdPmidTagId(String goldStdPmidTagId) {
	this.goldStdPmidTagId = goldStdPmidTagId;
    }

    public void setGoldStdPmidTagId(int pmid, String tagId) {
	this.goldStdPmidTagId = String.format("%1$d%2$s%3$s", pmid,
		pmidTagIdDelim, tagId);
    }

    public long getConceptUid() {
	return conceptUid;
    }

    public void setConceptUid(long conceptUid) {
	this.conceptUid = conceptUid;
    }

    public long getConceptNameUid() {
	return conceptNameUid;
    }

    public void setConceptNameUid(long conceptNameUid) {
	this.conceptNameUid = conceptNameUid;
    }

    public double getFractConceptWordsMatched() {
	return fractConceptWordsMatched;
    }

    public void setFractConceptWordsMatched(double fractConceptWordsMatched) {
	this.fractConceptWordsMatched = fractConceptWordsMatched;
    }

    public double getFractTextWordsMatched() {
	return fractTextWordsMatched;
    }

    public void setFractTextWordsMatched(double fractTextWordsMatched) {
	this.fractTextWordsMatched = fractTextWordsMatched;
    }

    public void setIsConceptSynonym(double isConceptSynonym) {
	this.isConceptSynonym = isConceptSynonym;
    }

    public double getIsConceptSynonym() {
	return isConceptSynonym;
    }

    public double getFractConceptIoc() {
	return fractConceptIoc;
    }

    public void setFractConceptIoc(double fractConceptIoc) {
	this.fractConceptIoc = fractConceptIoc;
    }

    public double getFractTextIoc() {
	return fractTextIoc;
    }

    public void setFractTextIoc(double fractTextIoc) {
	this.fractTextIoc = fractTextIoc;
    }

    public double isConceptSingleWord() {
	return isConceptSingleWord;
    }

    public void setIsConceptSingleWord(double isConceptSingleWord) {
	this.isConceptSingleWord = isConceptSingleWord;
    }

    public double isTextSingleWord() {
	return isTextSingleWord;
    }

    public void setIsTextSingleWord(double isTextSingleWord) {
	this.isTextSingleWord = isTextSingleWord;
    }

    public double isGoodMatch() {
	return isGoodMatch;
    }

    public void setIsGoodMatch(double isGoodMatch) {
	this.isGoodMatch = isGoodMatch;
    }

    public double isPoorMatch() {
	return isPoorMatch;
    }

    public void setIsPoorMatch(double isPoorMatch) {
	this.isPoorMatch = isPoorMatch;
    }

    public String getCui() {
	return cui;
    }

    public void setCui(String cui) {
	this.cui = cui;
    }

    public Corpus getCorpus() {
	return corpus;
    }

    public void setCorpus(Corpus corpus) {
	this.corpus = corpus;
    }

    public static FeatureName[] getOrderedFeatureNames() {
	return orderedFeatureNames;
    }

    public static int getNbrFeatures() {
	return nbrFeatures;
    }

    public double getFractAbstrCogency() {
	return fractAbstrCogency;
    }

    public void setFractAbstrCogency(double fractAbstrCogency) {
	this.fractAbstrCogency = fractAbstrCogency;
    }

    public double getFractSentCogency() {
	return fractSentCogency;
    }

    public void setFractSentCogency(double fractSentCogency) {
	this.fractSentCogency = fractSentCogency;
    }

    public double getTextMaxIoc() {
	return textMaxIoc;
    }

    public void setTextMaxIoc(double textMaxIoc) {
	this.textMaxIoc = textMaxIoc;
    }

    public double getTextMatchedIoc() {
	return textMatchedIoc;
    }

    public void setTextMatchedIoc(double textMatchedIoc) {
	this.textMatchedIoc = textMatchedIoc;
    }

    public double getConceptMaxIoc() {
	return conceptMaxIoc;
    }

    public void setConceptMaxIoc(double conceptMaxIoc) {
	this.conceptMaxIoc = conceptMaxIoc;
    }

    public double getConceptMatchedIoc() {
	return conceptMatchedIoc;
    }

    public void setConceptMatchedIoc(double conceptMatchedIoc) {
	this.conceptMatchedIoc = conceptMatchedIoc;
    }

    public double getMaxDeltaAbstrCogency() {
	return maxDeltaAbstrCogency;
    }

    public void setMaxDeltaAbstrCogency(double maxDeltaAbstrCogency) {
	this.maxDeltaAbstrCogency = maxDeltaAbstrCogency;
    }

    public double getMaxSentDeltaCogency() {
	return maxSentDeltaCogency;
    }

    public void setMaxSentDeltaCogency(double maxSentDeltaCogency) {
	this.maxSentDeltaCogency = maxSentDeltaCogency;
    }

    public double getMatchedAbstrDeltaCogency() {
	return matchedAbstrDeltaCogency;
    }

    public void setMatchedAbstrDeltaCogency(double matchedAbstrDeltaCogency) {
	this.matchedAbstrDeltaCogency = matchedAbstrDeltaCogency;
    }

    public double getMatchedSentDeltaCogency() {
	return matchedSentDeltaCogency;
    }

    public void setMatchedSentDeltaCogency(double matchedSentDeltaCogency) {
	this.matchedSentDeltaCogency = matchedSentDeltaCogency;
    }

    public String getConceptVarSubstWordIdsAsStr() {
	return conceptVarSubstWordIdsAsStr;
    }

    public void setConceptVarSubstWordIdsAsStr(
	    String conceptVarSubstWordIdsAsStr) {
	this.conceptVarSubstWordIdsAsStr = conceptVarSubstWordIdsAsStr;
    }

    public double getIsConceptSingleWord() {
	return isConceptSingleWord;
    }

    public double getIsTextSingleWord() {
	return isTextSingleWord;
    }

    public double getIsGoodMatch() {
	return isGoodMatch;
    }

    public double getIsPoorMatch() {
	return isPoorMatch;
    }

    public int getPmid() {
	return pmid;
    }

    public int getSentNbr() {
	return sentNbr;
    }

    public int getPhraseNbr() {
	return phraseNbr;
    }

    public String getConceptName() {
	return conceptName;
    }

    public int[] getTextLexWordIds() {
	return textLexWordIds;
    }

    public int getNbrTextWords() {
	return nbrTextWords;
    }

    public int getNbrTextWordsMatched() {
	return nbrTextWordsMatched;
    }

    public int[] getConceptVarSubstWordIds() {
	return conceptVarSubstWordIds;
    }

    public int getNbrConceptWords() {
	return nbrConceptWords;
    }

    public int getNbrConceptWordsMatched() {
	return nbrConceptWordsMatched;
    }

    public boolean[] getTextWordPosMapped() {
	return textWordPosMapped;
    }

    public int[] getTextSentWordPos() {
	return textSentWordPos;
    }

    public int[] getConceptLexWordIds() {
	return conceptLexWordIds;
    }

    public boolean[] getConceptWordPosMapped() {
	return conceptWordPosMapped;
    }

    public int getTextPredWordPos() {
	return textPredWordPos;
    }

    public int getTextPredWordId() {
	return textPredWordId;
    }

    public int getConceptPredWordPos() {
	return conceptPredWordPos;
    }

    public int getConceptPredWordId() {
	return conceptPredWordId;
    }

    public int getNbrWordsMapped() {
	return nbrWordsMapped;
    }

    public String getConceptWordIdsAsStr() {
	return conceptWordIdsAsStr;
    }

    public String getConceptMatchPosAsStr() {
	return conceptMatchPosAsStr;
    }

    public String getTextWordIdsAsStr() {
	return textWordIdsAsStr;
    }

    public String getTextMatchPosAsStr() {
	return textMatchPosAsStr;
    }

    public String getMappedCui() {
	return mappedCui;
    }

    public void setMappedCui(String mappedCui) {
	this.mappedCui = mappedCui;
    }

    public String getErrMsg() {
	return errMsg;
    }

    public Exception getException() {
	return exception;
    }

}
