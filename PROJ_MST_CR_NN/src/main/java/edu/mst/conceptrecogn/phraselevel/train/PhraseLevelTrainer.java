/**
 * 
 */
package edu.mst.conceptrecogn.phraselevel.train;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.conceptrecogn.ConceptRecognMlp;
import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureCombinationOutputs;
import edu.mst.conceptrecogn.RecognitionLevel;
import edu.mst.conceptrecogn.phraselevel.PhraseLevelRecognizer;
import edu.mst.conceptrecogn.phraselevel.data.PhraseLevelFeatures;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhrase;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWord;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWordDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PhraseLevelTrainer {

    private ConceptRecognMlp mlp = null;
    private JdbcConnectionPool connPool = null;
    private FeatureCombinationInputs featureCombo = null;

    private GoldStdTagDao tagDao = new GoldStdTagDao();
    private GoldStdTagWordDao tagWordsDao = new GoldStdTagWordDao();
    private GoldStdPhraseDao phraseDao = new GoldStdPhraseDao();

    public PhraseLevelTrainer(ConceptRecognMlp mlp,
	    FeatureCombinationInputs featureCombo,
	    JdbcConnectionPool connPool) {
	this.mlp = mlp;
	this.featureCombo = featureCombo;
	this.connPool = connPool;
    }

    public void train(boolean isUseEarlyStopping, boolean isPersistWgts,
	    boolean isOutputSingleNeuron) throws Exception {

	SortedSet<GoldStdTag> tags = tagDao.marshalAll(connPool);
	SortedSet<GoldStdPhrase> phrases = new TreeSet<GoldStdPhrase>();
	Iterator<GoldStdTag> tagIter = tags.iterator();
	while (tagIter.hasNext()) {

	    GoldStdTag tag = tagIter.next();
	    int pmid = tag.getPmid();
	    String tagId = tag.getTagId();
	    SortedSet<GoldStdTagWord> tagWords = tagWordsDao.marshal(pmid,
		    tagId, connPool);
	    // make sure none cross into other phrases
	    Iterator<GoldStdTagWord> tagWordIter = tagWords.iterator();
	    GoldStdPhrase lastPhrase = null;
	    boolean isCrossPhrase = false;
	    while (tagWordIter.hasNext()) {
		GoldStdTagWord word = tagWordIter.next();
		GoldStdPhrase phrase = phraseDao.retrievePhraseAtSentWordPos(
			pmid, word.getSentNbr(), word.getSentWordNbr(),
			connPool);
		if (lastPhrase == null) {
		    lastPhrase = phrase;
		    continue;
		} else {
		    if (phrase.getSentPhraseNbr() != lastPhrase
			    .getSentPhraseNbr()) {
			isCrossPhrase = true;
			break;
		    }
		}
	    }
	    if (isCrossPhrase) {
		continue;
	    }
	    phrases.add(lastPhrase);
	}

	FeatureCombinationOutputs featureCombinationOutputs = isOutputSingleNeuron
		? FeatureCombinationOutputs.ONE
		: FeatureCombinationOutputs.TWO;
	ConceptRecognMlp mlp = new ConceptRecognMlp(featureCombo,
		featureCombinationOutputs, RecognitionLevel.ANNOTATION_PHRASE);
	PhraseLevelRecognizer recognizer = new PhraseLevelRecognizer(mlp,
		connPool);
	Iterator<GoldStdPhrase> phraseIter = phrases.iterator();
	while (phraseIter.hasNext()) {
	    GoldStdPhrase phrase = phraseIter.next();
	    
	}

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	// TODO Auto-generated method stub

    }

}
