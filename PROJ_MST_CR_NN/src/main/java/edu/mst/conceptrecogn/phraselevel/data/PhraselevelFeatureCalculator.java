/**
 * 
 */
package edu.mst.conceptrecogn.phraselevel.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;

import edu.mst.conceptrecogn.ConceptRecognCogencyType;
import edu.mst.conceptrecogn.ConceptRecognTextType;
import edu.mst.db.nlm.corpora.Cogency;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.ontol.ConceptDao;
import edu.mst.db.ontol.cogency.OntolCogency;
import edu.mst.db.ontol.cogency.OntolCogencyDao;
import edu.mst.db.ontol.cogency.OntolCogencyVariant;
import edu.mst.db.ontol.cogency.OntolCogencyVariantDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordProb;
import edu.mst.db.ontol.words.condprob.OntolCondWordProbDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordVariantProb;
import edu.mst.db.ontol.words.condprob.OntolCondWordVariantProbDao;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.util.Constants;

/**
 * Performs feature calculations for each concept candidate.
 * 
 * @author gjs
 *
 */
public class PhraselevelFeatureCalculator {

    public static final double trueAsDbl = 1.0d;
    public static final double falseAsDbl = 0.0d;

    private JdbcConnectionPool connPool = null;

    private ConceptDao conceptDao = new ConceptDao();
    private NlmAbstractWordDao abstrWordDao = new NlmAbstractWordDao();
    private OntolCogencyDao ontolCogencyDao = new OntolCogencyDao();
    private OntolCogencyVariantDao ontolCogVariantDao = new OntolCogencyVariantDao();
    private OntolCondWordProbDao ontolCondWordProbDao = new OntolCondWordProbDao();
    private OntolCondWordVariantProbDao ontolCondWordVarProbDao = new OntolCondWordVariantProbDao();

    private SortedMap<Integer, Cogency> cogencyByPos = new TreeMap<Integer, Cogency>();

    public PhraselevelFeatureCalculator(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public void compute(PhraseLevelFeatures features) throws Exception {

	if (features.getTextPredWordPos() == Constants.uninitializedIntVal
		|| features
			.getConceptPredWordPos() == Constants.uninitializedIntVal
		|| features
			.getTextPredWordId() == Constants.uninitializedIntVal) {
	    String errMsg = String.format(
		    "Predicate word not found, cannot compute feature values. "
			    + "Text predicate pos = %5$d, predicate lex base word ID = %6$d, "
			    + "concept predicate pos = %7$d, concept UID = %8$d, concept name UID = %9$d %n",
		    features.getTextPredWordPos(),
		    features.getTextPredWordId(),
		    features.getConceptPredWordPos(),
		    features.getConceptUid(),
		    features.getConceptNameUid());
	    System.out.printf(errMsg);
	    throw new Exception(errMsg);
	}

	/*
	 * If both the text and concept are single-word, set feature values and
	 * return. If text is single-word, but concept is not, check to see if
	 * the concept name is a synonym.
	 */
	if (features.isTextSingleWord() == 1.0) {
	    if (features.isConceptSingleWord() == 1.0) {
		computeSingleWordFeatures(features);
		return;
	    } else {
		/*
		 * See if concept is a synonym to text token
		 */
		boolean isConceptSynonym = isConceptSynonym(features);
		if (isConceptSynonym) {
		    setConceptSynonymFeatures(features);
		    return;
		}
	    }
	}

	/*
	 * Extract cogencies for the sentence for use when computing maximum and
	 * matched cogencies for the text and candidate concept. Cogencies for
	 * sentence also contain cogencies for the complete abstract.
	 */
	for (Cogency cogency : abstrWordDao.getSentCogencies(features.getPmid(),
		features.getSentNbr(), connPool)) {
	    cogencyByPos.put(cogency.getWordNbr(), cogency);
	}

	/*
	 * Compute max IOC values
	 */
	for (ConceptRecognTextType textType : ConceptRecognTextType.values()) {
	    computeMaxIoc(features, textType);
	}

	/*
	 * Computed matched IOC values
	 */
	for (ConceptRecognTextType textType : ConceptRecognTextType.values()) {
	    computeMatchedIoc(features, textType);
	}

	/*
	 * Compute max cogency values
	 */
	computeMaxDeltaCogencies(features);

	/*
	 * compute text matched cogency values
	 */
	computeMatchedDeltaCogencies(features);

	/*
	 * compute fraction words mapped
	 */
	for (ConceptRecognTextType textType : ConceptRecognTextType.values()) {
	    computeFractWordsMatched(features, textType);
	}

	/*
	 * Fraction IOC matched
	 */
	for (ConceptRecognTextType textType : ConceptRecognTextType.values()) {
	    computeFractIocMatched(features, textType);
	}

	/*
	 * Fraction corpora cogency matched
	 */
	for (ConceptRecognCogencyType cogencyType : ConceptRecognCogencyType
		.values()) {
	    computeFractCogencyMatched(features, cogencyType);
	}

	/*
	 * Set matched CUI in the ontology
	 */
	features.setMappedCui(
		conceptDao.getCui(features.getConceptUid(), connPool));

    }

    private void computeMaxIoc(PhraseLevelFeatures features,
	    ConceptRecognTextType textType) throws SQLException {

	double maxIoc = 0.0d;
	switch (textType) {
	case CONCEPT:
	    if (features.getNbrConceptWords() == 1) {
		break;
	    } else {
		OntolCogency ontolCogency = ontolCogencyDao.marshal(
			features.getConceptNameUid(), features.getConceptUid(),
			features.getConceptPredWordId(), connPool);
		if (ontolCogency != null) {
		    maxIoc = Math.abs(ontolCogency.getLnCogency());
		} else {
		    SortedSet<OntolCogencyVariant> cogencyVariants = ontolCogVariantDao
			    .marshalForVariant(features.getConceptNameUid(),
				    features.getConceptPredWordId(), connPool);
		    if (cogencyVariants.size() > 0) {
			maxIoc = Math
				.abs(cogencyVariants.first().getLnCogency());
		    } else {
			String errMsg = String.format(
				"Maximum IOC not found for concept predicate word ID = %1$d, text predicate word ID = %2$d, "
					+ "concept name UID = %3$d, text type = %4$s, gold standard tag ID = %5$s.",
				features.getConceptPredWordId(),
				features.getTextPredWordId(),
				features.getConceptNameUid(), textType,
				features.getGoldStdPmidTagId());
			throw new SQLException(errMsg);
		    }
		}
	    }
	    break;
	case TEXT:
	    int[] textLexWordIds = features.getTextLexWordIds();
	    if (features.getNbrTextWords() == 1) {
		maxIoc = 0.0d;
	    } else {
		for (int i = 0; i < textLexWordIds.length; i++) {
		    int assumedFactWordId = textLexWordIds[i];
		    int textPredWordId = features.getTextPredWordId();
		    if (textPredWordId == assumedFactWordId) {
			continue;
		    }
		    OntolCondWordProb ontolCondWordProb = ontolCondWordProbDao
			    .marshal(assumedFactWordId, textPredWordId,
				    connPool);
		    if (ontolCondWordProb != null) {
			maxIoc += Math.abs(ontolCondWordProb.getLnCondProb());
		    } else {
			SortedSet<OntolCondWordVariantProb> variantCondProbs = ontolCondWordVarProbDao
				.marshalForVariants(textPredWordId,
					assumedFactWordId, connPool);
			if (variantCondProbs.size() > 0) {
			    maxIoc += Math.abs(
				    variantCondProbs.first().getLnCondProb());
			} else {
			    // variation not found, so continue
			    continue;
			}
		    }
		}
	    }

	    break;
	}

	switch (textType) {
	case CONCEPT:
	    features.setConceptMaxIoc(maxIoc);
	    break;
	case TEXT:
	    features.setTextMaxIoc(maxIoc);
	    break;
	}

    }

    private void computeMatchedIoc(PhraseLevelFeatures features,
	    ConceptRecognTextType textType) throws Exception {

	double matchedIoc = 0.0d;
	if (features.getNbrConceptWords() == 1) {
	    matchedIoc = 0.0;
	    return;
	}

	int[] lexWordIds = null;
	boolean[] wordPosMapped = null;
	int predWordId = 0;

	switch (textType) {
	case CONCEPT:
	    /*
	     * To ensure match to text word IDs, use the concept words array
	     * that includes variant substitution
	     */
	    lexWordIds = features.getConceptVarSubstWordIds();
	    wordPosMapped = features.getConceptWordPosMapped();
	    predWordId = features.getConceptPredWordId();
	    break;
	case TEXT:
	    lexWordIds = features.getTextLexWordIds();
	    wordPosMapped = features.getTextWordPosMapped();
	    predWordId = features.getTextPredWordId();
	    break;
	default:
	    break;
	}

	if (wordPosMapped.length != lexWordIds.length) {
	    String errMsg = String.format("Mapping array length mismatch: "
		    + "word pos mapped array len = %1$,d, text word IDs array len = %2$d, text type = %3$s, "
		    + "gold std tag ID = %4$s, concept name UID = %5$d",
		    wordPosMapped.length, lexWordIds.length, textType,
		    features.getGoldStdPmidTagId(),
		    features.getConceptNameUid());
	    throw new Exception(errMsg);
	}
	int nbrWords = lexWordIds.length;
	for (int i = 0; i < nbrWords; i++) {
	    if (!wordPosMapped[i]) {
		continue;
	    }
	    int assumedFactWordId = lexWordIds[i];
	    if (assumedFactWordId == predWordId) {
		continue;
	    }
	    OntolCondWordProb ontolCondWordProb = ontolCondWordProbDao
		    .marshal(assumedFactWordId, predWordId, connPool);
	    if (ontolCondWordProb != null) {
		matchedIoc += Math.abs(ontolCondWordProb.getLnCondProb());
	    } else {
		SortedSet<OntolCondWordVariantProb> variantCondProbs = ontolCondWordVarProbDao
			.marshalForVariants(predWordId, assumedFactWordId,
				connPool);
		if (variantCondProbs.size() > 0) {
		    matchedIoc += Math
			    .abs(variantCondProbs.first().getLnCondProb());
		} else {
		    continue;
		    /*
		     * String errMsg = String.format(
		     * "Ontology conditional word probability not found for " +
		     * "predicate word ID = %1$d, assumed fact word ID = %2$d, "
		     * + "text type = %3$s.", predWordId, assumedFactWordId,
		     * textType); throw new SQLException(errMsg);
		     */
		}
	    }
	}

	switch (textType) {
	case CONCEPT:
	    features.setConceptMatchedIoc(matchedIoc);
	    break;
	case TEXT:
	    features.setTextMatchedIoc(matchedIoc);
	    break;
	default:
	    break;
	}
    }

    private void computeMaxDeltaCogencies(PhraseLevelFeatures features)
	    throws SQLException {

	int[] textSentWordPos = features.getTextSentWordPos();
	int maxSentWordPos = textSentWordPos[textSentWordPos.length - 1];
	int minSentWordPos = textSentWordPos[0];

	// find word position immediately prior to min sent word
	if (minSentWordPos > 0) {
	    minSentWordPos += -1;
	}

	Cogency maxCogency = cogencyByPos.get(maxSentWordPos);
	Cogency minCogency = cogencyByPos.get(minSentWordPos);

	// sentence max delta cogency
	features.setMaxSentDeltaCogency(
		Math.abs(maxCogency.getCumLnSentCogency()
			- minCogency.getCumLnSentCogency()));

	// abstract max delta cogency
	if (minSentWordPos == 0 && features.getSentNbr() > 0) {
	    SortedSet<Cogency> priorSentCogencies = abstrWordDao
		    .getSentCogencies(features.getPmid(), features.getSentNbr() - 1,
			    connPool);
	    minCogency = priorSentCogencies.last();
	}

	features.setMaxDeltaAbstrCogency(
		Math.abs(maxCogency.getCumLnAbstractCogency()
			- minCogency.getCumLnAbstractCogency()));

    }

    private void computeMatchedDeltaCogencies(PhraseLevelFeatures features)
	    throws Exception {

	int[] textSentWordPos = features.getTextSentWordPos();
	boolean[] matchedTextPos = features.getTextWordPosMapped();
	boolean isFirstSent = features.getSentNbr() == 0;

	int maxSentWordPos = textSentWordPos[textSentWordPos.length - 1];
	int minSentWordPos = textSentWordPos[0];

	// find word position immediately prior to min sent word
	boolean isFirstSentPosFirstSentWord = true;
	if (minSentWordPos > 0) {
	    minSentWordPos += -1;
	    isFirstSentPosFirstSentWord = false;
	}

	Map<Integer, Integer> isMatchedArrayPosBySentPos = new HashMap<Integer, Integer>();
	for (int i = 0; i < textSentWordPos.length; i++) {
	    isMatchedArrayPosBySentPos.put(textSentWordPos[i], i);
	}

	double notMatchedSentDeltaCogency = 0.0d;
	double notMatchedAbstrDeltaCogency = 0.0d;
	for (int sentWordPos = maxSentWordPos; sentWordPos >= minSentWordPos; sentWordPos--) {
	    boolean isKeepDelta = false;
	    if (isMatchedArrayPosBySentPos.containsKey(sentWordPos)) {
		int matchedTextArrayPos = isMatchedArrayPosBySentPos
			.get(sentWordPos);
		isKeepDelta = !matchedTextPos[matchedTextArrayPos];
	    }
	    Cogency currentCogency = cogencyByPos.get(sentWordPos);
	    Cogency priorCogency = null;
	    /*
	     * If this is the first word in the sent, then compute abstract
	     * cogency delta only. If this is the first word and first sentence,
	     * then continue - no prior cogencies.
	     */
	    if (sentWordPos == minSentWordPos && isFirstSentPosFirstSentWord) {
		if (isFirstSent) {
		    continue;
		}
		if (isKeepDelta) {
		    SortedSet<Cogency> priorSentCogencies = abstrWordDao
			    .getSentCogencies(features.getPmid(),
				    features.getSentNbr() - 1, connPool);
		    priorCogency = priorSentCogencies.last();
		    notMatchedAbstrDeltaCogency += currentCogency
			    .getCumLnAbstractCogency()
			    - priorCogency.getCumLnAbstractCogency();
		}
		continue;
	    }
	    priorCogency = cogencyByPos.get(sentWordPos - 1);
	    if (isKeepDelta) {
		notMatchedSentDeltaCogency += currentCogency
			.getCumLnSentCogency()
			- priorCogency.getCumLnSentCogency();
		notMatchedAbstrDeltaCogency += currentCogency
			.getCumLnAbstractCogency()
			- priorCogency.getCumLnAbstractCogency();
	    }
	}

	double matchedAbstrCogency = features.getMaxDeltaAbstrCogency()
		- Math.abs(notMatchedAbstrDeltaCogency);
	double matchedTextCogency = features.getMaxSentDeltaCogency()
		- Math.abs(notMatchedSentDeltaCogency);
	features.setMatchedAbstrDeltaCogency(matchedAbstrCogency);
	features.setMatchedSentDeltaCogency(matchedTextCogency);

    }

    private void computeFractWordsMatched(PhraseLevelFeatures features,
	    ConceptRecognTextType textType) {

	int nbrWordsMapped = 0;
	int denomCount = 0;

	switch (textType) {
	case CONCEPT:
	    nbrWordsMapped = features.getNbrConceptWordsMatched();
	    denomCount = features.getNbrConceptWords();
	    break;
	case TEXT:
	    nbrWordsMapped = features.getNbrTextWordsMatched();
	    denomCount = features.getNbrTextWords();
	    break;
	}

	double fractWordsMatched = (double) nbrWordsMapped
		/ (double) denomCount;

	switch (textType) {
	case CONCEPT:
	    features.setFractConceptWordsMatched(fractWordsMatched);
	    break;
	case TEXT:
	    features.setFractTextWordsMatched(fractWordsMatched);
	    break;
	}

    }

    private void computeFractIocMatched(PhraseLevelFeatures features,
	    ConceptRecognTextType textType) {

	double numer = 0.0d;
	double denom = 0.0d;
	double fractIoc = 0.0d;

	switch (textType) {
	case CONCEPT:
	    numer = features.getConceptMatchedIoc();
	    denom = features.getConceptMaxIoc();
	    break;
	case TEXT:
	    numer = features.getTextMatchedIoc();
	    denom = features.getTextMaxIoc();
	    break;
	}

	if (denom > 0.0) {
	    fractIoc = numer / denom;
	}

	switch (textType) {
	case CONCEPT:
	    features.setFractConceptIoc(fractIoc);
	    break;
	case TEXT:
	    features.setFractTextIoc(fractIoc);
	    break;
	}

    }

    public void computeFractCogencyMatched(PhraseLevelFeatures features,
	    ConceptRecognCogencyType cogencyType) {

	double numer = 0.0d;
	double denom = 0.0d;
	double fractCogency = 0.0d;

	switch (cogencyType) {
	case ABSTRACT:
	    numer = features.getMatchedAbstrDeltaCogency();
	    denom = features.getMaxDeltaAbstrCogency();
	    break;
	case SENTENCE:
	    numer = features.getMatchedSentDeltaCogency();
	    denom = features.getMaxSentDeltaCogency();
	    break;
	}

	if (denom > 0.0d) {
	    fractCogency = numer / denom;
	}

	switch (cogencyType) {
	case ABSTRACT:
	    features.setFractAbstrCogency(fractCogency);
	    break;
	case SENTENCE:
	    features.setFractSentCogency(fractCogency);
	    break;
	}

    }

    /**
     * Compute features when both the text to be mapped (the gold std
     * annotation) and the concept consists of a single word only.
     * 
     * @param features
     */
    private void computeSingleWordFeatures(PhraseLevelFeatures features) {

	boolean isWordMatched = features.getConceptWordPosMapped()[0];
	if (isWordMatched) {
	    // see if the concept is a synonym
	    int conceptWordId = features.getConceptLexWordIds()[0];
	    int textWordId = features.getTextLexWordIds()[0];
	    double isConceptSynonym = conceptWordId != textWordId ? 1.0d : 0.0d;
	    features.setFractConceptWordsMatched(1.0d);
	    features.setFractTextWordsMatched(1.0d);
	    features.setIsConceptSynonym(isConceptSynonym);
	    features.setFractConceptIoc(1.0d);
	    features.setFractTextIoc(1.0d);
	    features.setFractAbstrCogency(0.0d);
	    features.setFractSentCogency(0.0d);
	    features.setIsGoodMatch(1.0d);
	    features.setIsPoorMatch(0.0d);
	} else {
	    features.setFractConceptWordsMatched(0.0d);
	    features.setFractTextWordsMatched(0.0d);
	    features.setIsConceptSynonym(0.0d);
	    features.setFractConceptIoc(0.0d);
	    features.setFractTextIoc(0.0d);
	    features.setFractAbstrCogency(0.0d);
	    features.setFractSentCogency(0.0d);
	    features.setIsGoodMatch(0.0d);
	    features.setIsPoorMatch(1.0d);
	}
    }

    /**
     * Check to see if a multi-word concept is a synonym for a single token in
     * the text.
     * 
     * @param features
     * @return
     * @throws SQLException
     */
    private boolean isConceptSynonym(PhraseLevelFeatures features)
	    throws SQLException {

	boolean isConceptSynonym = false;
	// get all concepts containing the text token
	String getSingleWordConceptSql = "select * from "
		+ " (select count(*) as nbrWords, conceptNameUid from conceptrecogn.conceptnamewords group by conceptNameUid) as nbrW, "
		+ " (select * from conceptrecogn.conceptnamewords) as words, "
		+ " (select lexWordVariantId from conceptrecogn.lexwordvariants where lexWordId=?) as vars "
		+ " where nbrW.nbrWords = 1 and nbrW.conceptNameUid = words.conceptNameUid and words.conceptUid=? "
		+ " and (words.lexWordId = ? or words.lexWordId = vars.lexWordVariantId)";
	int textWordId = features.getTextLexWordIds()[0];
	long conceptUid = features.getConceptUid();
	try (Connection conn = connPool.getConnection()) {
	    try(PreparedStatement getSingleWordConceptStmt = conn.prepareStatement(getSingleWordConceptSql)){
		getSingleWordConceptStmt.setInt(1, textWordId);
		getSingleWordConceptStmt.setLong(2, conceptUid);
		getSingleWordConceptStmt.setInt(3, textWordId);
		try(ResultSet rs = getSingleWordConceptStmt.executeQuery()){
		    if(rs.next()) {
			isConceptSynonym = true;
		    }
		}
	    }
	}

	return isConceptSynonym;
    }

    private void setConceptSynonymFeatures(PhraseLevelFeatures features) {

	features.setFractConceptWordsMatched(1.0d);
	features.setFractTextWordsMatched(1.0d);
	features.setIsConceptSynonym(1.0d);
	features.setFractConceptIoc(1.0d);
	features.setFractTextIoc(1.0d);
	features.setFractAbstrCogency(0.0d);
	features.setFractSentCogency(0.0d);
	features.setIsGoodMatch(1.0d);
	features.setIsPoorMatch(0.0d);

    }
}
