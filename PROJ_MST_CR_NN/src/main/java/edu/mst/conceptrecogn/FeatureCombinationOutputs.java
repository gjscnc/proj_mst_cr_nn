package edu.mst.conceptrecogn;

public enum FeatureCombinationOutputs {

    TWO(new FeatureName[] { 
	    FeatureName.IS_GOOD_MATCH,
	    FeatureName.IS_POOR_MATCH }),
    ONE(new FeatureName[] {
	    FeatureName.IS_GOOD_MATCH});

    FeatureCombinationOutputs(FeatureName[] orderedFeatureNames) {
	this.nbrOfFeatures = orderedFeatureNames.length;
	this.orderedFeatureNames = orderedFeatureNames;
    }
    
    private int nbrOfFeatures = 0;
    private FeatureName[] orderedFeatureNames = null;

    public int nbrOfFeatures() {
	return nbrOfFeatures;
    }

    public FeatureName[] orderedFeatureNames() {
	return orderedFeatureNames;
    }

}
