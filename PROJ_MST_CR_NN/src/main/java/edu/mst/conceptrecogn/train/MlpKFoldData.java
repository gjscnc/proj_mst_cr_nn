/**
 * 
 */
package edu.mst.conceptrecogn.train;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.javatuples.Pair;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import com.google.common.collect.Lists;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;
import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureCombinationOutputs;
import edu.mst.conceptrecogn.FeatureName;
import edu.mst.conceptrecogn.taglevel.data.TagLevelFeatures;
import edu.mst.conceptrecogn.taglevel.data.TrainingDataDao;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * This class contains the k-fold training, validation, and test data along with
 * a method for extracting these from the concept-annotation maps retrieved from
 * the SPS database.
 * 
 * @author George
 *
 */
public class MlpKFoldData extends BaseTrainingData {

    public static final float defaultFractTrain = 0.90f;
    public static final float defaultFractTest = 0.10f;
    public static final int defaultBatchSize = 100;

    private Corpus corpus = null;
    private JdbcConnectionPool connPool = null;

    private List<TagLevelFeatures> rawData = null;
    private int nbrSlices = 10;
    private int sliceSize = 0;
    private List<List<TagLevelFeatures>> rawDataBySlice = null;
    private Map<Integer, TagLevelNnData> nnDataByTestSlice = null;

    private FeatureName[] orderedFeatureNames = null;
    private FeatureCombinationInputs featureComboInputs = null;
    private FeatureCombinationOutputs featureComboOutputs = null;

    public MlpKFoldData(int batchSize, int nbrCuiSlices, Corpus corpus,
	    FeatureName[] orderedFeatureNames,
	    FeatureCombinationOutputs featureComboOutputs,
	    JdbcConnectionPool connPool) {
	super(batchSize);
	this.nbrSlices = nbrCuiSlices;
	this.corpus = corpus;
	this.connPool = connPool;
	this.orderedFeatureNames = orderedFeatureNames;
	this.featureComboOutputs = featureComboOutputs;
    }

    public MlpKFoldData(int batchSize, int nbrCuiSlices, Corpus corpus,
	    FeatureCombinationInputs featureComboInputs,
	    FeatureCombinationOutputs featureComboOutputs,
	    JdbcConnectionPool connPool) {
	super(batchSize);
	this.nbrSlices = nbrCuiSlices;
	this.corpus = corpus;
	this.connPool = connPool;
	this.featureComboInputs = featureComboInputs;
	this.featureComboOutputs = featureComboOutputs;
    }

    @Override
    public void marshalNnTrainingData() throws Exception {

	if (featureComboInputs != null) {
	    TagLevelFeatures.setFeatureCombination(featureComboInputs,
		    featureComboOutputs);
	} else if (orderedFeatureNames != null) {
	    TagLevelFeatures.setFeatureCombination(orderedFeatureNames,
		    featureComboOutputs);
	} else {
	    throw new Exception("Feature names missing");
	}
	TrainingDataDao trainingDataDao = new TrainingDataDao(connPool);
	if (featureComboInputs != null) {
	    rawData = trainingDataDao.marshalAllFeatures(corpus,
		    featureComboInputs, featureComboOutputs);
	} else {
	    rawData = trainingDataDao.marshalAllFeatures(corpus,
		    orderedFeatureNames, featureComboOutputs);
	}
	System.out.printf(
		"Total of %1$,d data records retrieved "
			+ "for corpus %2$s, both training and test.%n",
		rawData.size(), corpus.toString());

	sliceSize = (int) Math
		.round((double) rawData.size() / (double) nbrSlices);
	System.out.printf(
		"Partitioning %1$,d records into %2$,d slices, %3$,d records per slice. %n",
		rawData.size(), nbrSlices, sliceSize);
	Collections.shuffle(rawData);
	rawDataBySlice = new ArrayList<List<TagLevelFeatures>>();
	List<List<TagLevelFeatures>> tmpSlices = Lists.partition(rawData,
		sliceSize);
	/*
	 * copy to raw data by slice, and ensure stragglers in extra slice are
	 * put in prior slice
	 */
	for (int i = 0; i < tmpSlices.size(); i++) {
	    List<TagLevelFeatures> currentSlice = tmpSlices.get(i);
	    if (i == nbrSlices) {
		List<TagLevelFeatures> priorSlice = rawDataBySlice.get(i - 1);
		for (int j = 0; j < currentSlice.size(); j++) {
		    priorSlice.add(currentSlice.get(j));
		}
	    } else {
		List<TagLevelFeatures> newSlice = new ArrayList<TagLevelFeatures>();
		for (int j = 0; j < currentSlice.size(); j++) {
		    newSlice.add(currentSlice.get(j));
		}
		rawDataBySlice.add(newSlice);
	    }
	}
	tmpSlices = null;

	/*
	 * Retrieve randomly-ordered list of concept IDs from map for extracting
	 * training and test data
	 */

	// extract matrix data by slice to facilitate building NN training data
	nnDataByTestSlice = new HashMap<Integer, TagLevelNnData>();
	for (int testSliceNbr = 0; testSliceNbr < nbrSlices; testSliceNbr++) {
	    List<TagLevelFeatures> testDataRecordsForSlice = rawDataBySlice
		    .get(testSliceNbr);
	    DoubleMatrix2D rawTestDataMatrix = convertDataForNn(
		    testDataRecordsForSlice);
	    Pair<DataSet, ListDataSetIterator<DataSet>> testDataAndIter = getModelDataSet(
		    rawTestDataMatrix);
	    TagLevelNnData nnData = new TagLevelNnData();
	    nnData.testData = testDataAndIter.getValue0();
	    nnData.testDataIter = testDataAndIter.getValue1();
	    nnData.rawTestDataMatrix = rawTestDataMatrix;
	    nnData.rawTestData = (List<TagLevelFeatures>) testDataRecordsForSlice;
	    DoubleMatrix2D trainData = null;
	    for (int trainSliceNbr = 0; trainSliceNbr < nbrSlices; trainSliceNbr++) {
		if (testSliceNbr == trainSliceNbr) {
		    continue;
		}
		List<TagLevelFeatures> trainDataRecordsForSlice = rawDataBySlice
			.get(trainSliceNbr);
		DoubleMatrix2D nextTrainData = convertDataForNn(
			trainDataRecordsForSlice);
		if (trainData == null) {
		    trainData = nextTrainData;
		} else {
		    trainData = DoubleFactory2D.dense.appendRows(trainData,
			    nextTrainData);
		}
	    }
	    Pair<DataSet, ListDataSetIterator<DataSet>> trainDataAndIter = getModelDataSet(
		    trainData);
	    nnData.trainingData = trainDataAndIter.getValue0();
	    nnData.trainingDataIter = trainDataAndIter.getValue1();
	    nnDataByTestSlice.put(testSliceNbr, nnData);
	}

    }

    @Override
    protected DoubleMatrix2D convertDataForNn(
	    List<? extends BaseDataRecord> dataRecords) throws Exception {
	int nbrRows = dataRecords.size();
	int nbrColumns = TagLevelFeatures.getNbrOfColumns();
	DoubleMatrix2D dataArray = DoubleFactory2D.dense.make(nbrRows,
		nbrColumns);
	FeatureName[] featureNames = null;
	if (featureComboInputs != null) {
	    featureNames = featureComboInputs.orderedFeatureNames();
	} else if (orderedFeatureNames != null) {
	    featureNames = orderedFeatureNames;
	} else {
	    throw new Exception("Missing feature names");
	}
	for (int row = 0; row < nbrRows; row++) {
	    TagLevelFeatures dataRecord = (TagLevelFeatures) dataRecords
		    .get(row);
	    int column = 0;
	    for (FeatureName featureName : featureNames) {
		double value = 0.0d;
		switch (featureName) {
		case FRACT_CONCEPT_WORDS_MATCHED:
		    value = dataRecord.getFractConceptWordsMatched();
		    break;
		case FRACT_TEXT_WORDS_MATCHED:
		    value = dataRecord.getFractTextWordsMatched();
		    break;
		case FRACT_CONCEPT_ONTOL_IOC:
		    value = dataRecord.getFractConceptIoc();
		    break;
		case FRACT_TEXT_ONTOL_IOC:
		    value = dataRecord.getFractTextIoc();
		    break;
		case FRACT_ABSTR_CORPUS_COGENCY:
		    value = dataRecord.getFractAbstrCogency();
		    break;
		case FRACT_SENT_CORPUS_COGENCY:
		    value = dataRecord.getFractSentCogency();
		    break;
		case IS_CONCEPT_SINGLE_WORD:
		    value = dataRecord.isConceptSingleWord();
		    break;
		case IS_TEXT_SINGLE_WORD:
		    value = dataRecord.isTextSingleWord();
		    break;
		default:
		    break;
		}
		dataArray.set(row, column, value);
		column++;
	    }
	    dataArray.set(row, column, dataRecord.isGoodMatch());
	    column++;
	    if (featureComboOutputs.equals(FeatureCombinationOutputs.TWO)) {
		dataArray.set(row, column, dataRecord.isPoorMatch());
	    }
	}
	return dataArray;
    }

    private Pair<DataSet, ListDataSetIterator<DataSet>> getModelDataSet(
	    DoubleMatrix2D data) throws Exception {

	int nbrRows = data.rows();
	int nbrInputColumns = 0;
	if (featureComboInputs != null) {
	    nbrInputColumns = featureComboInputs.orderedFeatureNames().length;
	} else if (orderedFeatureNames != null) {
	    nbrInputColumns = orderedFeatureNames.length;
	} else {
	    throw new Exception("Missing feature names");
	}

	int nbrOutputColumns = featureComboOutputs.nbrOfFeatures();

	DoubleMatrix2D inputDataView = data.viewPart(0, 0, nbrRows,
		nbrInputColumns);
	double[][] inputDataArray = inputDataView.toArray();

	DoubleMatrix2D outputDataView = data.viewPart(0, nbrInputColumns,
		nbrRows, nbrOutputColumns);
	double[][] outputDataArray = outputDataView.toArray();

	INDArray featuresNDArray = Nd4j.create(inputDataArray);
	INDArray labelsNDArray = Nd4j.create(outputDataArray);

	DataSet dataSet = new DataSet(featuresNDArray, labelsNDArray);

	List<String> labelNames = new ArrayList<String>();
	for (int i = 0; i < nbrOutputColumns; i++) {
	    labelNames.add(
		    featureComboOutputs.orderedFeatureNames()[i].featureName());
	}
	dataSet.setLabelNames(labelNames);
	ListDataSetIterator<DataSet> listDataSetIter = new ListDataSetIterator<DataSet>(
		dataSet.asList(), batchSize);
	return new Pair<DataSet, ListDataSetIterator<DataSet>>(dataSet,
		listDataSetIter);
    }

    public List<TagLevelFeatures> getRawData() {
	return rawData;
    }

    public int getNbrSlices() {
	return nbrSlices;
    }

    public void setNbrSlices(int nbrSlices) {
	this.nbrSlices = nbrSlices;
    }

    public int getSliceSize() {
	return sliceSize;
    }

    public Map<Integer, TagLevelNnData> getNnDataByTestSlice() {
	return nnDataByTestSlice;
    }

    public Corpus getCorpus() {
	return corpus;
    }

    public JdbcConnectionPool getConnPool() {
	return connPool;
    }

    public class TagLevelNnData {

	public List<TagLevelFeatures> rawTestData = null;
	public DoubleMatrix2D rawTestDataMatrix = null;
	public DataSet trainingData = null;
	public ListDataSetIterator<DataSet> trainingDataIter = null;
	public DataSet testData = null;
	public ListDataSetIterator<DataSet> testDataIter = null;

    }

}
