/**
 * 
 */
package edu.mst.conceptrecogn.train;

import java.util.ArrayList;
import java.util.List;

import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.javatuples.Pair;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import cern.colt.matrix.DoubleMatrix2D;
import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureName;
import edu.mst.nn.mlp.BaseMlp;

/**
 * Class for training data common to any MLP.
 * 
 * @author George
 *
 */
public abstract class BaseTrainingData {

    public static final float defaultFractTrain = 0.90f;
    public static final float defaultFractTest = 0.10f;
    public static final int defaultBatchSize = 100;

    protected FeatureName[] orderedFeatureNames = null;
    protected FeatureCombinationInputs featureCombo = null;

    protected int batchSize = 0;

    public BaseTrainingData(int batchSize) {
	this.batchSize = batchSize;
    }

    /**
     * Method to marshal data using default parameters
     * 
     * @throws Exception
     */
    public abstract void marshalNnTrainingData() throws Exception;

    /**
     * Convert the data obtained from the database into a form suitable for use
     * by the MLP library (dl4j).
     * <p>
     * This uses a cross-validation method. The matrix created from the database
     * record consists of rows containing the training and expected outcomes
     * data.
     * </p>
     * 
     * @param dataRecords
     * @return
     * @throws Exception
     */
    protected abstract DoubleMatrix2D convertDataForNn(
	    List<? extends BaseDataRecord> dataRecords) throws Exception;

    protected Pair<DataSet, ListDataSetIterator<DataSet>> getModelDataSet(
	    DoubleMatrix2D data, int nbrInputColumns, int nbrOutputColumns,
	    BaseMlp mlp) {

	int nbrRows = data.rows();

	DoubleMatrix2D inputDataView = data.viewPart(0, 0, nbrRows,
		nbrInputColumns);
	double[][] inputDataArray = inputDataView.toArray();

	DoubleMatrix2D outputDataView = data.viewPart(0, nbrInputColumns,
		nbrRows, nbrOutputColumns);
	double[][] outputDataArray = outputDataView.toArray();

	INDArray featuresNDArray = Nd4j.create(inputDataArray);
	INDArray labelsNDArray = Nd4j.create(outputDataArray);

	DataSet dataSet = new DataSet(featuresNDArray, labelsNDArray);

	List<String> labelNames = new ArrayList<String>();
	for (int i = 0; i < nbrOutputColumns; i++) {
	    labelNames.add(mlp.getColumnNames()[i]);
	}
	dataSet.setLabelNames(labelNames);
	final List<DataSet> list = dataSet.asList();
	ListDataSetIterator<DataSet> listDataSetIter = new ListDataSetIterator<DataSet>(
		list, batchSize);
	return new Pair<DataSet, ListDataSetIterator<DataSet>>(dataSet,
		listDataSetIter);
    }

}
