/**
 * 
 */
package edu.mst.conceptrecogn.train;

/**
 * Placeholder abstract class, does nothing at this time. Created to enable the use of other abstract
 * classes.
 * 
 * @author George
 *
 */
public abstract class BaseDataRecord {

}
