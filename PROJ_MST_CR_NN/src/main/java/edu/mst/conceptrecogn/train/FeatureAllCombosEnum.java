/**
 * 
 */
package edu.mst.conceptrecogn.train;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;

import org.apache.commons.math3.util.CombinatoricsUtils;

import edu.mst.conceptrecogn.FeatureName;

/**
 * Class to generate all possible feature combinations
 * 
 * @author gjs
 *
 */
public class FeatureAllCombosEnum {

    public List<int[]> allCombinations() throws Exception {

	List<int[]> result = new ArrayList<int[]>();

	SortedMap<Integer, FeatureName> inputFeatureByNbr = FeatureName
		.inputFeatureByNbr();
	// last two feature names are output features
	int nbrInputFeatures = inputFeatureByNbr.size();
	for (int i = 1; i <= inputFeatureByNbr.keySet().size(); i++) {
	    //System.out.printf(
		//    "Nbr input features = %1$,d, nbr in combination = %2$,d %n",
		  //  nbrInputFeatures, i);
	    if (i == nbrInputFeatures) {
		int[] combination = new int[nbrInputFeatures];
		Iterator<Integer> featureNbrIter = inputFeatureByNbr.keySet().iterator();
		int pos = 0;
		while(featureNbrIter.hasNext()) {
		    Integer featureNbr = featureNbrIter.next();
		    combination[pos] = featureNbr;
		    pos++;
		}
		Arrays.sort(combination);
		result.add(combination);
	    } else {
		Iterator<int[]> iterator = CombinatoricsUtils
			.combinationsIterator(nbrInputFeatures, i);
		while (iterator.hasNext()) {
		    final int[] combination = iterator.next();
		    Arrays.sort(combination);
		    result.add(combination);
		}
	    }
	}

	return result;
    }

}
