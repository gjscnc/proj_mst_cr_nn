/**
 * 
 */
package edu.mst.conceptrecogn.train;

import java.sql.SQLException;

import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.nn.mlp.MlpResults;
import edu.mst.nn.mlp.MlpResultsSlice;
import edu.mst.nn.mlp.data.MlpResultsDao;

/**
 * Prints results of the MLP training and test to the console. Training and test
 * data are stored in the MySQL database.
 * 
 * @author gjs
 *
 */
public class MlpResultsReport {

    private JdbcConnectionPool connPool = null;

    public MlpResultsReport(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public void listResultsAndSlices() throws SQLException, Exception {

	MlpResultsDao resultsDao = new MlpResultsDao();
	Corpus[] corpuses = Corpus.values();
	FeatureCombinationInputs[] featureCombos = FeatureCombinationInputs.values();
	for (int i = 0; i < featureCombos.length; i++) {
	    FeatureCombinationInputs featureCombo = featureCombos[i];
	    for (int j = 0; j < corpuses.length; j++) {
		Corpus corpus = corpuses[j];
		MlpResults mlpResults = resultsDao.marshalLatest(featureCombo,
			corpus, connPool);
		System.out.printf("%n%nFor corpus '%1$s' %n",
			corpus.toString());
		System.out.printf(
			"Precision = %1$.4f, recall = %2$.4f, "
				+ "F1 = %3$.4f, accuracy = %4$.4f %n",
			mlpResults.getPrecision(), mlpResults.getRecall(),
			mlpResults.getF1(), mlpResults.getAccuracy());
		System.out.printf("By slice %n");
		for (MlpResultsSlice slice : mlpResults.getSlices()) {
		    System.out.printf(
			    "Slice nbr = %1$d, precision = %2$.4f, recall = %3$.4f, "
				    + "F1 = %4$.4f, accuracy = %5$.4f %n",
			    slice.getSliceNbr(), slice.getPrecision(),
			    slice.getRecall(), slice.getF1(),
			    slice.getAccuracy());
		}
	    }
	}
    }

    public void listResults() throws SQLException, Exception {

	MlpResultsDao resultsDao = new MlpResultsDao();
	Corpus[] corpuses = Corpus.values();
	FeatureCombinationInputs[] featureCombos = FeatureCombinationInputs.values();
	for (int i = 0; i < featureCombos.length; i++) {
	    FeatureCombinationInputs featureCombo = featureCombos[i];
	    for (int j = 0; j < corpuses.length; j++) {
		Corpus corpus = corpuses[j];
		MlpResults mlpResults = resultsDao.marshalLatest(featureCombo,
			corpus, connPool);
		System.out.printf("%n%nFor corpus %1$s %n", corpus.toString());
		System.out.printf(
			"Precision = %1$.4f, recall = %2$.4f, "
				+ "F1 = %3$.4f, accuracy = %4$.4f %n",
			mlpResults.getPrecision(), mlpResults.getRecall(),
			mlpResults.getF1(), mlpResults.getAccuracy());
	    }
	}
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    MlpResultsReport report = new MlpResultsReport(connPool);
	    report.listResultsAndSlices();

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}
