/**
 * 
 */
package edu.mst.conceptrecogn.train;

/**
 * Evaluator for performance of MLP.
 * <p>
 * Assumes one label classified as true or false, i.e., a 2x2 matrix.
 * </p>
 * 
 * @author George J. Shannon
 *
 */
public class Performance {

    private double tp = 0;
    private double fp = 0;
    private double tn = 0;
    private double fn = 0;
    private double p = 0.0d;
    private double r = 0.0d;
    private double f1 = 0.0d;
    private double a = 0.0d;
    private double mcc = 0.0d;

    public void compute() {
	a = (tp + tn) / (tp + tn + fp + fn);
	p = tp / (tp + fp);
	r = tp / (tp + fn);
	f1 = 2 * p * r / (p + r);
	double mccDenom = (tp + fp) * (tp + fn) * (tn + fp) * (tn + fn);
	mccDenom = Math.sqrt(mccDenom);
	double mccNumer = (tp * tn) - (fp * fn);
	mcc = mccNumer / mccDenom;
    }

    public void incrementTp() {
	tp += 1.0d;
    }

    public void incrementFp() {
	fp += 1.0d;
    }

    public void incrementTn() {
	tn += 1.0d;
    }

    public void incrementFn() {
	fn += 1.0d;
    }

    public double getTp() {
        return tp;
    }

    public void setTp(double tp) {
        this.tp = tp;
    }

    public double getFp() {
        return fp;
    }

    public void setFp(double fp) {
        this.fp = fp;
    }

    public double getTn() {
        return tn;
    }

    public void setTn(double tn) {
        this.tn = tn;
    }

    public double getFn() {
        return fn;
    }

    public void setFn(double fn) {
        this.fn = fn;
    }

    public double getPrecision() {
        return p;
    }

    public void setPrecision(double p) {
        this.p = p;
    }

    public double getRecall() {
        return r;
    }

    public void setRecall(double r) {
        this.r = r;
    }

    public double getF1() {
        return f1;
    }

    public void setF1(double f1) {
        this.f1 = f1;
    }

    public double getAccuracy() {
        return a;
    }

    public void setAccuracy(double a) {
        this.a = a;
    }

    public double getMcc() {
        return mcc;
    }

    public void setMcc(double mcc) {
        this.mcc = mcc;
    }


}
