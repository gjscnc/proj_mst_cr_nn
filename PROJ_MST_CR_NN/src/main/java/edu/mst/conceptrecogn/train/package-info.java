/**
 * 
 */
/**
 * Components for training neural network for concept recognition.
 * 
 * @author gjs
 *
 */
package edu.mst.conceptrecogn.train;