package edu.mst.conceptrecogn;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Note: feature number is zero-based
 * 
 * @author gjs
 *
 */
public enum FeatureName {
    
    IS_CONCEPT_SINGLE_WORD("isConceptSingleWord", 0),
    IS_TEXT_SINGLE_WORD("isTextSingleWord", 1),
    FRACT_CONCEPT_WORDS_MATCHED("fractConceptWordsMatched", 2),
    FRACT_TEXT_WORDS_MATCHED("fractTextWordsMatched", 3),
    IS_CONCEPT_SYNONYM("isConceptSynonym", 4),
    FRACT_CONCEPT_ONTOL_IOC("fractConceptIOC", 5),
    FRACT_TEXT_ONTOL_IOC("fractTextIOC", 6),
    FRACT_ABSTR_CORPUS_COGENCY("fractAbstrCorpusCogency", 7),
    FRACT_SENT_CORPUS_COGENCY("fractSentCorpusCogency", 8),
    IS_GOOD_MATCH("isGoodMatch", 8), // output field
    IS_POOR_MATCH("isPoorMatch", 9); // output field
    
    private String name = null;
    private int featureNbr;
    private static SortedMap<Integer, FeatureName> inputFeatureByNbr;
    
    private FeatureName(String name, int featureNbr) {
	this.name = name;
	this.featureNbr = featureNbr;
    }
    
    public String featureName() {
	return name;
    }
    
    public int featureNbr() {
	return featureNbr;
    }
    
    public static SortedMap<Integer, FeatureName> inputFeatureByNbr() {
	if (inputFeatureByNbr == null) {
	    inputFeatureByNbr = new TreeMap<Integer, FeatureName>();
	    for (FeatureName name : FeatureName.values()) {
		if (name.equals(FeatureName.IS_GOOD_MATCH)
			|| name.equals(FeatureName.IS_POOR_MATCH)) {
		    continue;
		}
		inputFeatureByNbr.put(name.featureNbr, name);
	    }
	}
	return inputFeatureByNbr;
    }
    
    public static String orderedFeatureComboToStr(
	    FeatureName[] orderedFeatureCombo) {
	StringBuilder builder = new StringBuilder();
	for (int i = 0; i < orderedFeatureCombo.length; i++) {
	    builder.append(orderedFeatureCombo[i].featureNbr);
	    if (i < orderedFeatureCombo.length - 1) {
		builder.append(",");
	    }
	}
	return builder.toString();
    }

}
