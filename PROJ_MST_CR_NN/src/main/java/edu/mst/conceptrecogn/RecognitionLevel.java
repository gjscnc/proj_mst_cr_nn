package edu.mst.conceptrecogn;

public enum RecognitionLevel {
    ANNOTATION_TAG, ANNOTATION_PHRASE;
}
