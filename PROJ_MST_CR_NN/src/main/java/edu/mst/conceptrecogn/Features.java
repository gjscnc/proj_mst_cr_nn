package edu.mst.conceptrecogn;

public interface Features {
    
    public double getFractConceptWordsMatched();
    public double getFractTextWordsMatched();
    public double getFractConceptIoc();
    public double getFractTextIoc();
    public double getFractAbstrCogency();
    public double getFractSentCogency();
    public double isConceptSingleWord();
    public double isTextSingleWord();
    public double isGoodMatch();
    public double isPoorMatch();

}
