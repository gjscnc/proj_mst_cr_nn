/**
 * 
 */
package edu.mst.conceptrecogn;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;
import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver;
import org.deeplearning4j.earlystopping.scorecalc.DataSetLossCalculator;
import org.deeplearning4j.earlystopping.termination.EpochTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxTimeIterationTerminationCondition;
import org.deeplearning4j.earlystopping.termination.ScoreImprovementEpochTerminationCondition;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.javatuples.Pair;
import org.nd4j.linalg.api.ndarray.INDArray;

import edu.mst.conceptrecogn.taglevel.data.TagLevelFeatures;
import edu.mst.conceptrecogn.train.BaseTrainingData;
import edu.mst.conceptrecogn.train.MlpKFoldData;
import edu.mst.conceptrecogn.train.Performance;
import edu.mst.conceptrecogn.train.MlpKFoldData.TagLevelNnData;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagDao;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.nn.mlp.BaseMlp;
import edu.mst.nn.mlp.MlpResults;
import edu.mst.nn.mlp.MlpResultInstance;
import edu.mst.nn.mlp.MlpResultsSlice;
import edu.mst.nn.mlp.data.MlpResultInstanceDao;
import edu.mst.nn.mlp.data.MlpResultsDao;
import edu.mst.nn.mlp.data.MlpResultsSliceDao;
import edu.mst.nn.mlp.data.MlpWgtsDao;

/**
 * This class uses the MLP from DL4J for determining whether or not a candidate
 * concept is a good fit for a phrase.
 * 
 * @author George
 *
 */
public class ConceptRecognMlp extends BaseMlp {

    public static String earlyStopModelDir = "/home/gjs/Documents/workspaceSPS_MySQL/nn2/src/main/resource/";

    private RecognitionLevel recogitionLevel = null;
    private FeatureCombinationInputs featureComboInputs = null;
    private FeatureCombinationOutputs featureComboOutputs = null;
    private FeatureName[] orderedFeatureComboInputs = null;
    private String featureComboStr = null;
    private Corpus corpus = null;

    private Map<Integer, TagLevelNnData> nnDataByTestSliceNbr = null;
    private Map<Integer, List<MlpResultInstance>> instancesBySliceNbr = new HashMap<Integer, List<MlpResultInstance>>();
    private MlpResults mlpResults = null;
    private Map<String, SortedMap<String, GoldStdTag>> falseResultsByType = new HashMap<String, SortedMap<String, GoldStdTag>>();
    public static final String falsePositiveName = "fp";
    public static final String falseNegativeName = "fn";

    private GoldStdTagDao tagDao = new GoldStdTagDao();

    public ConceptRecognMlp(FeatureName[] orderedFeatureComboInputs,
	    FeatureCombinationOutputs featureComboOutputs,
	    RecognitionLevel recogitionLevel) throws Exception {
	this.recogitionLevel = recogitionLevel;
	this.orderedFeatureComboInputs = orderedFeatureComboInputs;
	this.featureComboOutputs = featureComboOutputs;
	featuresInit();
    }

    public ConceptRecognMlp(FeatureCombinationInputs featureComboInputs,
	    FeatureCombinationOutputs featureComboOutputs,
	    RecognitionLevel recognitionLevel) throws Exception {
	this.recogitionLevel = recognitionLevel;
	this.featureComboInputs = featureComboInputs;
	this.featureComboOutputs = featureComboOutputs;
	featuresInit();
    }

    private void featuresInit() throws Exception {
	if (featureComboInputs != null) {
	    nbrInputs = featureComboInputs.nbrOfFeatures();
	    featureComboStr = featureComboInputs.toString();
	} else if (orderedFeatureComboInputs != null) {
	    nbrInputs = orderedFeatureComboInputs.length;
	    featureComboStr = FeatureName
		    .orderedFeatureComboToStr(orderedFeatureComboInputs);
	} else {
	    throw new Exception("Missing feature names");
	}
	nbrOutputs = featureComboOutputs.nbrOfFeatures();
	marshalColumnNames();
	nbrHiddenNodes = BaseMlp.defaultNbrHiddenNodes;

    }

    @Override
    protected void marshalColumnNames() throws Exception {
	columnNames = new String[nbrInputs + nbrOutputs];
	int i = 0;
	if (featureComboInputs != null) {
	    for (FeatureName name : featureComboInputs.orderedFeatureNames()) {
		columnNames[i] = name.featureName();
		i++;
	    }
	} else if (orderedFeatureComboInputs != null) {
	    for (FeatureName name : orderedFeatureComboInputs) {
		columnNames[i] = name.featureName();
		i++;
	    }
	} else {
	    throw new Exception("No features specified");
	}
	for (FeatureName name : featureComboOutputs.orderedFeatureNames()) {
	    columnNames[i] = name.featureName();
	    i++;
	}
    }

    @Override
    public void train(BaseTrainingData trainingData) throws Exception {

    }

    /**
     * Train network using the k-fold approach.
     * <p>
     * <b>IMPORTANT NOTE</b>: This method resets network weights for training
     * and test over each of the k-folds. Only the weights for the last model
     * trained are retained and persisted in the database.
     * </p>
     * 
     * @param trainingData
     * @param isUseEarlyStopping
     * @param isPersistInstances
     *                               Persist results for each individual
     *                               training record to the database
     * @throws Exception
     */
    public void kFoldTraining(MlpKFoldData trainingData,
	    boolean isUseEarlyStopping, boolean isPersistInstances)
	    throws Exception {

	corpus = trainingData.getCorpus();

	/*
	 * Iterate over slices and train, including computing precision.
	 * Training occurs for the specified number of epochs for each slice
	 * combination.
	 */
	nnDataByTestSliceNbr = trainingData.getNnDataByTestSlice();
	mlpResults = new MlpResults();
	mlpResults.setFeatureCombo(featureComboStr);
	String datetime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss")
		.format(new Date());

	int nbrSlices = trainingData.getNbrSlices();
	for (int sliceNbr = 0; sliceNbr < nbrSlices; sliceNbr++) {

	    System.out.printf("Training and testing at slice number %1$d %n",
		    sliceNbr);
	    System.out.printf(
		    "Using early stopping = %1$b, max nbr epochs = %2$,d %n",
		    isUseEarlyStopping, nbrEpochs);
	    TagLevelNnData nnData = nnDataByTestSliceNbr.get(sliceNbr);
	    // re-initialize for this training slice
	    init();
	    // train
	    if (isUseEarlyStopping) {
		List<EpochTerminationCondition> earlyStopConditions = new ArrayList<EpochTerminationCondition>();
		earlyStopConditions
			.add(new MaxEpochsTerminationCondition(nbrEpochs));
		earlyStopConditions
			.add(new ScoreImprovementEpochTerminationCondition(
				maxEpochsScoreImprovementTerminationCondition,
				scoreImprovementTerminationCondition));
		EarlyStoppingConfiguration<MultiLayerNetwork> esConf = new EarlyStoppingConfiguration.Builder<MultiLayerNetwork>()
			.epochTerminationConditions(earlyStopConditions)
			.iterationTerminationConditions(
				new MaxTimeIterationTerminationCondition(60,
					TimeUnit.MINUTES))
			.scoreCalculator(new DataSetLossCalculator(
				nnData.testDataIter, true))
			.evaluateEveryNEpochs(1)
			.modelSaver(new LocalFileModelSaver(earlyStopModelDir))
			.build();

		trainer = new EarlyStoppingTrainer(esConf, conf,
			nnData.trainingDataIter);

		// Conduct early stopping training:
		EarlyStoppingResult<MultiLayerNetwork> result = trainer.fit();

		// Print out the results:
		System.out.println(
			"Termination reason: " + result.getTerminationReason());
		System.out.println("Termination details: "
			+ result.getTerminationDetails());
		System.out.println("Total epochs: " + result.getTotalEpochs());
		System.out.println(
			"Best epoch number: " + result.getBestModelEpoch());
		System.out.println(
			"Score at best epoch: " + result.getBestModelScore());

		mlp = result.getBestModel();

	    } else {
		for (int epoch = 1; epoch <= nbrEpochs; epoch++) {
		    nnData.trainingDataIter.reset();
		    mlp.fit(nnData.trainingDataIter);
		}
	    }

	    Performance slicePerf = new Performance();
	    nnData.testDataIter.reset();
	    INDArray predicted = mlp.output(nnData.testDataIter);
	    INDArray actual = nnData.testData.getLabels();
	    List<MlpResultInstance> instances = new ArrayList<MlpResultInstance>();
	    for (int rowNbr = 0; rowNbr < predicted.rows(); rowNbr++) {
		INDArray predictionRow = predicted.getRow(rowNbr);
		double probGoodFit = predictionRow.getDouble(0);
		double probPoorFit = 0.0d;
		if (featureComboOutputs.equals(FeatureCombinationOutputs.ONE)) {
		    probPoorFit = 1.0d - probGoodFit;
		} else {
		    probPoorFit = predictionRow.getDouble(1);
		}
		boolean isPredictedGoodFit = probGoodFit > probPoorFit;
		boolean isPredictedPoorFit = !isPredictedGoodFit;
		INDArray actualRow = actual.getRow(rowNbr);
		boolean isGoodFit = actualRow.getDouble(0) > 0.0d;
		// boolean isPoorFit = !isGoodFit;
		TagLevelFeatures testRecord = nnData.rawTestData.get(rowNbr);
		Pair<Integer, String> idPair = testRecord
			.parseGoldStdPmidTagId();
		int pmid = idPair.getValue0();
		String tagId = idPair.getValue1();
		GoldStdTag tag = tagDao.marshal(pmid, tagId,
			trainingData.getConnPool());
		String falseResultType = null;
		MlpResultInstance instance = new MlpResultInstance();
		instance.setFeatureCombo(featureComboStr);
		instance.setSourceCorpus(testRecord.getCorpus().toString());
		instance.setOutputSingleNeuron(featureComboOutputs
			.equals(FeatureCombinationOutputs.ONE));
		instance.setDatetime(datetime);
		instance.setPmid(pmid);
		instance.setTagId(tagId);
		instance.setConceptNameUid(testRecord.getConceptNameUid());
		instance.setConceptUid(testRecord.getConceptUid());
		instance.setTrainingDataId(testRecord.getId());
		instance.setTruePositive(false);
		instance.setFalsePositive(false);
		instance.setTrueNegative(false);
		instance.setFalseNegative(false);
		if (isGoodFit) {
		    // actual true
		    if (isPredictedGoodFit) {
			slicePerf.incrementTp(); // predicted true tp
			instance.setTruePositive(true);
		    } else {
			slicePerf.incrementFn(); // predicted false fn
			instance.setFalseNegative(true);
			falseResultType = falseNegativeName;
		    }
		} else {
		    // actual false
		    if (isPredictedPoorFit) {
			slicePerf.incrementTn(); // predicate false tn
			instance.setTrueNegative(true);
		    } else {
			slicePerf.incrementFp(); // predicted true fp
			instance.setFalsePositive(true);
			falseResultType = falsePositiveName;
		    }
		}
		instances.add(instance);
		if (falseResultType != null) {
		    if (!falseResultsByType.containsKey(falseResultType)) {
			falseResultsByType.put(falseResultType,
				new TreeMap<String, GoldStdTag>());
		    }
		    Map<String, GoldStdTag> annotCuiByText = falseResultsByType
			    .get(falseResultType);
		    String text = tag.getTaggedText();
		    if (!annotCuiByText.containsKey(text)) {
			annotCuiByText.put(text, tag);
		    }
		}
	    }

	    instancesBySliceNbr.put(sliceNbr, instances);

	    slicePerf.compute();

	    MlpResultsSlice slice = new MlpResultsSlice();
	    slice.setFeatureCombo(featureComboStr);
	    slice.setSourceCorpus(corpus);
	    boolean isOutputSingleNeuron = featureComboOutputs
		    .equals(FeatureCombinationOutputs.ONE);
	    slice.setOutputSingleNeuron(isOutputSingleNeuron);
	    slice.setDatetime(datetime);
	    slice.setSliceNbr(sliceNbr);
	    slice.setPrecision(slicePerf.getPrecision());
	    slice.setRecall(slicePerf.getRecall());
	    slice.setF1(slicePerf.getF1());
	    slice.setAccuracy(slicePerf.getAccuracy());
	    slice.setMcc(slicePerf.getMcc());
	    int sliceTp = (int) slicePerf.getTp();
	    int sliceFn = (int) slicePerf.getFn();
	    int sliceFp = (int) slicePerf.getFp();
	    int sliceTn = (int) slicePerf.getTn();
	    int sliceNTest = sliceTp + sliceFn + sliceFp + sliceTn;
	    slice.setTp(sliceTp);
	    slice.setFn(sliceFn);
	    slice.setFp(sliceFp);
	    slice.setTn(sliceTn);
	    slice.setNTrain(nnData.trainingData.numExamples());
	    slice.setNTest(sliceNTest);
	    mlpResults.getSlices().add(slice);

	    System.out.printf(
		    "%nResults for feature combination %1$s, slice = %2$d: %n",
		    featureComboStr, sliceNbr);
	    System.out.printf("%1$,d training examples, %2$,d test examples %n",
		    nnData.trainingData.numExamples(), sliceNTest);
	    System.out.printf(
		    "Actual labeled as 'true' predicted as 'true': %1$,d times (tp) %n",
		    sliceTp);
	    System.out.printf(
		    "Actual labeled as 'true' predicted as 'false': %1$,d times (fn) %n",
		    sliceFn);
	    System.out.printf(
		    "Actual labeled as 'false' predicted as 'true': %1$,d times (fp) %n",
		    sliceFp);
	    System.out.printf(
		    "Actual labeled as 'false' predicted as 'false': %1$,d times (tn) %n",
		    sliceTn);
	    System.out.printf(
		    "%n%n=======================Performance Results=====================%n");
	    System.out.printf("Accuracy:       %1$.4f %n",
		    slicePerf.getAccuracy());
	    System.out.printf("Precision:      %1$.4f %n",
		    slicePerf.getPrecision());
	    System.out.printf("Recall:         %1$.4f %n",
		    slicePerf.getRecall());
	    System.out.printf("F1:             %1$.4f %n", slicePerf.getF1());
	    System.out.printf("MCC:            %1$.4f %n", slicePerf.getMcc());
	    System.out.printf(
		    "===============================================================%n%n");

	}

	// print out precision numbers
	System.out.printf(
		"%n%nTraining finished for feature combination %1$s %n",
		featureComboStr);
	System.out.printf("%nPrecision, recall, F1, and accuracy results: %n");
	int aggrTp = 0;
	int aggrFn = 0;
	int aggrFp = 0;
	int aggrTn = 0;
	int nbrOfSlices = mlpResults.getSlices().size();
	for (int sliceNbr = 0; sliceNbr < nbrOfSlices; sliceNbr++) {
	    MlpResultsSlice slice = mlpResults.getSlices().get(sliceNbr);
	    System.out.printf(
		    "For slice number %1$d precision = %2$.4f, recall = %3$.4f, "
			    + "f1 = %4$.4f, accuracy = %5$.4f, MCC = %6$.4f %n",
		    sliceNbr, slice.getPrecision(), slice.getRecall(),
		    slice.getF1(), slice.getAccuracy(), slice.getMcc());
	    aggrTp += slice.getTp();
	    aggrFn += slice.getFn();
	    aggrFp += slice.getFp();
	    aggrTn += slice.getTn();
	}
	Performance aggrPerf = new Performance();
	aggrPerf.setTp((double) aggrTp);
	aggrPerf.setFn((double) aggrFn);
	aggrPerf.setFp((double) aggrFp);
	aggrPerf.setTn((double) aggrTn);
	aggrPerf.compute();
	mlpResults.setSourceCorpus(corpus);
	boolean isOutputSingleNeuron = featureComboOutputs
		.equals(FeatureCombinationOutputs.ONE);
	mlpResults.setOutputSingleNeuron(isOutputSingleNeuron);
	mlpResults.setDatetime(datetime);
	mlpResults.setPrecision(aggrPerf.getPrecision());
	mlpResults.setRecall(aggrPerf.getRecall());
	mlpResults.setF1(aggrPerf.getF1());
	mlpResults.setAccuracy(aggrPerf.getAccuracy());
	mlpResults.setMcc(aggrPerf.getMcc());
	int nTest = aggrTp + aggrFn + aggrFp + aggrTn;
	mlpResults.setNTest(nTest);
	mlpResults.setTp(aggrTp);
	mlpResults.setFn(aggrFn);
	mlpResults.setFp(aggrFp);
	mlpResults.setTn(aggrTn);

	System.out.printf("%n%nOverall Results%n");
	System.out.printf("%1$,d test examples %n", nTest);
	System.out.printf(
		"Actual labeled as 'true' predicted as 'true': %1$,d times (tp) %n",
		aggrTp);
	System.out.printf(
		"Actual labeled as 'true' predicted as 'false': %1$,d times (fn) %n",
		aggrFn);
	System.out.printf(
		"Actual labeled as 'false' predicted as 'true': %1$,d times (fp) %n",
		aggrFp);
	System.out.printf(
		"Actual labeled as 'false' predicted as 'false': %1$,d times (tn) %n",
		aggrTn);
	System.out.printf(
		"%n%n===================Overall Performance Results=================%n");
	System.out.printf("Accuracy:       %1$.4f %n",
		mlpResults.getAccuracy());
	System.out.printf("Precision:      %1$.4f %n",
		mlpResults.getPrecision());
	System.out.printf("Recall:         %1$.4f %n", mlpResults.getRecall());
	System.out.printf("F1:             %1$.4f %n", mlpResults.getF1());
	System.out.printf("MCC:            %1$.4f %n", mlpResults.getMcc());
	System.out.printf(
		"===============================================================%n%n");

	System.out.printf("Total of %1$,d training exemplars %n",
		trainingData.getRawData().size());
    }

    public void persistResults(JdbcConnectionPool connPool)
	    throws SQLException, Exception {

	MlpResultsSliceDao sliceDao = new MlpResultsSliceDao();
	for (MlpResultsSlice slice : mlpResults.getSlices()) {
	    sliceDao.persist(slice, connPool);
	}

	MlpResultsDao resultsDao = new MlpResultsDao();
	resultsDao.persist(mlpResults, connPool);

	MlpResultInstanceDao instanceDao = new MlpResultInstanceDao();
	Iterator<Integer> sliceNbrIter = instancesBySliceNbr.keySet()
		.iterator();
	while (sliceNbrIter.hasNext()) {
	    Integer sliceNbr = sliceNbrIter.next();
	    instanceDao.persistBatch(instancesBySliceNbr.get(sliceNbr),
		    connPool);
	}

    }

    public void persistWeights(JdbcConnectionPool connPool) throws Exception {
	MlpWgtsDao mlpDao = null;
	if (featureComboInputs != null) {
	    mlpDao = new MlpWgtsDao(corpus, featureComboInputs,
		    featureComboOutputs, connPool);
	} else if (orderedFeatureComboInputs != null) {
	    mlpDao = new MlpWgtsDao(corpus, orderedFeatureComboInputs,
		    featureComboOutputs, connPool);
	} else {
	    throw new Exception("Feature names missing");
	}
	mlpDao.persistMLP(mlp);
    }

    public void printWeights() {
	Layer[] layers = mlp.getLayers();
	for (int i = 0; i < layers.length; i++) {
	    Layer layer = layers[i];
	    Map<String, INDArray> paramMap = layer.paramTable();
	    System.out.printf("%nParameters for layer = %1$d %n", i);
	    for (String paramKey : paramMap.keySet()) {
		System.out.printf("Param Key = %1$s%n", paramKey);
		INDArray values = layer.getParam(paramKey);
		for (int j = 0; j < values.rows(); j++) {
		    System.out.println(values.getRow(j));
		}
	    }
	}
    }

    public Map<String, SortedMap<String, GoldStdTag>> getFalseResultsByType() {
	return falseResultsByType;
    }

    public FeatureCombinationInputs getFeatureComboInputs() {
	return featureComboInputs;
    }

    public FeatureCombinationOutputs getFeatureComboOutputs() {
	return featureComboOutputs;
    }

    public FeatureName[] getOrderedFeatureCombo() {
	return orderedFeatureComboInputs;
    }

    public RecognitionLevel getRecognitionLevel() {
	return recogitionLevel;
    }

}
