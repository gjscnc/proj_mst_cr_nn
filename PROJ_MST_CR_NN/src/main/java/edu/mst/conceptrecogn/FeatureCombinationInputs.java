package edu.mst.conceptrecogn;

public enum FeatureCombinationInputs {

    FRACT_WORDS_MATCHED(new FeatureName[]{
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED}),
    BASE(new FeatureName[]{
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED,
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD}), 
    CONCEPT_IOC_ONLY(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_ONTOL_IOC,
	    FeatureName.IS_CONCEPT_SYNONYM}), 
    TEXT_IOC_ONLY(new FeatureName[] {
	    FeatureName.FRACT_TEXT_ONTOL_IOC,
	    FeatureName.IS_CONCEPT_SYNONYM}), 
    IOC_ONLY(new FeatureName[] { 
	    FeatureName.FRACT_CONCEPT_ONTOL_IOC, 
	    FeatureName.FRACT_TEXT_ONTOL_IOC,
	    FeatureName.IS_CONCEPT_SYNONYM}), 
    BASE_PLUS_CONCEPT_IOC(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED, 
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD,
	    FeatureName.FRACT_CONCEPT_ONTOL_IOC,
	    FeatureName.IS_CONCEPT_SYNONYM}), 
    BASE_PLUS_TEXT_IOC(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED, 
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD,
	    FeatureName.FRACT_TEXT_ONTOL_IOC,
	    FeatureName.IS_CONCEPT_SYNONYM}), 
    BASE_PLUS_IOC(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED,
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD,
	    FeatureName.FRACT_CONCEPT_ONTOL_IOC, 
	    FeatureName.FRACT_TEXT_ONTOL_IOC,
	    FeatureName.IS_CONCEPT_SYNONYM}), 
    CORPORA_ABSTR_COGENCY_ONLY(new FeatureName[] {
	    FeatureName.FRACT_ABSTR_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM}),
    CORPORA_SENT_COGENCY_ONLY(new FeatureName[] {
	    FeatureName.FRACT_SENT_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM}),
    CORPORA_COGENCY_ONLY(new FeatureName[] {
	    FeatureName.FRACT_ABSTR_CORPUS_COGENCY, 
	    FeatureName.FRACT_SENT_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM}),
    BASE_PLUS_ABSTR_CORPORA_COGENCY(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED, 
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD,
	    FeatureName.FRACT_ABSTR_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM}),
    BASE_PLUS_SENT_CORPORA_COGENCY(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED, 
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD, 
	    FeatureName.FRACT_SENT_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM}),
    BASE_PLUS_CORPORA_COGENCY(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED, 
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD,
	    FeatureName.FRACT_ABSTR_CORPUS_COGENCY, 
	    FeatureName.FRACT_SENT_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM}),
    CORPORA_COGENCY_PLUS_IOC(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_ONTOL_IOC, 
	    FeatureName.FRACT_TEXT_ONTOL_IOC,
	    FeatureName.FRACT_ABSTR_CORPUS_COGENCY, 
	    FeatureName.FRACT_SENT_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM}),
    CORPORA_COGENCY_IOC_FRACT_WORDS_MATCHED(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_ONTOL_IOC, 
	    FeatureName.FRACT_TEXT_ONTOL_IOC, 
	    FeatureName.FRACT_ABSTR_CORPUS_COGENCY,
	    FeatureName.FRACT_SENT_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM,
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED}), 
    CORPORA_COGENCY_IOC_IS_SINGLE_WORD(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_ONTOL_IOC, 
	    FeatureName.FRACT_TEXT_ONTOL_IOC, 
	    FeatureName.FRACT_ABSTR_CORPUS_COGENCY,
	    FeatureName.FRACT_SENT_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM,
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD}), 
    ALL(new FeatureName[] {
	    FeatureName.FRACT_CONCEPT_ONTOL_IOC, 
	    FeatureName.FRACT_TEXT_ONTOL_IOC, 
	    FeatureName.FRACT_ABSTR_CORPUS_COGENCY,
	    FeatureName.FRACT_SENT_CORPUS_COGENCY,
	    FeatureName.IS_CONCEPT_SYNONYM,
	    FeatureName.FRACT_CONCEPT_WORDS_MATCHED, 
	    FeatureName.FRACT_TEXT_WORDS_MATCHED,
	    FeatureName.IS_CONCEPT_SINGLE_WORD, 
	    FeatureName.IS_TEXT_SINGLE_WORD});

    private int nbrOfFeatures = 0;
    private FeatureName[] orderedFeatureNames = null;

    FeatureCombinationInputs(FeatureName[] orderedFeatureNames) {
	this.nbrOfFeatures = orderedFeatureNames.length;
	this.orderedFeatureNames = orderedFeatureNames;
    }

    public int nbrOfFeatures() {
	return nbrOfFeatures;
    }

    public FeatureName[] orderedFeatureNames() {
	return orderedFeatureNames;
    }

}
