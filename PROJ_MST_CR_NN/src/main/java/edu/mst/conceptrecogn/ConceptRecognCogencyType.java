/**
 * 
 */
package edu.mst.conceptrecogn;

/**
 * Enumerates the cogency type to calculate when calculating matched cogency.
 * 
 * @author gjs
 *
 */
public enum ConceptRecognCogencyType {
    
    ABSTRACT, SENTENCE;

}
