/**
 * 
 */
package edu.mst.conceptrecogn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;

import edu.mst.conceptrecogn.taglevel.data.TrainingRecord;
import edu.mst.db.nlm.corpora.Cogency;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.ontol.ConceptDao;
import edu.mst.db.ontol.cogency.OntolCogency;
import edu.mst.db.ontol.cogency.OntolCogencyDao;
import edu.mst.db.ontol.cogency.OntolCogencyVariant;
import edu.mst.db.ontol.cogency.OntolCogencyVariantDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordProb;
import edu.mst.db.ontol.words.condprob.OntolCondWordProbDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordVariantProb;
import edu.mst.db.ontol.words.condprob.OntolCondWordVariantProbDao;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.util.Constants;

/**
 * Performs feature calculations for each concept candidate.
 * 
 * @author gjs
 *
 */
public class FeatureCalculator {

    public static final double trueAsDbl = 1.0d;
    public static final double falseAsDbl = 0.0d;

    private JdbcConnectionPool connPool = null;

    private GoldStdTag tag;

    private ConceptDao conceptDao = new ConceptDao();
    private NlmAbstractWordDao abstrWordDao = new NlmAbstractWordDao();
    private OntolCogencyDao ontolCogencyDao = new OntolCogencyDao();
    private OntolCogencyVariantDao ontolCogVariantDao = new OntolCogencyVariantDao();
    private OntolCondWordProbDao ontolCondWordProbDao = new OntolCondWordProbDao();
    private OntolCondWordVariantProbDao ontolCondWordVarProbDao = new OntolCondWordVariantProbDao();

    private SortedMap<Integer, Cogency> cogencyByPos = new TreeMap<Integer, Cogency>();

    public FeatureCalculator(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public void compute(TrainingRecord trainingRecord) throws Exception {

	tag = trainingRecord.getGoldStdTag();

	if (trainingRecord.getTextPredWordPos() == Constants.uninitializedIntVal
		|| trainingRecord
			.getConceptPredWordPos() == Constants.uninitializedIntVal
		|| trainingRecord
			.getTextPredWordId() == Constants.uninitializedIntVal) {
	    String errMsg = String.format(
		    "Predicate word not found, cannot compute feature values. "
			    + "Gold std tag PMID = %1$d, tag ID = %2$s, text = %3$s, CUI = %4$s, "
			    + "text predicate pos = %5$d, predicate lex base word ID = %6$d, "
			    + "concept predicate pos = %7$d, concept UID = %8$d, concept name UID = %9$d %n",
		    tag.getPmid(), tag.getTagId(), tag.getTaggedText(),
		    tag.getCui(), trainingRecord.getTextPredWordPos(),
		    trainingRecord.getTextPredWordId(),
		    trainingRecord.getConceptPredWordPos(),
		    trainingRecord.getConceptUid(),
		    trainingRecord.getConceptNameUid());
	    System.out.printf(errMsg);
	    throw new Exception(errMsg);
	}

	/*
	 * If both the text and concept are single-word, set feature values and
	 * return. If text is single-word, but concept is not, check to see if
	 * the concept name is a synonym.
	 */
	if (trainingRecord.isTextSingleWord() == 1.0) {
	    if (trainingRecord.isConceptSingleWord() == 1.0) {
		computeSingleWordFeatures(trainingRecord);
		return;
	    } else {
		/*
		 * See if concept is a synonym to text token
		 */
		boolean isConceptSynonym = isConceptSynonym(trainingRecord);
		if (isConceptSynonym) {
		    setConceptSynonymFeatures(trainingRecord);
		    return;
		}
	    }
	}

	/*
	 * Extract cogencies for the sentence for use when computing maximum and
	 * matched cogencies for the text and candidate concept. Cogencies for
	 * sentence also contain cogencies for the complete abstract.
	 */
	for (Cogency cogency : abstrWordDao.getSentCogencies(tag.getPmid(),
		trainingRecord.getSentNbr(), connPool)) {
	    cogencyByPos.put(cogency.getWordNbr(), cogency);
	}

	/*
	 * Compute max IOC values
	 */
	for (ConceptRecognTextType textType : ConceptRecognTextType.values()) {
	    computeMaxIoc(trainingRecord, textType);
	}

	/*
	 * Computed matched IOC values
	 */
	for (ConceptRecognTextType textType : ConceptRecognTextType.values()) {
	    computeMatchedIoc(trainingRecord, textType);
	}

	/*
	 * Compute max cogency values
	 */
	computeMaxDeltaCogencies(trainingRecord);

	/*
	 * compute text matched cogency values
	 */
	computeMatchedDeltaCogencies(trainingRecord);

	/*
	 * compute fraction words mapped
	 */
	for (ConceptRecognTextType textType : ConceptRecognTextType.values()) {
	    computeFractWordsMatched(trainingRecord, textType);
	}

	/*
	 * Fraction IOC matched
	 */
	for (ConceptRecognTextType textType : ConceptRecognTextType.values()) {
	    computeFractIocMatched(trainingRecord, textType);
	}

	/*
	 * Fraction corpora cogency matched
	 */
	for (ConceptRecognCogencyType cogencyType : ConceptRecognCogencyType
		.values()) {
	    computeFractCogencyMatched(trainingRecord, cogencyType);
	}

	/*
	 * Set matched CUI in the ontology
	 */
	trainingRecord.setMappedCui(
		conceptDao.getCui(trainingRecord.getConceptUid(), connPool));

    }

    private void computeMaxIoc(TrainingRecord features,
	    ConceptRecognTextType textType) throws SQLException {

	double maxIoc = 0.0d;
	switch (textType) {
	case CONCEPT:
	    if (features.getNbrConceptWords() == 1) {
		break;
	    } else {
		OntolCogency ontolCogency = ontolCogencyDao.marshal(
			features.getConceptNameUid(), features.getConceptUid(),
			features.getConceptPredWordId(), connPool);
		if (ontolCogency != null) {
		    maxIoc = Math.abs(ontolCogency.getLnCogency());
		} else {
		    SortedSet<OntolCogencyVariant> cogencyVariants = ontolCogVariantDao
			    .marshalForVariant(features.getConceptNameUid(),
				    features.getConceptPredWordId(), connPool);
		    if (cogencyVariants.size() > 0) {
			maxIoc = Math
				.abs(cogencyVariants.first().getLnCogency());
		    } else {
			String errMsg = String.format(
				"Maximum IOC not found for concept predicate word ID = %1$d, text predicate word ID = %2$d, "
					+ "concept name UID = %3$d, text type = %4$s, gold standard tag ID = %5$s.",
				features.getConceptPredWordId(),
				features.getTextPredWordId(),
				features.getConceptNameUid(), textType,
				features.getGoldStdPmidTagId());
			throw new SQLException(errMsg);
		    }
		}
	    }
	    break;
	case TEXT:
	    int[] textLexWordIds = features.getTextLexWordIds();
	    if (features.getNbrTextWords() == 1) {
		maxIoc = 0.0d;
	    } else {
		for (int i = 0; i < textLexWordIds.length; i++) {
		    int assumedFactWordId = textLexWordIds[i];
		    int textPredWordId = features.getTextPredWordId();
		    if (textPredWordId == assumedFactWordId) {
			continue;
		    }
		    OntolCondWordProb ontolCondWordProb = ontolCondWordProbDao
			    .marshal(assumedFactWordId, textPredWordId,
				    connPool);
		    if (ontolCondWordProb != null) {
			maxIoc += Math.abs(ontolCondWordProb.getLnCondProb());
		    } else {
			SortedSet<OntolCondWordVariantProb> variantCondProbs = ontolCondWordVarProbDao
				.marshalForVariants(textPredWordId,
					assumedFactWordId, connPool);
			if (variantCondProbs.size() > 0) {
			    maxIoc += Math.abs(
				    variantCondProbs.first().getLnCondProb());
			} else {
			    // variation not found, so continue
			    continue;
			}
		    }
		}
	    }

	    break;
	}

	switch (textType) {
	case CONCEPT:
	    features.setConceptMaxIoc(maxIoc);
	    break;
	case TEXT:
	    features.setTextMaxIoc(maxIoc);
	    break;
	}

    }

    private void computeMatchedIoc(TrainingRecord features,
	    ConceptRecognTextType textType) throws Exception {

	double matchedIoc = 0.0d;
	if (features.getNbrConceptWords() == 1) {
	    matchedIoc = 0.0;
	    return;
	}

	int[] lexWordIds = null;
	boolean[] wordPosMapped = null;
	int predWordId = 0;

	switch (textType) {
	case CONCEPT:
	    /*
	     * To ensure match to text word IDs, use the concept words array
	     * that includes variant substitution
	     */
	    lexWordIds = features.getConceptVarSubstWordIds();
	    wordPosMapped = features.getConceptWordPosMapped();
	    predWordId = features.getConceptPredWordId();
	    break;
	case TEXT:
	    lexWordIds = features.getTextLexWordIds();
	    wordPosMapped = features.getTextWordPosMapped();
	    predWordId = features.getTextPredWordId();
	    break;
	default:
	    break;
	}

	if (wordPosMapped.length != lexWordIds.length) {
	    String errMsg = String.format("Mapping array length mismatch: "
		    + "word pos mapped array len = %1$,d, text word IDs array len = %2$d, text type = %3$s, "
		    + "gold std tag ID = %4$s, concept name UID = %5$d",
		    wordPosMapped.length, lexWordIds.length, textType,
		    features.getGoldStdPmidTagId(),
		    features.getConceptNameUid());
	    throw new Exception(errMsg);
	}
	int nbrWords = lexWordIds.length;
	for (int i = 0; i < nbrWords; i++) {
	    if (!wordPosMapped[i]) {
		continue;
	    }
	    int assumedFactWordId = lexWordIds[i];
	    if (assumedFactWordId == predWordId) {
		continue;
	    }
	    OntolCondWordProb ontolCondWordProb = ontolCondWordProbDao
		    .marshal(assumedFactWordId, predWordId, connPool);
	    if (ontolCondWordProb != null) {
		matchedIoc += Math.abs(ontolCondWordProb.getLnCondProb());
	    } else {
		SortedSet<OntolCondWordVariantProb> variantCondProbs = ontolCondWordVarProbDao
			.marshalForVariants(predWordId, assumedFactWordId,
				connPool);
		if (variantCondProbs.size() > 0) {
		    matchedIoc += Math
			    .abs(variantCondProbs.first().getLnCondProb());
		} else {
		    continue;
		    /*
		     * String errMsg = String.format(
		     * "Ontology conditional word probability not found for " +
		     * "predicate word ID = %1$d, assumed fact word ID = %2$d, "
		     * + "text type = %3$s.", predWordId, assumedFactWordId,
		     * textType); throw new SQLException(errMsg);
		     */
		}
	    }
	}

	switch (textType) {
	case CONCEPT:
	    features.setConceptMatchedIoc(matchedIoc);
	    break;
	case TEXT:
	    features.setTextMatchedIoc(matchedIoc);
	    break;
	default:
	    break;
	}
    }

    private void computeMaxDeltaCogencies(TrainingRecord features)
	    throws SQLException {

	int[] textSentWordPos = features.getTextSentWordPos();
	int maxSentWordPos = textSentWordPos[textSentWordPos.length - 1];
	int minSentWordPos = textSentWordPos[0];

	// find word position immediately prior to min sent word
	if (minSentWordPos > 0) {
	    minSentWordPos += -1;
	}

	Cogency maxCogency = cogencyByPos.get(maxSentWordPos);
	Cogency minCogency = cogencyByPos.get(minSentWordPos);

	// sentence max delta cogency
	features.setMaxSentDeltaCogency(
		Math.abs(maxCogency.getCumLnSentCogency()
			- minCogency.getCumLnSentCogency()));

	// abstract max delta cogency
	if (minSentWordPos == 0 && features.getSentNbr() > 0) {
	    SortedSet<Cogency> priorSentCogencies = abstrWordDao
		    .getSentCogencies(tag.getPmid(), features.getSentNbr() - 1,
			    connPool);
	    minCogency = priorSentCogencies.last();
	}

	features.setMaxAbstrDeltaCogency(
		Math.abs(maxCogency.getCumLnAbstractCogency()
			- minCogency.getCumLnAbstractCogency()));

    }

    private void computeMatchedDeltaCogencies(TrainingRecord features)
	    throws Exception {

	int[] textSentWordPos = features.getTextSentWordPos();
	boolean[] matchedTextPos = features.getTextWordPosMapped();
	boolean isFirstSent = features.getSentNbr() == 0;

	int maxSentWordPos = textSentWordPos[textSentWordPos.length - 1];
	int minSentWordPos = textSentWordPos[0];

	// find word position immediately prior to min sent word
	boolean isFirstSentPosFirstSentWord = true;
	if (minSentWordPos > 0) {
	    minSentWordPos += -1;
	    isFirstSentPosFirstSentWord = false;
	}

	Map<Integer, Integer> isMatchedArrayPosBySentPos = new HashMap<Integer, Integer>();
	for (int i = 0; i < textSentWordPos.length; i++) {
	    isMatchedArrayPosBySentPos.put(textSentWordPos[i], i);
	}

	double notMatchedSentDeltaCogency = 0.0d;
	double notMatchedAbstrDeltaCogency = 0.0d;
	for (int sentWordPos = maxSentWordPos; sentWordPos >= minSentWordPos; sentWordPos--) {
	    boolean isKeepDelta = false;
	    if (isMatchedArrayPosBySentPos.containsKey(sentWordPos)) {
		int matchedTextArrayPos = isMatchedArrayPosBySentPos
			.get(sentWordPos);
		isKeepDelta = !matchedTextPos[matchedTextArrayPos];
	    }
	    Cogency currentCogency = cogencyByPos.get(sentWordPos);
	    Cogency priorCogency = null;
	    /*
	     * If this is the first word in the sent, then compute abstract
	     * cogency delta only. If this is the first word and first sentence,
	     * then continue - no prior cogencies.
	     */
	    if (sentWordPos == minSentWordPos && isFirstSentPosFirstSentWord) {
		if (isFirstSent) {
		    continue;
		}
		if (isKeepDelta) {
		    SortedSet<Cogency> priorSentCogencies = abstrWordDao
			    .getSentCogencies(tag.getPmid(),
				    features.getSentNbr() - 1, connPool);
		    priorCogency = priorSentCogencies.last();
		    notMatchedAbstrDeltaCogency += currentCogency
			    .getCumLnAbstractCogency()
			    - priorCogency.getCumLnAbstractCogency();
		}
		continue;
	    }
	    priorCogency = cogencyByPos.get(sentWordPos - 1);
	    if (isKeepDelta) {
		notMatchedSentDeltaCogency += currentCogency
			.getCumLnSentCogency()
			- priorCogency.getCumLnSentCogency();
		notMatchedAbstrDeltaCogency += currentCogency
			.getCumLnAbstractCogency()
			- priorCogency.getCumLnAbstractCogency();
	    }
	}

	double matchedAbstrCogency = features.getMaxAbstrDeltaCogency()
		- Math.abs(notMatchedAbstrDeltaCogency);
	double matchedTextCogency = features.getMaxSentDeltaCogency()
		- Math.abs(notMatchedSentDeltaCogency);
	features.setMatchedAbstrDeltaCogency(matchedAbstrCogency);
	features.setMatchedSentDeltaCogency(matchedTextCogency);

    }

    private void computeFractWordsMatched(TrainingRecord features,
	    ConceptRecognTextType textType) {

	int nbrWordsMapped = 0;
	int denomCount = 0;

	switch (textType) {
	case CONCEPT:
	    nbrWordsMapped = features.getNbrConceptWordsMatched();
	    denomCount = features.getNbrConceptWords();
	    break;
	case TEXT:
	    nbrWordsMapped = features.getNbrTextWordsMatched();
	    denomCount = features.getNbrTextWords();
	    break;
	}

	double fractWordsMatched = (double) nbrWordsMapped
		/ (double) denomCount;

	switch (textType) {
	case CONCEPT:
	    features.setFractConceptWordsMatched(fractWordsMatched);
	    break;
	case TEXT:
	    features.setFractTextWordsMatched(fractWordsMatched);
	    break;
	}

    }

    private void computeFractIocMatched(TrainingRecord features,
	    ConceptRecognTextType textType) {

	double numer = 0.0d;
	double denom = 0.0d;
	double fractIoc = 0.0d;

	switch (textType) {
	case CONCEPT:
	    numer = features.getConceptMatchedIoc();
	    denom = features.getConceptMaxIoc();
	    break;
	case TEXT:
	    numer = features.getTextMatchedIoc();
	    denom = features.getTextMaxIoc();
	    break;
	}

	if (denom > 0.0) {
	    fractIoc = numer / denom;
	}

	switch (textType) {
	case CONCEPT:
	    features.setFractConceptIoc(fractIoc);
	    break;
	case TEXT:
	    features.setFractTextIoc(fractIoc);
	    break;
	}

    }

    public void computeFractCogencyMatched(TrainingRecord features,
	    ConceptRecognCogencyType cogencyType) {

	double numer = 0.0d;
	double denom = 0.0d;
	double fractCogency = 0.0d;

	switch (cogencyType) {
	case ABSTRACT:
	    numer = features.getMatchedAbstrDeltaCogency();
	    denom = features.getMaxAbstrDeltaCogency();
	    break;
	case SENTENCE:
	    numer = features.getMatchedSentDeltaCogency();
	    denom = features.getMaxSentDeltaCogency();
	    break;
	}

	if (denom > 0.0d) {
	    fractCogency = numer / denom;
	}

	switch (cogencyType) {
	case ABSTRACT:
	    features.setFractAbstrCogency(fractCogency);
	    break;
	case SENTENCE:
	    features.setFractSentCogency(fractCogency);
	    break;
	}

    }

    /**
     * Compute features when both the text to be mapped (the gold std
     * annotation) and the concept consists of a single word only.
     * 
     * @param trainingRecord
     */
    private void computeSingleWordFeatures(TrainingRecord trainingRecord) {

	boolean isWordMatched = trainingRecord.getConceptWordPosMapped()[0];
	if (isWordMatched) {
	    // see if the concept is a synonym
	    int conceptWordId = trainingRecord.getConceptLexWordIds()[0];
	    int textWordId = trainingRecord.getTextLexWordIds()[0];
	    double isConceptSynonym = conceptWordId != textWordId ? 1.0d : 0.0d;
	    trainingRecord.setFractConceptWordsMatched(1.0d);
	    trainingRecord.setFractTextWordsMatched(1.0d);
	    trainingRecord.setIsConceptSynonym(isConceptSynonym);
	    trainingRecord.setFractConceptIoc(1.0d);
	    trainingRecord.setFractTextIoc(1.0d);
	    trainingRecord.setFractAbstrCogency(0.0d);
	    trainingRecord.setFractSentCogency(0.0d);
	    trainingRecord.setIsGoodMatch(1.0d);
	    trainingRecord.setIsPoorMatch(0.0d);
	} else {
	    trainingRecord.setFractConceptWordsMatched(0.0d);
	    trainingRecord.setFractTextWordsMatched(0.0d);
	    trainingRecord.setIsConceptSynonym(0.0d);
	    trainingRecord.setFractConceptIoc(0.0d);
	    trainingRecord.setFractTextIoc(0.0d);
	    trainingRecord.setFractAbstrCogency(0.0d);
	    trainingRecord.setFractSentCogency(0.0d);
	    trainingRecord.setIsGoodMatch(0.0d);
	    trainingRecord.setIsPoorMatch(1.0d);
	}
    }

    /**
     * Check to see if a multi-word concept is a synonym for a single token in
     * the text.
     * 
     * @param trainingRecord
     * @return
     * @throws SQLException
     */
    private boolean isConceptSynonym(TrainingRecord trainingRecord)
	    throws SQLException {

	boolean isConceptSynonym = false;
	// get all concepts containing the text token
	String getSingleWordConceptSql = "select * from "
		+ " (select count(*) as nbrWords, conceptNameUid from conceptrecogn.conceptnamewords group by conceptNameUid) as nbrW, "
		+ " (select * from conceptrecogn.conceptnamewords) as words, "
		+ " (select lexWordVariantId from conceptrecogn.lexwordvariants where lexWordId=?) as vars "
		+ " where nbrW.nbrWords = 1 and nbrW.conceptNameUid = words.conceptNameUid and words.conceptUid=? "
		+ " and (words.lexWordId = ? or words.lexWordId = vars.lexWordVariantId)";
	int textWordId = trainingRecord.getTextLexWordIds()[0];
	long conceptUid = trainingRecord.getConceptUid();
	try (Connection conn = connPool.getConnection()) {
	    try(PreparedStatement getSingleWordConceptStmt = conn.prepareStatement(getSingleWordConceptSql)){
		getSingleWordConceptStmt.setInt(1, textWordId);
		getSingleWordConceptStmt.setLong(2, conceptUid);
		getSingleWordConceptStmt.setInt(3, textWordId);
		try(ResultSet rs = getSingleWordConceptStmt.executeQuery()){
		    if(rs.next()) {
			isConceptSynonym = true;
		    }
		}
	    }
	}

	return isConceptSynonym;
    }

    private void setConceptSynonymFeatures(TrainingRecord trainingRecord) {

	trainingRecord.setFractConceptWordsMatched(1.0d);
	trainingRecord.setFractTextWordsMatched(1.0d);
	trainingRecord.setIsConceptSynonym(1.0d);
	trainingRecord.setFractConceptIoc(1.0d);
	trainingRecord.setFractTextIoc(1.0d);
	trainingRecord.setFractAbstrCogency(0.0d);
	trainingRecord.setFractSentCogency(0.0d);
	trainingRecord.setIsGoodMatch(1.0d);
	trainingRecord.setIsPoorMatch(0.0d);

    }
}
