/**
 * 
 */
package edu.mst.conceptrecogn;

/**
 * Type of candidate concept to marshal for concept recognition.
 * 
 * @author gjs
 *
 */
public enum CandidateNameType {
    
    WITH_ACR_ABBREV, WITHOUT_ACR_ABBREV;

}
