/**
 * 
 */
package edu.mst.conceptrecogn;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import edu.mst.db.ontol.words.ConceptNameWord;

/**
 * Candidate concept when performing concept recognition
 * 
 * @author gjs
 *
 */
public class CandidateName implements Comparable<CandidateName> {

    private long conceptNameUid = 0L;
    private long conceptUid = 0L;
    private CandidateNameType candidateType = null;
    private List<ConceptNameWord> words = new ArrayList<ConceptNameWord>();

    public CandidateName(long conceptNameUid, long conceptUid,
	    CandidateNameType candidateType) {
	this.conceptNameUid = conceptNameUid;
	this.conceptUid = conceptUid;
	this.candidateType = candidateType;
    }

    public static class Comparators {
	public static final Comparator<CandidateName> byCandidateType = (
		CandidateName candidate1,
		CandidateName candidate2) -> candidate1.candidateType
			.compareTo(candidate2.candidateType);
	public static final Comparator<CandidateName> byConceptNameUid = (
		CandidateName candidate1,
		CandidateName candidate2) -> Long.compare(candidate1.conceptNameUid,
			candidate2.conceptNameUid);
	public static final Comparator<CandidateName> byConceptUid = (
		CandidateName candidate1,
		CandidateName candidate2) -> Long
			.compare(candidate1.conceptUid, candidate2.conceptUid);
	public static final Comparator<CandidateName> byType_ConceptUid_NameUid = (
		CandidateName candidate1,
		CandidateName candidate2) -> byCandidateType
			.thenComparing(byConceptNameUid).thenComparing(byConceptUid)
			.compare(candidate1, candidate2);
    }

    @Override
    public int compareTo(CandidateName candidate) {
	return Comparators.byConceptNameUid.compare(this, candidate);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof CandidateName) {
	    CandidateName candidate = (CandidateName) obj;
	    return conceptNameUid == candidate.conceptNameUid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Long.hashCode(conceptNameUid);
    }

    public long getConceptNameUid() {
        return conceptNameUid;
    }

    public long getConceptUid() {
        return conceptUid;
    }

    public CandidateNameType getCandidateType() {
        return candidateType;
    }

    public List<ConceptNameWord> getWords() {
        return words;
    }

}
