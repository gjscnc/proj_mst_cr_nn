/**
 * 
 */
package edu.mst.conceptrecogn.taglevel.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureCombinationOutputs;
import edu.mst.conceptrecogn.FeatureName;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Data access object for persisting or retrieving IOC training data persisted
 * in database.
 * 
 * @author George
 *
 */
public class TrainingDataDao {

    public static final String[] autoPkField = new String[] { "id" };

    public static final String persistTagTrainingFields = "goldStdPmidTagId, fractConceptWordsMatched, "
	    + "fractTextWordsMatched, fractConceptIoc, fractTextIoc, fractAbstrCogency, fractSentCogency, "
	    + "isConceptSingleWord, isTextSingleWord, isConceptSynonym, isGoodMatch, isPoorMatch, conceptNameUid, conceptUid, "
	    + "cui, sourceCorpus, nbrWordsMapped, textMaxIoc, textMatchedIoc, conceptMaxIoc, conceptMatchedIoc, "
	    + "maxAbstrCogency, matchedAbstrCogency, maxSentCogency, matchedSentCogency, "
	    + "conceptWordIdsAsStr, conceptMatchPosAsStr, textWordIdsAsStr, textMatchPosAsStr, conceptVarSubstWordIdsAsStr ";

    public static final String allTagTrainingFields = "id, "
	    + persistTagTrainingFields;

    public static final String deleteAllSql = "truncate table conceptrecogn.trainingdata";

    public static final String resetAutoIdSql = "ALTER TABLE conceptrecogn.trainingdata AUTO_INCREMENT = 1";

    public static final String persistTrainingRecordSql = "INSERT INTO conceptrecogn.trainingdata "
	    + "(" + persistTagTrainingFields
	    + ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String marshalTrainingRecordSql = "SELECT "
	    + allTagTrainingFields
	    + " FROM conceptrecogn.trainingdata WHERE id = ?";

    private static final String marshalCorpusTrainingRecordsSql = "SELECT "
	    + allTagTrainingFields
	    + " FROM conceptrecogn.trainingdata where sourceCorpus=?";

    public static final String marshalAllTrainingRecordsSql = "SELECT "
	    + allTagTrainingFields + " FROM conceptrecogn.trainingdata";

    public static final String conceptRecognInputFields = "id, goldStdPmidTagId, fractConceptWordsMatched, "
	    + "fractTextWordsMatched, fractConceptIoc, fractTextIoc, fractAbstrCogency, fractSentCogency, "
	    + "isConceptSingleWord, isTextSingleWord, isConceptSynonym, isGoodMatch, isPoorMatch, "
	    + "conceptNameUid, conceptUid, cui, sourceCorpus ";

    public static final String marshalFeatureRecordSql = "SELECT "
	    + conceptRecognInputFields
	    + " FROM conceptrecogn.trainingdata where id=?";

    public static final String marshalAllFeaturesSql = "SELECT "
	    + conceptRecognInputFields + " FROM conceptrecogn.trainingdata";

    public static final String marshalCorpusFeaturesSql = "SELECT "
	    + conceptRecognInputFields
	    + " FROM conceptrecogn.trainingdata where sourceCorpus=?";

    private JdbcConnectionPool connPool;

    public TrainingDataDao(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteAllSql);
		    PreparedStatement updateAutoIdStmt = conn
			    .prepareStatement(resetAutoIdSql)) {
		deleteStmt.executeUpdate();
		updateAutoIdStmt.executeUpdate();
	    }
	}
    }

    public void persist(TrainingRecord trainingRecord) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistTrainingRecordSql, autoPkField)) {
		setPersistValues(trainingRecord, persistStmt);

		persistStmt.executeUpdate();

		int id = 0;

		try (ResultSet keyRs = persistStmt.getGeneratedKeys()) {
		    if (keyRs.next()) {
			id = keyRs.getInt(1);
		    } else {
			throw new SQLException(
				"Auto-incremented PK value not returned from database");
		    }
		}

		trainingRecord.setId(id);
	    }
	}
    }

    public void persistBatch(Collection<TrainingRecord> trainingRecords)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistTrainingDataStmt = conn
		    .prepareStatement(persistTrainingRecordSql, autoPkField)) {
		for (TrainingRecord trainingRecord : trainingRecords) {
		    setPersistValues(trainingRecord, persistTrainingDataStmt);
		    persistTrainingDataStmt.addBatch();
		}
		persistTrainingDataStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(TrainingRecord record,
	    PreparedStatement persistStmt) throws SQLException {
	/*
	 * id, 1-goldStdPmidTagId, 2-fractConceptWordsMatched,
	 * 3-fractTextWordsMatched, 4-fractConceptIoc, 5-fractTextIoc,
	 * 6-fractAbstrCogency, 7-fractSentCogency, 8-isConceptSingleWord,
	 * 9-isTextSingleWord, 10-isConceptSynonym, 11-isGoodMatch,
	 * 12-isPoorMatch, 13-conceptNameUid, 14-conceptUid, 15-cui,
	 * 16-sourceCorpus, 17-nbrWordsMapped, 18-textMaxIoc, 19-textMatchedIoc,
	 * 20-conceptMaxIoc, 21-conceptMatchedIoc, 22-maxAbstrCogency,
	 * 23-matchedAbstrCogency, 24-maxSentCogency, 25-matchedSentCogency,
	 * 26-conceptWordIdsAsStr, 27-conceptMatchPosAsStr, 28-textWordIdsAsStr,
	 * 29-textMatchPosAsStr, 30-conceptVarSubstWordIdsAsStr
	 * 
	 */
	persistStmt.setString(1, record.getGoldStdPmidTagId());
	persistStmt.setDouble(2, record.getFractConceptWordsMatched());
	persistStmt.setDouble(3, record.getFractTextWordsMatched());
	persistStmt.setDouble(4, record.getFractConceptIoc());
	persistStmt.setDouble(5, record.getFractTextIoc());
	persistStmt.setDouble(6, record.getFractAbstrCogency());
	persistStmt.setDouble(7, record.getFractSentCogency());
	persistStmt.setDouble(8, record.isConceptSingleWord());
	persistStmt.setDouble(9, record.isTextSingleWord());
	persistStmt.setDouble(10, record.getIsConceptSynonym());
	persistStmt.setDouble(11, record.isGoodMatch());
	persistStmt.setDouble(12, record.isPoorMatch());
	persistStmt.setLong(13, record.getConceptNameUid());
	persistStmt.setLong(14, record.getConceptUid());
	persistStmt.setString(15, record.getMappedCui());
	persistStmt.setString(16, record.getCorpus().toString());
	persistStmt.setInt(17, record.getNbrWordsMapped());
	persistStmt.setDouble(18, record.getTextMaxIoc());
	persistStmt.setDouble(19, record.getTextMatchedIoc());
	persistStmt.setDouble(20, record.getConceptMaxIoc());
	persistStmt.setDouble(21, record.getConceptMatchedIoc());
	persistStmt.setDouble(22, record.getMaxAbstrDeltaCogency());
	persistStmt.setDouble(23, record.getMatchedAbstrDeltaCogency());
	persistStmt.setDouble(24, record.getMaxSentDeltaCogency());
	persistStmt.setDouble(25, record.getMatchedSentDeltaCogency());
	persistStmt.setString(26, record.getConceptWordIdsAsStr());
	persistStmt.setString(27, record.getConceptMatchPosAsStr());
	persistStmt.setString(28, record.getTextWordIdsAsStr());
	persistStmt.setString(29, record.getTextMatchPosAsStr());
	persistStmt.setString(30, record.getConceptVarSubstWordIdsAsStr());

    }

    public List<TrainingRecord> marshalTrainingRecords(Corpus corpus,
	    FeatureName[] orderedFeatureNames,
	    FeatureCombinationOutputs featureComboOutputs) throws SQLException {

	List<TrainingRecord> featureSet = new ArrayList<TrainingRecord>();

	TrainingRecord.setFeatureCombination(orderedFeatureNames,
		featureComboOutputs);

	try (Connection conn = connPool.getConnection()) {
	    String getDataSql = null;
	    if (corpus == Corpus.ALL) {
		getDataSql = marshalAllTrainingRecordsSql;
	    } else {
		getDataSql = marshalCorpusTrainingRecordsSql;
	    }
	    try (PreparedStatement getCorpusTrainingDataStmt = conn
		    .prepareStatement(getDataSql)) {
		if (corpus != Corpus.ALL) {
		    getCorpusTrainingDataStmt.setString(1, corpus.toString());
		}
		try (ResultSet rs = getCorpusTrainingDataStmt.executeQuery()) {
		    while (rs.next()) {
			TrainingRecord features = instantiateTrainingRecordFromResultSet(
				rs);
			featureSet.add(features);
		    }
		}
	    }
	}

	return featureSet;
    }

    public TrainingRecord marshalTrainingRecord(int recordId,
	    FeatureName[] orderedFeatureNames,
	    FeatureCombinationOutputs featureComboOutputs) throws SQLException {

	TrainingRecord features = null;
	TrainingRecord.setFeatureCombination(orderedFeatureNames,
		featureComboOutputs);

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement retrieveTrainingRecordStmt = conn
		    .prepareStatement(marshalTrainingRecordSql)) {
		try (ResultSet rs = retrieveTrainingRecordStmt.executeQuery()) {
		    if (rs.next()) {
			features = instantiateTrainingRecordFromResultSet(rs);
		    }
		}
	    }
	}

	return features;

    }

    private TrainingRecord instantiateTrainingRecordFromResultSet(ResultSet rs)
	    throws SQLException {

	/*
	 * 1-id, 2-goldStdPmidTagId, 3-fractConceptWordsMatched,
	 * 4-fractTextWordsMatched, 5-fractConceptIoc, 6-fractTextIoc,
	 * 7-fractAbstrCogency, 8-fractSentCogency, 9-isConceptSingleWord,
	 * 10-isTextSingleWord, 11-isConceptSynonym, 12-isGoodMatch,
	 * 13-isPoorMatch, 14-conceptNameUid, 15-conceptUid, 16-cui,
	 * 17-sourceCorpus, 18-nbrWordsMapped, 19-textMaxIoc, 20-textMatchedIoc,
	 * 21-conceptMaxIoc, 22-conceptMatchedIoc, 23-maxAbstrCogency,
	 * 24-matchedAbstrCogency, 25-maxSentCogency, 26-matchedSentCogency,
	 * 27-conceptWordIdsAsStr, 28-conceptMatchPosAsStr, 29-textWordIdsAsStr,
	 * 30-textMatchPosAsStr, 31-conceptVarSubstWordIdsAsStr
	 * 
	 */
	TrainingRecord record = new TrainingRecord();
	record.setId(rs.getInt(1));
	record.setGoldStdPmidTagId(rs.getString(2));
	record.setFractConceptWordsMatched(rs.getDouble(3));
	record.setFractTextWordsMatched(rs.getDouble(4));
	record.setFractConceptIoc(rs.getDouble(5));
	record.setFractTextIoc(rs.getDouble(6));
	record.setFractAbstrCogency(rs.getDouble(7));
	record.setFractSentCogency(rs.getDouble(8));
	record.setIsConceptSingleWord(rs.getDouble(9));
	record.setIsTextSingleWord(rs.getDouble(10));
	record.setIsConceptSynonym(rs.getDouble(11));
	record.setIsGoodMatch(rs.getDouble(12));
	record.setIsPoorMatch(rs.getDouble(13));
	record.setConceptNameUid(rs.getLong(14));
	record.setConceptUid(rs.getLong(15));
	record.setCui(rs.getString(16));
	record.setCorpus(Corpus.valueOf(rs.getString(17)));
	record.setNbrWordsMapped(rs.getInt(18));
	record.setTextMaxIoc(rs.getDouble(19));
	record.setTextMatchedIoc(rs.getDouble(20));
	record.setConceptMaxIoc(rs.getDouble(21));
	record.setConceptMatchedIoc(rs.getDouble(22));
	record.setMaxAbstrDeltaCogency(rs.getDouble(23));
	record.setMatchedAbstrDeltaCogency(rs.getDouble(24));
	record.setMaxSentDeltaCogency(rs.getDouble(25));
	record.setMatchedSentDeltaCogency(rs.getDouble(26));
	record.setConceptWordIdsAsStr(rs.getString(27));
	record.setConceptMatchPosAsStr(rs.getString(28));
	record.setTextWordIdsAsStr(rs.getString(29));
	record.setTextMatchPosAsStr(rs.getString(30));
	record.setConceptVarSubstWordIdsAsStr(rs.getString(31));

	return record;

    }

    public TagLevelFeatures marshalFeatures(int recordId,
	    FeatureName[] orderedFeatureNames,
	    FeatureCombinationOutputs featureComboOutputs) throws SQLException {

	TagLevelFeatures features = null;
	TagLevelFeatures.setFeatureCombination(orderedFeatureNames,
		featureComboOutputs);

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalFeatureRecordSql)) {
		marshalStmt.setInt(1, recordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			features = instantiateFeaturesFromResultSet(rs);
		    }
		}
	    }
	}

	return features;
    }

    public List<TagLevelFeatures> marshalAllFeatures(Corpus sourceCorpus,
	    FeatureName[] orderedFeatureNames,
	    FeatureCombinationOutputs featureComboOutputs) throws SQLException {

	List<TagLevelFeatures> featureCollection = new ArrayList<TagLevelFeatures>();
	TagLevelFeatures.setFeatureCombination(orderedFeatureNames,
		featureComboOutputs);

	try (Connection conn = connPool.getConnection()) {
	    String getDataSql = null;
	    if (sourceCorpus == Corpus.ALL) {
		getDataSql = marshalAllFeaturesSql;
	    } else {
		getDataSql = marshalCorpusFeaturesSql;
	    }
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(getDataSql)) {
		if (sourceCorpus != Corpus.ALL) {
		    marshalStmt.setString(1, sourceCorpus.toString());
		}
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			TagLevelFeatures features = instantiateFeaturesFromResultSet(
				rs);
			featureCollection.add(features);
		    }
		}
	    }
	}

	return featureCollection;
    }

    public List<TagLevelFeatures> marshalAllFeatures(Corpus sourceCorpus,
	    FeatureCombinationInputs featureCombo,
	    FeatureCombinationOutputs featureComboOutputs) throws SQLException {
	return marshalAllFeatures(sourceCorpus,
		featureCombo.orderedFeatureNames(), featureComboOutputs);
    }

    private TagLevelFeatures instantiateFeaturesFromResultSet(ResultSet rs)
	    throws SQLException {

	TagLevelFeatures features = new TagLevelFeatures();

	/*
	 * 1-id, 2-goldStdPmidTagId, 3-fractConceptWordsMatched,
	 * 4-fractTextWordsMatched, 5-fractConceptIoc, 6-fractTextIoc,
	 * 7-fractAbstrCogency, 8-fractSentCogency, 9-isConceptSingleWord,
	 * 10-isTextSingleWord, 11-isConceptSynonym, 12-isGoodMatch,
	 * 13-isPoorMatch, 14-conceptNameUid, 15-conceptUid, 16-cui,
	 * 17-sourceCorpus
	 */

	features.setId(rs.getInt(1));
	features.setGoldStdPmidTagId(rs.getString(2));
	features.setFractConceptWordsMatched(rs.getDouble(3));
	features.setFractTextWordsMatched(rs.getDouble(4));
	features.setFractConceptIoc(rs.getDouble(5));
	features.setFractTextIoc(rs.getDouble(6));
	features.setFractAbstrCogency(rs.getDouble(7));
	features.setFractSentCogency(rs.getDouble(8));
	features.setIsConceptSingleWord(rs.getDouble(9));
	features.setIsTextSingleWord(rs.getDouble(10));
	features.setIsConceptSynonym(rs.getDouble(11));
	features.setIsGoodMatch(rs.getDouble(12));
	features.setIsPoorMatch(rs.getDouble(13));
	features.setConceptNameUid(rs.getLong(14));
	features.setConceptUid(rs.getLong(15));
	features.setCui(rs.getString(16));
	features.setCorpus(Corpus.valueOf(rs.getString(17)));

	return features;
    }

}
