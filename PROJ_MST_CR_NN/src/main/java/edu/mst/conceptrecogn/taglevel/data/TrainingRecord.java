/**
 * 
 */
package edu.mst.conceptrecogn.taglevel.data;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import edu.mst.conceptrecogn.FeatureCalculator;
import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWord;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWordDao;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.util.Constants;

/**
 * Feature values for training the neural network input.
 * <p>
 * This class retrieves arrays of words for this candidate concept, and then
 * determines word matches. This includes an array of word IDs and an array that
 * identifies whether or not the word in the concept matches a word in the text.
 * <p>
 * To compute feature values input to the tagging MLP neural network, use
 * {@link FeatureCalculator}.
 * 
 * @author George
 *
 */
public class TrainingRecord extends TagLevelFeatures {

    public static final double defaultTrue = 1.0d;
    public static final double defaultFalse = 0.0d;
    public static final char wordIdsArrayDelim = ',';

    private GoldStdTag tag;
    private int sentNbr;
    private String conceptName;
    private boolean isBestMatch = false;
    private JdbcConnectionPool connPool;

    /**
     * This array stores the gold standard lexicon word IDs
     */
    private int[] textLexWordIds;
    private int nbrTextWords = 0;
    private int nbrTextWordsMatched = 0;

    /**
     * Word ID in text matched to the concept word, by pos in the text
     */
    private int[] conceptVarSubstWordIds;
    private int nbrConceptWords = 0;
    private int nbrConceptWordsMatched = 0;

    /**
     * This array captures whether or not the word in the phrase is mapped to a
     * word in the phrase.
     */
    private boolean[] textWordPosMapped;

    /**
     * Word position of the text in the document sentence.
     */
    private int[] textSentWordPos;

    /**
     * This array stores the lexicon word IDs for the candidate concept.
     */
    private int[] conceptLexWordIds;

    /**
     * This array captures whether or not the word in the concept is mapped to a
     * word in the phrase.
     */
    private boolean[] conceptWordPosMapped;

    private int textPredWordPos = Constants.uninitializedIntVal;
    private int textPredWordId = Constants.uninitializedIntVal;
    private int conceptPredWordPos = Constants.uninitializedIntVal;
    private int conceptPredWordId = Constants.uninitializedIntVal;

    private int nbrWordsMapped = 0;
    private double textMaxIoc = 0.0d;
    private double textMatchedIoc = 0.0d;
    private double conceptMaxIoc = 0.0d;
    private double conceptMatchedIoc = 0.0d;
    private double maxDeltaAbstrCogency = 0.0d;
    private double maxSentDeltaCogency = 0.0d;
    private double matchedAbstrDeltaCogency = 0.0d;
    private double matchedSentDeltaCogency = 0.0d;
    private String conceptWordIdsAsStr = null;
    private String conceptVarSubstWordIdsAsStr = null;
    private String conceptMatchPosAsStr = null;
    private String textWordIdsAsStr = null;
    private String textMatchPosAsStr = null;

    private String mappedCui = null;

    private String errMsg;
    private boolean isErrorExists = false;
    private Exception exception = null;
    private GoldStdTagWordDao tagWordDao = new GoldStdTagWordDao();
    private ConceptNameDao conceptNameDao = new ConceptNameDao();
    private ConceptNameWordDao conceptNameWordDao = new ConceptNameWordDao();
    private LexWordVariantDao variantDao = new LexWordVariantDao();

    public TrainingRecord() {

    }

    public TrainingRecord(GoldStdTag tag, long conceptNameUid, long conceptUid,
	    boolean isGoodMatch, Corpus sourceCorpus,
	    JdbcConnectionPool connPool) throws Exception {
	this.tag = tag;
	this.conceptUid = conceptUid;
	this.conceptNameUid = conceptNameUid;
	this.isGoodMatch = isGoodMatch == true ? defaultTrue : defaultFalse;
	this.isPoorMatch = isGoodMatch == false ? defaultTrue : defaultFalse;
	this.corpus = sourceCorpus;
	this.connPool = connPool;
	setGoldStdPmidTagId(tag.getPmid(), tag.getTagId());
	marshallMappings();
    }

    @Override
    public int compareTo(TagLevelFeatures valueSet) {
	return Long.compare(conceptUid, valueSet.getConceptUid());
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof TrainingRecord) {
	    TrainingRecord valueSet = (TrainingRecord) obj;
	    return conceptUid == valueSet.conceptUid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Long.hashCode(conceptUid);
    }

    /**
     * Retrieves word arrays and word matching required for computing feature
     * values.
     * 
     * @throws Exception
     */
    private void marshallMappings() throws Exception {

	SortedSet<GoldStdTagWord> tagWords = tagWordDao.marshal(tag.getPmid(),
		tag.getTagId(), connPool);

	sentNbr = tagWords.first().getSentNbr();

	nbrTextWords = tagWords.size();
	textLexWordIds = new int[nbrTextWords];
	textSentWordPos = new int[nbrTextWords];
	Arrays.fill(textLexWordIds, 0);
	Arrays.fill(textSentWordPos, 0);
	Map<Integer, Set<Integer>> variantsByTextLexWordId = null;
	{
	    Set<Integer> textLexWordIdSet = new HashSet<Integer>();
	    int arrayPos = 0;
	    for (GoldStdTagWord tagWord : tagWords) {
		textLexWordIds[arrayPos] = tagWord.getLexWordId();
		textSentWordPos[arrayPos] = tagWord.getSentWordNbr();
		textLexWordIdSet.add(tagWord.getLexWordId());
		arrayPos++;
	    }
	    variantsByTextLexWordId = variantDao
		    .marshalVariantsByLexWordId(textLexWordIdSet, connPool);
	}

	try {
	    List<ConceptNameWord> nameWords = conceptNameWordDao
		    .marshalWordsForName(conceptNameUid, connPool);
	    nbrConceptWords = nameWords.size();
	    conceptLexWordIds = new int[nameWords.size()];
	    conceptVarSubstWordIds = new int[nameWords.size()];
	    Arrays.fill(conceptLexWordIds, 0);
	    Arrays.fill(conceptVarSubstWordIds, 0);
	    int pos = 0;
	    for (ConceptNameWord nameWord : nameWords) {
		conceptLexWordIds[pos] = nameWord.getLexWordId();
		conceptVarSubstWordIds[pos] = nameWord.getLexWordId();
		pos++;
	    }
	} catch (SQLException ex) {
	    isErrorExists = true;
	    errMsg = ex.getLocalizedMessage();
	    exception = new Exception(ex);
	    return;
	}
	isConceptSingleWord = nbrConceptWords == 1 ? defaultTrue : defaultFalse;
	conceptWordPosMapped = new boolean[conceptLexWordIds.length];
	Arrays.fill(conceptWordPosMapped, false);
	try {
	    conceptName = conceptNameDao.marshal(conceptNameUid, connPool)
		    .getNameText();
	} catch (SQLException ex) {
	    isErrorExists = true;
	    errMsg = ex.getLocalizedMessage();
	    exception = new Exception(ex);
	    return;
	}
	if (isErrorExists) {
	    return;
	}

	/*
	 * Iterates over text word IDs and concept word IDs, beginning at the
	 * right-most side, to find the first predicate word. Then, scan text
	 * words and concept words to identify which words are matched.
	 */
	// get predicate word id and position
	boolean isPredFound = false;
	for (int i = nbrTextWords - 1; i >= 0; i--) {
	    int textWordId = textLexWordIds[i];
	    for (int j = nbrConceptWords - 1; j >= 0; j--) {
		int conceptWordId = conceptLexWordIds[j];
		if (textWordId == conceptWordId || variantsByTextLexWordId
			.get(textWordId).contains(conceptWordId)) {
		    textPredWordId = textWordId;
		    textPredWordPos = i;
		    conceptPredWordPos = j;
		    conceptPredWordId = conceptWordId;
		    isPredFound = true;
		    break;
		}
	    }
	    if (isPredFound) {
		break;
	    }
	}

	if (!isPredFound) {
	    String errMsg = String.format(
		    "Predicate word not found. Tag = %1$s, concept name = %2$s",
		    tag.toString(), conceptName.toString());
	    throw new Exception(errMsg);
	}

	/*
	 * Scan the text words, leftward starting at the predicate word, and
	 * match words in concept that match words in text
	 */
	textWordPosMapped = new boolean[nbrTextWords];
	Arrays.fill(textWordPosMapped, false);
	for (int i = textPredWordPos; i >= 0; i--) {
	    int textWordId = textLexWordIds[i];
	    for (int j = conceptPredWordPos; j >= 0; j--) {
		int conceptWordId = conceptLexWordIds[j];
		if (conceptWordPosMapped[j]) {
		    continue;
		}
		boolean isConceptWordMatched = textWordId == conceptWordId;
		boolean isTextVariantMatched = variantsByTextLexWordId
			.get(textWordId).contains(conceptWordId);
		if (isConceptWordMatched || isTextVariantMatched) {
		    if (isTextVariantMatched) {
			conceptVarSubstWordIds[j] = textWordId;
		    }
		    conceptWordPosMapped[j] = true;
		    nbrConceptWordsMatched++;
		    textWordPosMapped[i] = true;
		    nbrTextWordsMatched++;
		}
	    }
	}

	isTextSingleWord = nbrTextWords == 1 ? defaultTrue : defaultFalse;

	nbrWordsMapped = nbrConceptWordsMatched;

	conceptWordIdsAsStr = intArrayToStr(conceptLexWordIds);
	conceptVarSubstWordIdsAsStr = intArrayToStr(conceptVarSubstWordIds);
	conceptMatchPosAsStr = boolArrayToStr(conceptWordPosMapped);
	textWordIdsAsStr = intArrayToStr(textLexWordIds);
	textMatchPosAsStr = boolArrayToStr(textWordPosMapped);

    }

    private String boolArrayToStr(boolean[] bools) {
	StringBuilder strBuilder = new StringBuilder();
	for (int i = 0; i < bools.length; i++) {
	    strBuilder.append(bools[i] ? '1' : '0');
	    if (i != bools.length - 1) {
		strBuilder.append(wordIdsArrayDelim);
	    }
	}
	return strBuilder.toString();
    }

    private String intArrayToStr(int[] lexWordIds) {
	StringBuilder strBuilder = new StringBuilder();
	for (int i = 0; i < lexWordIds.length; i++) {
	    strBuilder.append(lexWordIds[i]);
	    if (i != lexWordIds.length - 1) {
		strBuilder.append(wordIdsArrayDelim);
	    }
	}
	return strBuilder.toString();
    }

    public GoldStdTag getGoldStdTag() {
	return tag;
    }

    public void setGoldStdTag(GoldStdTag goldStdTag) {
	this.tag = goldStdTag;
    }

    public int getSentNbr() {
	return sentNbr;
    }

    public void setSentNbr(int sentNbr) {
	this.sentNbr = sentNbr;
    }

    public String getConceptName() {
	return conceptName;
    }

    public boolean isBestMatch() {
	return isBestMatch;
    }

    public JdbcConnectionPool getConnPool() {
	return connPool;
    }

    public int[] getTextLexWordIds() {
	return textLexWordIds;
    }

    public int getNbrTextWords() {
	return nbrTextWords;
    }

    public boolean[] getTextWordPosMapped() {
	return textWordPosMapped;
    }

    public int[] getTextSentWordPos() {
	return textSentWordPos;
    }

    public int getNbrConceptWords() {
	return nbrConceptWords;
    }

    public boolean[] getConceptWordPosMapped() {
	return conceptWordPosMapped;
    }

    public int[] getConceptLexWordIds() {
	return conceptLexWordIds;
    }

    public int[] getConceptVarSubstWordIds() {
	return conceptVarSubstWordIds;
    }

    public void setConceptVarSubstWordIds(int[] conceptVarSubstWordIds) {
	this.conceptVarSubstWordIds = conceptVarSubstWordIds;
    }

    public int getTextPredWordPos() {
	return textPredWordPos;
    }

    public int getConceptPredWordPos() {
	return conceptPredWordPos;
    }

    public int getConceptPredWordId() {
	return conceptPredWordId;
    }

    public int getTextPredWordId() {
	return textPredWordId;
    }

    public int getNbrWordsMapped() {
	return nbrWordsMapped;
    }

    public void setNbrWordsMapped(int nbrWordsMapped) {
	this.nbrWordsMapped = nbrWordsMapped;
    }

    public String getErrMsg() {
	return errMsg;
    }

    public void setErrMsg(String errMsg) {
	this.errMsg = errMsg;
    }

    public boolean isErrorExists() {
	return isErrorExists;
    }

    public void setErrorExists(boolean isErrorExists) {
	this.isErrorExists = isErrorExists;
    }

    public Exception getException() {
	return exception;
    }

    public int getNbrConceptWordsMatched() {
	return nbrConceptWordsMatched;
    }

    public int getNbrTextWordsMatched() {
	return nbrTextWordsMatched;
    }

    public double getTextMaxIoc() {
	return textMaxIoc;
    }

    public void setTextMaxIoc(double textMaxIoc) {
	this.textMaxIoc = textMaxIoc;
    }

    public double getTextMatchedIoc() {
	return textMatchedIoc;
    }

    public void setTextMatchedIoc(double textMatchedIoc) {
	this.textMatchedIoc = textMatchedIoc;
    }

    public double getMaxAbstrDeltaCogency() {
	return maxDeltaAbstrCogency;
    }

    public void setMaxAbstrDeltaCogency(double maxAbstrDeltaCogency) {
	this.maxDeltaAbstrCogency = maxAbstrDeltaCogency;
    }

    public double getMaxSentDeltaCogency() {
	return maxSentDeltaCogency;
    }

    public void setMaxSentDeltaCogency(double maxSentDeltaCogency) {
	this.maxSentDeltaCogency = maxSentDeltaCogency;
    }

    public double getMatchedAbstrDeltaCogency() {
	return matchedAbstrDeltaCogency;
    }

    public void setMatchedAbstrDeltaCogency(double matchedAbstrDeltaCogency) {
	this.matchedAbstrDeltaCogency = matchedAbstrDeltaCogency;
    }

    public double getMatchedSentDeltaCogency() {
	return matchedSentDeltaCogency;
    }

    public void setMatchedSentDeltaCogency(double matchedSentDeltaCogency) {
	this.matchedSentDeltaCogency = matchedSentDeltaCogency;
    }

    public double getConceptMaxIoc() {
	return conceptMaxIoc;
    }

    public void setConceptMaxIoc(double conceptMaxIoc) {
	this.conceptMaxIoc = conceptMaxIoc;
    }

    public double getConceptMatchedIoc() {
	return conceptMatchedIoc;
    }

    public void setConceptMatchedIoc(double conceptMatchedIoc) {
	this.conceptMatchedIoc = conceptMatchedIoc;
    }

    public String getConceptMatchPosAsStr() {
	return conceptMatchPosAsStr;
    }

    public void setConceptMatchPosAsStr(String conceptMatchPosAsStr) {
	this.conceptMatchPosAsStr = conceptMatchPosAsStr;
    }

    public String getTextWordIdsAsStr() {
	return textWordIdsAsStr;
    }

    public void setTextWordIdsAsStr(String textWordIdsAsStr) {
	this.textWordIdsAsStr = textWordIdsAsStr;
    }

    public String getTextMatchPosAsStr() {
	return textMatchPosAsStr;
    }

    public void setTextMatchPosAsStr(String textMatchPosAsStr) {
	this.textMatchPosAsStr = textMatchPosAsStr;
    }

    public String getMappedCui() {
	return mappedCui;
    }

    public void setMappedCui(String mappedCui) {
	this.mappedCui = mappedCui;
    }

    public String getConceptWordIdsAsStr() {
	return conceptWordIdsAsStr;
    }

    public String getConceptVarSubstWordIdsAsStr() {
	return conceptVarSubstWordIdsAsStr;
    }

    public void setConceptVarSubstWordIdsAsStr(
	    String conceptVarSubstWordIdsAsStr) {
	this.conceptVarSubstWordIdsAsStr = conceptVarSubstWordIdsAsStr;
    }

    public void setConceptWordIdsAsStr(String conceptWordIdsAsStr) {
	this.conceptWordIdsAsStr = conceptWordIdsAsStr;
    }

}
