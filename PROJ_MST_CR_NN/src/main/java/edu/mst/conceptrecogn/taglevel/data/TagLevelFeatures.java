/**
 * 
 */
package edu.mst.conceptrecogn.taglevel.data;

import java.util.Comparator;

import org.javatuples.Pair;

import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureCombinationOutputs;
import edu.mst.conceptrecogn.FeatureName;
import edu.mst.conceptrecogn.train.BaseDataRecord;
import edu.mst.db.nlm.corpora.Corpus;

/**
 * Input features for concept recognition multi-layer perceptron (MLP).
 * 
 * @author George
 *
 */
public class TagLevelFeatures extends BaseDataRecord
	implements Comparable<TagLevelFeatures> {

    public static final String pmidTagIdDelim = "-";

    protected int id = 0;
    protected String goldStdPmidTagId = null;
    protected long conceptUid = 0L;
    protected long conceptNameUid = 0L;
    protected String cui = null;
    protected Corpus corpus = null;

    protected double isConceptSingleWord = 0.0d;
    protected double isTextSingleWord = 0.0d;
    protected double fractConceptWordsMatched = 0.0d;
    protected double fractTextWordsMatched = 0.0d;
    protected double isConceptSynonym = 0.0d;
    protected double fractConceptIoc = 0.0d;
    protected double fractTextIoc = 0.0d;
    protected double fractAbstrCogency = 0.0d;
    protected double fractSentCogency = 0.0d;
    protected double isGoodMatch = 0.0d;
    protected double isPoorMatch = 0.0d;

    protected static FeatureCombinationInputs featureComboInputs = null;
    protected static FeatureCombinationOutputs featureComboOutputs = null;
    protected static FeatureName[] orderedFeatureNames = null;
    protected static int nbrFeatures = 0;
    protected static FeatureName[] orderedOutputNames = null;
    protected static int nbrOutputs = 0;
    protected static String[] columnNames = null;
    protected static int nbrOfColumns = 0;

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("goldStdPmidTagId = ").append(goldStdPmidTagId)
		.append(", ");
	builder.append("conceptNameUid = ").append(conceptNameUid).append(", ");
	builder.append("fractConceptWordsMatched = ")
		.append(fractConceptWordsMatched).append(", ");
	builder.append("fractTextWordsMatched = ").append(fractTextWordsMatched)
		.append(", ");
	builder.append("fractConceptIoc = ").append(fractConceptIoc)
		.append(", ");
	builder.append("fractTextIoc = ").append(fractTextIoc).append(", ");
	builder.append("fractAbstrCogency = ").append(fractAbstrCogency)
		.append(", ");
	builder.append("fractSentCogency = ").append(fractSentCogency)
		.append(", ");
	builder.append("isConceptSingleWord = ").append(isConceptSingleWord)
		.append(", ");
	builder.append("isTextSingleWord = ").append(isTextSingleWord)
		.append(", ");
	builder.append("isGoodMatch = ").append(isGoodMatch).append(", ");
	builder.append("isPoorMatch = ").append(isPoorMatch).append(", ");
	return builder.toString();
    }

    public static void setFeatureCombination(FeatureName[] orderedFeatureNames,
	    FeatureCombinationOutputs featureComboOutputs) {
	TagLevelFeatures.orderedFeatureNames = orderedFeatureNames;
	TagLevelFeatures.featureComboOutputs = featureComboOutputs;
	nbrFeatures = orderedFeatureNames.length;
	nbrOutputs = featureComboOutputs.nbrOfFeatures();
	int totalNbrColumns = nbrFeatures + nbrOutputs;
	columnNames = new String[totalNbrColumns];
	int i = 0;
	for (FeatureName inputFeature : orderedFeatureNames) {
	    columnNames[i] = inputFeature.featureName();
	    i++;
	}
	for (FeatureName outputFeature : featureComboOutputs
		.orderedFeatureNames()) {
	    columnNames[i] = outputFeature.featureName();
	    i++;
	}
	nbrOfColumns = columnNames.length;
    }

    public static void setFeatureCombination(
	    FeatureCombinationInputs featureComboInputs,
	    FeatureCombinationOutputs featureComboOutputs) {
	TagLevelFeatures.featureComboInputs = featureComboInputs;
	TagLevelFeatures.featureComboOutputs = featureComboOutputs;
	nbrFeatures = featureComboInputs.nbrOfFeatures();
	nbrOutputs = featureComboOutputs.nbrOfFeatures();
	int totalNbrColumns = nbrFeatures + nbrOutputs;
	columnNames = new String[totalNbrColumns];
	int i = 0;
	for (FeatureName inputFeature : featureComboInputs
		.orderedFeatureNames()) {
	    columnNames[i] = inputFeature.featureName();
	    i++;
	}
	for (FeatureName outputFeature : featureComboOutputs
		.orderedFeatureNames()) {
	    columnNames[i] = outputFeature.featureName();
	    i++;
	}
	nbrOfColumns = columnNames.length;
    }

    public static class Comparators {
	public static final Comparator<TagLevelFeatures> byId = (
		TagLevelFeatures record1, TagLevelFeatures record2) -> Integer
			.compare(record1.id, record2.id);
	public static final Comparator<TagLevelFeatures> byGoldStdPmidTagId = (
		TagLevelFeatures record1,
		TagLevelFeatures record2) -> record1.goldStdPmidTagId
			.compareTo(record2.goldStdPmidTagId);
	public static final Comparator<TagLevelFeatures> byConceptNameUid = (
		TagLevelFeatures record1,
		TagLevelFeatures record2) -> Long.compare(
			record1.conceptNameUid, record2.conceptNameUid);
	public static final Comparator<TagLevelFeatures> byConceptUid = (
		TagLevelFeatures record1, TagLevelFeatures record2) -> Long
			.compare(record1.conceptUid, record2.conceptUid);
	public static final Comparator<TagLevelFeatures> byGoldStdPmidTagId_ConceptNameUid = (
		TagLevelFeatures record1,
		TagLevelFeatures record2) -> byGoldStdPmidTagId
			.thenComparing(byConceptNameUid)
			.compare(record1, record2);
    }

    public int compareTo(TagLevelFeatures record) {
	return Comparators.byId.compare(this, record);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof TagLevelFeatures) {
	    TagLevelFeatures record = (TagLevelFeatures) obj;
	    return id == record.id;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(id);
    }

    public static FeatureCombinationInputs getFeatureCombo() {
	return featureComboInputs;
    }

    public static String[] getColumnNames() {
	return columnNames;
    }

    public static int getNbrOfColumns() {
	return nbrOfColumns;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getGoldStdPmidTagId() {
	return goldStdPmidTagId;
    }

    public Pair<Integer, String> parseGoldStdPmidTagId() {
	String[] parts = goldStdPmidTagId.split(pmidTagIdDelim);
	Integer pmid = Integer.valueOf(parts[0]);
	String tagId = parts[1];
	return new Pair<Integer, String>(pmid, tagId);
    }

    public void setGoldStdPmidTagId(String goldStdPmidTagId) {
	this.goldStdPmidTagId = goldStdPmidTagId;
    }

    public void setGoldStdPmidTagId(int pmid, String tagId) {
	this.goldStdPmidTagId = String.format("%1$d%2$s%3$s", pmid,
		pmidTagIdDelim, tagId);
    }

    public long getConceptUid() {
	return conceptUid;
    }

    public void setConceptUid(long conceptUid) {
	this.conceptUid = conceptUid;
    }

    public long getConceptNameUid() {
	return conceptNameUid;
    }

    public void setConceptNameUid(long conceptNameUid) {
	this.conceptNameUid = conceptNameUid;
    }

    public double getFractConceptWordsMatched() {
	return fractConceptWordsMatched;
    }

    public void setFractConceptWordsMatched(double fractConceptWordsMatched) {
	this.fractConceptWordsMatched = fractConceptWordsMatched;
    }

    public double getFractTextWordsMatched() {
	return fractTextWordsMatched;
    }

    public void setFractTextWordsMatched(double fractTextWordsMatched) {
	this.fractTextWordsMatched = fractTextWordsMatched;
    }

    public void setIsConceptSynonym(double isConceptSynonym) {
	this.isConceptSynonym = isConceptSynonym;
    }

    public double getIsConceptSynonym() {
	return isConceptSynonym;
    }

    public double getFractConceptIoc() {
	return fractConceptIoc;
    }

    public void setFractConceptIoc(double fractConceptIoc) {
	this.fractConceptIoc = fractConceptIoc;
    }

    public double getFractTextIoc() {
	return fractTextIoc;
    }

    public void setFractTextIoc(double fractTextIoc) {
	this.fractTextIoc = fractTextIoc;
    }

    public double isConceptSingleWord() {
	return isConceptSingleWord;
    }

    public void setIsConceptSingleWord(double isConceptSingleWord) {
	this.isConceptSingleWord = isConceptSingleWord;
    }

    public double isTextSingleWord() {
	return isTextSingleWord;
    }

    public void setIsTextSingleWord(double isTextSingleWord) {
	this.isTextSingleWord = isTextSingleWord;
    }

    public double isGoodMatch() {
	return isGoodMatch;
    }

    public void setIsGoodMatch(double isGoodMatch) {
	this.isGoodMatch = isGoodMatch;
    }

    public double isPoorMatch() {
	return isPoorMatch;
    }

    public void setIsPoorMatch(double isPoorMatch) {
	this.isPoorMatch = isPoorMatch;
    }

    public String getCui() {
	return cui;
    }

    public void setCui(String cui) {
	this.cui = cui;
    }

    public Corpus getCorpus() {
	return corpus;
    }

    public void setCorpus(Corpus corpus) {
	this.corpus = corpus;
    }

    public static FeatureName[] getOrderedFeatureNames() {
	return orderedFeatureNames;
    }

    public static int getNbrFeatures() {
	return nbrFeatures;
    }

    public double getFractAbstrCogency() {
	return fractAbstrCogency;
    }

    public void setFractAbstrCogency(double fractAbstrCogency) {
	this.fractAbstrCogency = fractAbstrCogency;
    }

    public double getFractSentCogency() {
	return fractSentCogency;
    }

    public void setFractSentCogency(double fractSentCogency) {
	this.fractSentCogency = fractSentCogency;
    }

}
