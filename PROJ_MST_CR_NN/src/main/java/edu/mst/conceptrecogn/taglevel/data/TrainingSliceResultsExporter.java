/**
 * 
 */
package edu.mst.conceptrecogn.taglevel.data;

import java.io.FileOutputStream;
import java.util.EnumMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.nn.mlp.MlpResultsSlice;
import edu.mst.nn.mlp.data.MlpResultsSliceDao;

/**
 * Purpose is to export training results for each slice as a column.
 * 
 * @author gjs
 *
 */
public class TrainingSliceResultsExporter {

    private static final String csvDirName = "/home/gjs/Documents/MST/PhD/Publications/IOC/Submission3/Resubmission_2020_October/";
    private static final String csvFileName = "mlp_results_byslice_20201003.xlsx";

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    MlpResultsSliceDao sliceDao = new MlpResultsSliceDao();
	    EnumMap<FeatureCombinationInputs, List<MlpResultsSlice>> slicesByFeatureCombo = new EnumMap<FeatureCombinationInputs, List<MlpResultsSlice>>(
		    FeatureCombinationInputs.class);
	    for (FeatureCombinationInputs featureCombo : FeatureCombinationInputs
		    .values()) {
		List<MlpResultsSlice> resultSlices = sliceDao
			.marshalLast(featureCombo, Corpus.ALL, connPool);
		slicesByFeatureCombo.put(featureCombo, resultSlices);
	    }

	    FeatureCombinationInputs[] orderedCombos = new FeatureCombinationInputs[] {
		    FeatureCombinationInputs.BASE,
		    FeatureCombinationInputs.BASE_PLUS_CONCEPT_IOC,
		    FeatureCombinationInputs.BASE_PLUS_TEXT_IOC,
		    FeatureCombinationInputs.BASE_PLUS_IOC,
		    FeatureCombinationInputs.BASE_PLUS_ABSTR_CORPORA_COGENCY,
		    FeatureCombinationInputs.BASE_PLUS_SENT_CORPORA_COGENCY,
		    FeatureCombinationInputs.BASE_PLUS_CORPORA_COGENCY,
		    FeatureCombinationInputs.CONCEPT_IOC_ONLY,
		    FeatureCombinationInputs.TEXT_IOC_ONLY,
		    FeatureCombinationInputs.IOC_ONLY,
		    FeatureCombinationInputs.CORPORA_ABSTR_COGENCY_ONLY,
		    FeatureCombinationInputs.CORPORA_SENT_COGENCY_ONLY,
		    FeatureCombinationInputs.CORPORA_COGENCY_ONLY,
		    FeatureCombinationInputs.CORPORA_COGENCY_PLUS_IOC,
		    FeatureCombinationInputs.ALL };

	    // create workbook and sheets
	    Workbook workbook = new XSSFWorkbook();

	    for (SheetName sheetName : SheetName.values()) {
		createSheet(sheetName, workbook, orderedCombos,
			slicesByFeatureCombo);
	    }

	    // output
	    FileOutputStream fos = new FileOutputStream(
		    csvDirName + csvFileName);
	    workbook.write(fos);
	    fos.close();

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

    private static void createSheet(SheetName sheetName, Workbook workbook,
	    FeatureCombinationInputs[] orderedCombos,
	    EnumMap<FeatureCombinationInputs, List<MlpResultsSlice>> slicesByFeatureCombo) {

	Sheet sheet = workbook.createSheet(sheetName.toString());
	int rowIndex = 0;
	int cellIndex = 0;
	Row headerRow = sheet.createRow(rowIndex++);
	Cell dateTimeHdr = headerRow.createCell(cellIndex++);
	dateTimeHdr.setCellValue("Datetime");
	Cell featureHdr = headerRow.createCell(cellIndex++);
	featureHdr.setCellValue("Feature Combination");
	for (int i = 1; i <= 10; i++) {
	    Cell nextHeaderCell = headerRow.createCell(cellIndex++);
	    nextHeaderCell.setCellValue(i);
	}
	for (FeatureCombinationInputs featureCombo : orderedCombos) {
	    Row newRow = sheet.createRow(rowIndex++);
	    cellIndex = 0;
	    List<MlpResultsSlice> slices = slicesByFeatureCombo
		    .get(featureCombo);
	    Cell dateTimeCell = newRow.createCell(cellIndex++);
	    dateTimeCell.setCellValue(slices.get(0).getDatetime());
	    Cell featureCell = newRow.createCell(cellIndex++);
	    featureCell.setCellValue(featureCombo.toString());
	    for (int i = 0; i < slices.size(); i++) {
		MlpResultsSlice slice = slices.get(i);
		double cellValue = 0.0d;
		switch (sheetName) {
		case PRECISION:
		    cellValue = slice.getPrecision();
		    break;
		case RECALL:
		    cellValue = slice.getRecall();
		    break;
		case F1:
		    cellValue = slice.getF1();
		    break;
		}
		Cell newCell = newRow.createCell(cellIndex++);
		newCell.setCellValue(cellValue);
	    }
	}
    }

    public enum SheetName {
	PRECISION, RECALL, F1;
    }

}
