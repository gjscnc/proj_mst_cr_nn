/**
 * 
 */
/**
 * Components for persisting and accessing training data for concept
 * recognition.
 * 
 * @author gjs
 *
 */
package edu.mst.conceptrecogn.taglevel.data;