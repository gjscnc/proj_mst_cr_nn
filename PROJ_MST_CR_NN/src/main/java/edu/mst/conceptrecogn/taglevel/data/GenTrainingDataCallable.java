/**
 * 
 */
package edu.mst.conceptrecogn.taglevel.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.conceptrecogn.FeatureCalculator;
import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagNameMatch;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagNameMatchDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWord;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWordDao;
import edu.mst.db.ontol.names.ConceptName;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GenTrainingDataCallable
	implements Callable<GenTrainingDataResult> {

    private GoldStdTag tag = null;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool = null;

    protected FeatureCalculator featureCalculator;
    protected TrainingDataDao mlpTrainingDataDao;

    private GoldStdTagNameMatchDao matchDao = new GoldStdTagNameMatchDao();
    private GoldStdTagWordDao tagWordDao = new GoldStdTagWordDao();
    private ConceptNameDao nameDao = new ConceptNameDao();
    private ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
    private LexWordVariantDao lexVariantDao = new LexWordVariantDao();

    public GenTrainingDataCallable(GoldStdTag tag, boolean isPersist,
	    JdbcConnectionPool connPool) {
	this.tag = tag;
	this.isPersist = isPersist;
	featureCalculator = new FeatureCalculator(connPool);
	mlpTrainingDataDao = new TrainingDataDao(connPool);
	this.connPool = connPool;
    }

    @Override
    public GenTrainingDataResult call() throws Exception {

	GenTrainingDataResult result = new GenTrainingDataResult();
	result.pmid = tag.getPmid();
	result.tagId = tag.getTagId();

	Map<Long, GoldStdTagNameMatch> correctMatchByNameUid = new HashMap<Long, GoldStdTagNameMatch>();
	{
	    SortedSet<GoldStdTagNameMatch> matches = matchDao
		    .marshal(tag.getPmid(), tag.getTagId(), connPool);
	    for (GoldStdTagNameMatch match : matches) {
		correctMatchByNameUid.put(match.getConceptNameUid(), match);
	    }
	}

	SortedSet<GoldStdTagWord> tagWords = tagWordDao.marshal(tag.getPmid(),
		tag.getTagId(), connPool);
	Set<Integer> wordIdsProcessed = new HashSet<Integer>();
	Set<Long> candidateNameUids = new HashSet<Long>();
	for (GoldStdTagWord tagWord : tagWords) {
	    int lexWordId = tagWord.getLexWordId();
	    if (wordIdsProcessed.contains(lexWordId)) {
		continue;
	    }
	    candidateNameUids.addAll(
		    nameWordDao.getNameIdsWithWord(lexWordId, connPool));
	    wordIdsProcessed.add(lexWordId);

	    // now get candidates for word synonyms, etc.
	    Set<Integer> variantWordIds = lexVariantDao
		    .marshalVariantIdsForLexWord(lexWordId, connPool);
	    for (int variantWordId : variantWordIds) {
		if (wordIdsProcessed.contains(variantWordId)) {
		    continue;
		}
		SortedSet<Long> newCandidateNameUids = nameWordDao
			.getNameIdsWithWord(variantWordId, connPool);
		candidateNameUids.addAll(newCandidateNameUids);
		wordIdsProcessed.add(variantWordId);
	    }
	}

	// get incorrect candidate names
	boolean isAllCandidatesMatched = candidateNameUids
		.equals(correctMatchByNameUid.keySet());
	Set<Long> trainingSetNameUids = new HashSet<Long>();
	{
	    List<Long> ids = new ArrayList<Long>();
	    ids.addAll(candidateNameUids);
	    Collections.shuffle(ids);
	    int maxPos = ids.size() - 1;
	    int nbrCorrectMatches = correctMatchByNameUid.size();
	    int nbrIncorrectNamesPicked = 0;
	    if (candidateNameUids.size() > 1
		    && isAllCandidatesMatched == false) {
		while (nbrIncorrectNamesPicked < nbrCorrectMatches) {
		    double posAsDbl = maxPos * Math.random();
		    int randomPos = Math.round((float) posAsDbl);
		    long nameUid = ids.get(randomPos);
		    if (correctMatchByNameUid.keySet().contains(nameUid)) {
			continue;
		    }
		    trainingSetNameUids.add(nameUid);
		    nbrIncorrectNamesPicked++;
		}
	    }

	}

	// add correct name UIDs to training set
	trainingSetNameUids.addAll(correctMatchByNameUid.keySet());

	candidateNameUids.addAll(correctMatchByNameUid.keySet());
	List<TrainingRecord> trainingRecords = new ArrayList<TrainingRecord>();
	for (long candidateNameUid : trainingSetNameUids) {
	    ConceptName name = nameDao.marshal(candidateNameUid, connPool);
	    boolean isGoodMatch = correctMatchByNameUid
		    .containsKey(candidateNameUid);
	    TrainingRecord trainingRecord = new TrainingRecord(tag,
		    candidateNameUid, name.getConceptUid(), isGoodMatch,
		    tag.getCorpus(), connPool);
	    featureCalculator.compute(trainingRecord);
	    trainingRecords.add(trainingRecord);
	}

	if (isPersist) {
	    for (TrainingRecord trainingRecord : trainingRecords) {
		try {
		    mlpTrainingDataDao.persist(trainingRecord);
		} catch (Exception ex) {
		    String errMsg = String.format(
			    "Error computing feature values. "
				    + "Gold std tag PMID = %1$d, tag ID = %2$s, text = %3$s, CUI = %4$s, "
				    + "text predicate pos = %5$d, predicate lex base word ID = %6$d, "
				    + "concept predicate pos = %7$d, concept UID = %8$d, concept name UID = %9$d %n",
			    tag.getPmid(), tag.getTagId(), tag.getTaggedText(),
			    tag.getCui(), trainingRecord.getTextPredWordPos(),
			    trainingRecord.getTextPredWordId(),
			    trainingRecord.getConceptPredWordPos(),
			    trainingRecord.getConceptUid(),
			    trainingRecord.getConceptNameUid());
		    throw new Exception(errMsg, ex);
		}
	    }
	}

	result.nbrTrainingRecordsGenerated = trainingRecords.size();

	return result;
    }

}
