package edu.mst.conceptrecogn.taglevel.data;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class GenTrainingDataWorker extends BaseWorker<GenTrainingDataResult> {

    private boolean isPersist = false;
    private boolean isDeleteExisting = false;

    private GoldStdTag[] tags = null;
    private int nbrTags = 0;
    private int nbrTagsProcessed = 0;
    private int nbrTrainingRecordsGenerated = 0;
    private int rptInterval = 24;

    private SortedMap<Integer, SortedSet<String>> remainingByPmid = new TreeMap<Integer, SortedSet<String>>();

    public GenTrainingDataWorker(boolean isPersist, boolean isDeleteExisting,
	    int threadCallablesBatchSize, JdbcConnectionPool connPool)
	    throws Exception {
	super(threadCallablesBatchSize, connPool);
	this.isPersist = isPersist;
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if (isDeleteExisting) {
	    System.out.println("Deleting existing training data");
	    TrainingDataDao.deleteAll(connPool);
	}

	{
	    System.out.println("Marshaling gold standard annotation tags");
	    GoldStdTagDao tagDao = new GoldStdTagDao();
	    SortedSet<GoldStdTag> tagSet = tagDao.marshalCuiNotNull(connPool);
	    nbrTags = tagSet.size();
	    tags = new GoldStdTag[nbrTags];
	    int pos = 0;
	    for (GoldStdTag tag : tagSet) {
		tags[pos] = tag;
		pos++;
	    }
	    System.out.printf(
		    "Marshaled %1$,d gold standard annotation tags. %n",
		    nbrTags);
	}

	System.out.println("Now generating training records");

	while (currentPos < nbrTags) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		GoldStdTag tag = tags[i];
		GenTrainingDataCallable callable = new GenTrainingDataCallable(
			tag, isPersist, connPool);
		svc.submit(callable);
		if (!remainingByPmid.containsKey(tag.getPmid())) {
		    remainingByPmid.put(tag.getPmid(), new TreeSet<String>());
		}
		remainingByPmid.get(tag.getPmid()).add(tag.getTagId());
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		GenTrainingDataResult result = svc.take().get();
		nbrTrainingRecordsGenerated += result.nbrTrainingRecordsGenerated;
		nbrTagsProcessed++;
		if (nbrTagsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d gold standard tags, "
				    + "generating %2$,d training records. %n",
			    nbrTagsProcessed, nbrTrainingRecordsGenerated);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d gold standard tags, "
			+ "generating %2$,d training records. %n",
		nbrTagsProcessed, nbrTrainingRecordsGenerated);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrTags - 1)
	    maxPos = nbrTags - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isPersist = true;
	    boolean isDeleteExisting = true;
	    int threadsCallableBatchSize = 24;

	    try (GenTrainingDataWorker worker = new GenTrainingDataWorker(
		    isPersist, isDeleteExisting, threadsCallableBatchSize,
		    connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}
