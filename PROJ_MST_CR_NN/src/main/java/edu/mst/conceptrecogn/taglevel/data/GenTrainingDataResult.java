package edu.mst.conceptrecogn.taglevel.data;

public class GenTrainingDataResult {
    
    public int pmid = 0;
    public String tagId = null;
    public int nbrTrainingRecordsGenerated = 0;

}
