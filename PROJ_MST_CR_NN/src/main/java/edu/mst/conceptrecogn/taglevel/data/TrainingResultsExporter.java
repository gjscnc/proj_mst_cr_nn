/**
 * 
 */
package edu.mst.conceptrecogn.taglevel.data;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.nn.mlp.MlpResults;
import edu.mst.nn.mlp.MlpResultsSlice;
import edu.mst.nn.mlp.data.MlpResultsDao;
import edu.mst.nn.mlp.data.MlpResultsSliceDao;

/**
 * Purpose is to export training results in format that enumerates all feature
 * combinations.
 * 
 * @author gjs
 *
 */
public class TrainingResultsExporter {

    private static final String csvDirName = "/home/gjs/Documents/MST/PhD/Publications/IOC/Submission3/Resubmission_2020_October/";
    private static final String csvFileName = "mlp_results_20201013.xlsx";

    private JdbcConnectionPool connPool;
    private MlpResultsDao resultsDao = new MlpResultsDao();
    private MlpResultsSliceDao sliceDao = new MlpResultsSliceDao();

    public TrainingResultsExporter(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public void exportAllResults() throws SQLException, Exception {

	List<MlpResults> results = resultsDao.marshal(connPool);
	results.sort(MlpResults.Comparators.byFeatureCombo);
	List<MlpResultsSlice> allSlices = sliceDao.marshal(connPool);
	for (MlpResults result : results) {
	    List<MlpResultsSlice> slices = new ArrayList<MlpResultsSlice>();
	    for (MlpResultsSlice slice : allSlices) {
		if (slice.getFeatureCombo().equals(result.getFeatureCombo())) {
		    slices.add(slice);
		}
	    }
	    slices.sort(MlpResultsSlice.Comparators.byFeatureCombo_Slice);
	    result.setSlices(slices);
	}

	try (Workbook workbook = new XSSFWorkbook()) {

	    {

		Sheet resultsSheet = workbook.createSheet("RESULTS");

		int rowNbr = 0;
		{
		    Row resultsHdrRow = resultsSheet.createRow(rowNbr++);
		    int resultsCellNbr = 0;
		    Cell blankHdrCell = resultsHdrRow
			    .createCell(resultsCellNbr++);
		    blankHdrCell.setCellValue("");
		    Cell precHdrCell = resultsHdrRow
			    .createCell(resultsCellNbr++);
		    precHdrCell.setCellValue("P");
		    Cell recallHdrcell = resultsHdrRow
			    .createCell(resultsCellNbr++);
		    recallHdrcell.setCellValue("R");
		    Cell f1HdrCell = resultsHdrRow.createCell(resultsCellNbr++);
		    f1HdrCell.setCellValue("F1");
		}

		for (MlpResults result : results) {
		    int cellNbr = 0;
		    Row row = resultsSheet.createRow(rowNbr++);
		    Cell featureCell = row.createCell(cellNbr++);
		    featureCell.setCellValue(result.getFeatureCombo());
		    Cell precCell = row.createCell(cellNbr++);
		    precCell.setCellValue(result.getPrecision());
		    Cell recallCell = row.createCell(cellNbr++);
		    recallCell.setCellValue(result.getRecall());
		    Cell f1Cell = row.createCell(cellNbr++);
		    f1Cell.setCellValue(result.getF1());
		}

	    }

	    {

		for (SheetName sheetName : SheetName.values()) {

		    Sheet slicesSheet = workbook
			    .createSheet(sheetName.toString());

		    int rowNbr = 0;
		    {
			Row sliceHdrRow = slicesSheet.createRow(rowNbr++);
			int cellNbr = 0;
			Cell blankHdrCell = sliceHdrRow.createCell(cellNbr++);
			blankHdrCell.setCellValue("");
			for (int i = 1; i <= 10; i++) {
			    Cell cell = sliceHdrRow.createCell(cellNbr++);
			    cell.setCellValue(String.valueOf(i));
			}
		    }
		    {
			for (MlpResults result : results) {
			    Row row = slicesSheet.createRow(rowNbr++);
			    Cell featureComboCell = row.createCell(0);
			    featureComboCell
				    .setCellValue(result.getFeatureCombo());
			    for (MlpResultsSlice slice : result.getSlices()) {
				Cell cell = row
					.createCell(slice.getSliceNbr() + 1);
				switch (sheetName) {
				case PRECISION:
				    cell.setCellValue(slice.getPrecision());
				    break;
				case RECALL:
				    cell.setCellValue(slice.getRecall());
				    break;
				case F1:
				    cell.setCellValue(slice.getF1());
				    break;
				}
			    }
			}
		    }
		}

	    }

	    FileOutputStream fos = new FileOutputStream(
		    csvDirName + csvFileName);
	    workbook.write(fos);

	}

    }

    public void exportFeatures() throws IOException, SQLException {

	MlpResultsSliceDao sliceDao = new MlpResultsSliceDao();
	EnumMap<FeatureCombinationInputs, List<MlpResultsSlice>> slicesByFeatureCombo = new EnumMap<FeatureCombinationInputs, List<MlpResultsSlice>>(
		FeatureCombinationInputs.class);
	for (FeatureCombinationInputs featureCombo : FeatureCombinationInputs.values()) {
	    List<MlpResultsSlice> resultSlices = sliceDao
		    .marshalLast(featureCombo, Corpus.ALL, connPool);
	    slicesByFeatureCombo.put(featureCombo, resultSlices);
	}

	FeatureCombinationInputs[] orderedCombos = new FeatureCombinationInputs[] {
		FeatureCombinationInputs.BASE,
		FeatureCombinationInputs.BASE_PLUS_CONCEPT_IOC,
		FeatureCombinationInputs.BASE_PLUS_TEXT_IOC,
		FeatureCombinationInputs.BASE_PLUS_IOC,
		FeatureCombinationInputs.BASE_PLUS_ABSTR_CORPORA_COGENCY,
		FeatureCombinationInputs.BASE_PLUS_SENT_CORPORA_COGENCY,
		FeatureCombinationInputs.BASE_PLUS_CORPORA_COGENCY,
		FeatureCombinationInputs.CONCEPT_IOC_ONLY,
		FeatureCombinationInputs.TEXT_IOC_ONLY, FeatureCombinationInputs.IOC_ONLY,
		FeatureCombinationInputs.CORPORA_ABSTR_COGENCY_ONLY,
		FeatureCombinationInputs.CORPORA_SENT_COGENCY_ONLY,
		FeatureCombinationInputs.CORPORA_COGENCY_ONLY,
		FeatureCombinationInputs.CORPORA_COGENCY_PLUS_IOC,
		FeatureCombinationInputs.ALL };

	// create workbook and sheets
	Workbook workbook = new XSSFWorkbook();

	for (SheetName sheetName : SheetName.values()) {
	    createSheet(sheetName, workbook, orderedCombos,
		    slicesByFeatureCombo);
	}

	// output
	FileOutputStream fos = new FileOutputStream(csvDirName + csvFileName);
	workbook.write(fos);
	fos.close();

    }

    private static void createSheet(SheetName sheetName, Workbook workbook,
	    FeatureCombinationInputs[] orderedCombos,
	    EnumMap<FeatureCombinationInputs, List<MlpResultsSlice>> slicesByFeatureCombo) {

	Sheet sheet = workbook.createSheet(sheetName.toString());
	int rowIndex = 0;
	int cellIndex = 0;
	Row headerRow = sheet.createRow(rowIndex++);
	Cell dateTimeHdr = headerRow.createCell(cellIndex++);
	dateTimeHdr.setCellValue("Datetime");
	Cell featureHdr = headerRow.createCell(cellIndex++);
	featureHdr.setCellValue("Feature Combination");
	for (int i = 1; i <= 10; i++) {
	    Cell nextHeaderCell = headerRow.createCell(cellIndex++);
	    nextHeaderCell.setCellValue(i);
	}
	for (FeatureCombinationInputs featureCombo : orderedCombos) {
	    Row newRow = sheet.createRow(rowIndex++);
	    cellIndex = 0;
	    List<MlpResultsSlice> slices = slicesByFeatureCombo
		    .get(featureCombo);
	    Cell dateTimeCell = newRow.createCell(cellIndex++);
	    dateTimeCell.setCellValue(slices.get(0).getDatetime());
	    Cell featureCell = newRow.createCell(cellIndex++);
	    featureCell.setCellValue(featureCombo.toString());
	    for (int i = 0; i < slices.size(); i++) {
		MlpResultsSlice slice = slices.get(i);
		double cellValue = 0.0d;
		switch (sheetName) {
		case PRECISION:
		    cellValue = slice.getPrecision();
		    break;
		case RECALL:
		    cellValue = slice.getRecall();
		    break;
		case F1:
		    cellValue = slice.getF1();
		    break;
		}
		Cell newCell = newRow.createCell(cellIndex++);
		newCell.setCellValue(cellValue);
	    }
	}
    }

    public enum SheetName {
	PRECISION, RECALL, F1;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    TrainingResultsExporter exporter = new TrainingResultsExporter(
		    connPool);
	    exporter.exportAllResults();

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}
