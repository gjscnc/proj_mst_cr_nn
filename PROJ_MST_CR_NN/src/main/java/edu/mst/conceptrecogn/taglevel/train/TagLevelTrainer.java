/**
 * 
 */
package edu.mst.conceptrecogn.taglevel.train;

import java.util.List;
import java.util.SortedMap;

import edu.mst.conceptrecogn.ConceptRecognMlp;
import edu.mst.conceptrecogn.FeatureCombinationInputs;
import edu.mst.conceptrecogn.FeatureCombinationOutputs;
import edu.mst.conceptrecogn.FeatureName;
import edu.mst.conceptrecogn.RecognitionLevel;
import edu.mst.conceptrecogn.train.FeatureAllCombosEnum;
import edu.mst.conceptrecogn.train.MlpKFoldData;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Class with method for training the concept recognizer using the 'tag-level'
 * gold standard tokens.
 * <p>
 * This means that only tokens between the beginning and ending position of the
 * tags in the text are used for training.
 * 
 * @author gjs
 *
 */
public class TagLevelTrainer {

    private Corpus corpus = null;
    private ConceptRecognMlp mlp = null;
    private JdbcConnectionPool connPool = null;
    private FeatureName[] orderedFeatureComboInputs = null;
    private FeatureCombinationInputs featureComboInputs = null;
    private FeatureCombinationOutputs featureComboOutputs = null;
    private int nbrCuiSlices = 0;

    public TagLevelTrainer(ConceptRecognMlp mlp, Corpus corpus,
	    FeatureName[] orderedFeatureNames,
	    FeatureCombinationOutputs featureComboOutputs, int nbrCuiSlices,
	    JdbcConnectionPool connPool) {
	this.mlp = mlp;
	this.corpus = corpus;
	this.orderedFeatureComboInputs = orderedFeatureNames;
	this.featureComboOutputs = featureComboOutputs;
	this.nbrCuiSlices = nbrCuiSlices;
	this.connPool = connPool;
    }

    public TagLevelTrainer(ConceptRecognMlp mlp, Corpus corpus,
	    FeatureCombinationInputs featureComboInputs,
	    FeatureCombinationOutputs featureComboOutputs, int nbrCuiSlices,
	    JdbcConnectionPool connPool) {
	this.mlp = mlp;
	this.corpus = corpus;
	this.featureComboInputs = featureComboInputs;
	this.featureComboOutputs = featureComboOutputs;
	this.nbrCuiSlices = nbrCuiSlices;
	this.connPool = connPool;
    }

    public TagLevelTrainer(ConceptRecognMlp mlp, Corpus corpus,
	    int nbrCuiSlices, FeatureCombinationOutputs featureComboOutputs,
	    JdbcConnectionPool connPool) {
	this.mlp = mlp;
	this.corpus = corpus;
	this.nbrCuiSlices = nbrCuiSlices;
	this.connPool = connPool;
	orderedFeatureComboInputs = FeatureCombinationInputs.ALL
		.orderedFeatureNames();
	this.featureComboOutputs = featureComboOutputs;
    }

    public void train(boolean isUseEarlyStopping, boolean isPersistWgts, boolean isPersistInstances)
	    throws Exception {

	MlpKFoldData kFoldData = null;
	if (featureComboInputs != null) {
	    kFoldData = new MlpKFoldData(mlp.getBatchSize(), nbrCuiSlices,
		    corpus, featureComboInputs, featureComboOutputs, connPool);
	} else if (orderedFeatureComboInputs != null) {
	    kFoldData = new MlpKFoldData(mlp.getBatchSize(), nbrCuiSlices,
		    corpus, orderedFeatureComboInputs, featureComboOutputs, connPool);
	} else {
	    throw new Exception("Missing feature names");
	}
	kFoldData.marshalNnTrainingData();
	mlp.kFoldTraining(kFoldData, isUseEarlyStopping, isPersistInstances);
	mlp.persistResults(connPool);
	mlp.printWeights();
	if (isPersistWgts) {
	    mlp.persistWeights(connPool);
	}

    }

    public static void trainAllFeatureCombos(boolean isUseEarlyStopping,
	    boolean isPersistWgts, Corpus corpus, int nbrCuiSlices,
	    boolean isOutputSingleNeuron,
	    FeatureCombinationOutputs featureComboOutputs, boolean isPersistInstances,
	    JdbcConnectionPool connPool) throws Exception {

	FeatureAllCombosEnum combosEnum = new FeatureAllCombosEnum();
	List<int[]> allCombos = combosEnum.allCombinations();
	int nbrCombos = allCombos.size() - 2;
	System.out.printf(
		"%n===============================================================%n");
	System.out.printf(
		"Training neural network for %1$,d feature combinations. %n",
		nbrCombos);
	System.out.printf(
		"===============================================================%n%n");

	SortedMap<Integer, FeatureName> inputFeatureByNbr = FeatureName
		.inputFeatureByNbr();

	int combinationNbr = 1;
	for (int[] combination : allCombos) {

	    FeatureName[] orderedFeatureNames = new FeatureName[combination.length];
	    for (int i = 0; i < combination.length; i++) {
		int featureNbr = combination[i];
		FeatureName name = inputFeatureByNbr.get(featureNbr);
		orderedFeatureNames[i] = name;
	    }

	    if (orderedFeatureNames.length == 1) {
		FeatureName name = orderedFeatureNames[0];
		if (name.equals(FeatureName.IS_CONCEPT_SINGLE_WORD)
			|| name.equals(FeatureName.IS_TEXT_SINGLE_WORD)) {
		    continue;
		}
	    }

	    System.out.printf(
		    "%n===============================================================%n");
	    System.out.printf(
		    "Training for feature combination = [%1$s], nbr %2$,d of %3$,d possible %n",
		    FeatureName.orderedFeatureComboToStr(orderedFeatureNames),
		    combinationNbr, nbrCombos);
	    for (FeatureName name : orderedFeatureNames) {
		System.out.printf("    %1$s %n", name.toString());
	    }
	    System.out.printf(
		    "===============================================================%n%n");
	    ConceptRecognMlp mlp = new ConceptRecognMlp(orderedFeatureNames,
		    featureComboOutputs, RecognitionLevel.ANNOTATION_TAG);
	    mlp.init();

	    TagLevelTrainer trainer = new TagLevelTrainer(mlp, corpus,
		    orderedFeatureNames, featureComboOutputs, nbrCuiSlices,
		    connPool);
	    trainer.train(isUseEarlyStopping, isPersistWgts, isPersistInstances);

	    combinationNbr++;

	}

    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    Corpus[] allCorpus = Corpus.values();
	    int nbrCuiSlices = 10;
	    boolean isTrainForEachCorpus = false;
	    boolean isTrainForFeatureComparisons = false;
	    boolean isUseEarlyStopping = true;
	    boolean isPersistWgts = true;
	    boolean isTrainAllFeatureCombos = false;
	    boolean isOutputSingleNeuron = true;
	    boolean isPersistInstances = true;
	    FeatureCombinationOutputs featureComboOutputs = isOutputSingleNeuron
		    ? FeatureCombinationOutputs.ONE
		    : FeatureCombinationOutputs.TWO;
	    if (isTrainAllFeatureCombos) {
		trainAllFeatureCombos(isUseEarlyStopping, isPersistWgts,
			Corpus.ALL, nbrCuiSlices, isOutputSingleNeuron,
			featureComboOutputs, isPersistInstances, connPool);
		return;
	    }
	    if (isTrainForEachCorpus) {
		for (int i = 0; i < allCorpus.length; i++) {
		    Corpus corpus = allCorpus[i];
		    if (isTrainForFeatureComparisons) {
			FeatureCombinationInputs[] featureCombos = FeatureCombinationInputs
				.values();
			for (FeatureCombinationInputs featureComboInputs : featureCombos) {
			    ConceptRecognMlp mlp = new ConceptRecognMlp(
				    featureComboInputs, featureComboOutputs,
				    RecognitionLevel.ANNOTATION_TAG);
			    mlp.init();
			    TagLevelTrainer trainer = new TagLevelTrainer(mlp,
				    corpus, featureComboInputs,
				    featureComboOutputs, nbrCuiSlices,
				    connPool);
			    trainer.train(isUseEarlyStopping, isPersistWgts, isPersistInstances);
			}
		    } else {
			FeatureCombinationInputs featureComboInputs = FeatureCombinationInputs.ALL;
			ConceptRecognMlp mlp = new ConceptRecognMlp(
				featureComboInputs, featureComboOutputs,
				RecognitionLevel.ANNOTATION_TAG);
			mlp.init();
			TagLevelTrainer trainer = new TagLevelTrainer(mlp,
				corpus, featureComboInputs, featureComboOutputs,
				nbrCuiSlices, connPool);
			trainer.train(isUseEarlyStopping, isPersistWgts, isPersistInstances);
		    }
		}
	    } else {
		Corpus corpus = Corpus.ALL;
		if (isTrainForFeatureComparisons) {
		    FeatureCombinationInputs[] featureCombos = FeatureCombinationInputs
			    .values();
		    for (FeatureCombinationInputs featureComboInputs : featureCombos) {
			System.out.printf(
				"===============================================================%n");
			System.out.printf(
				"Training for feature combination: %1$s %n",
				featureComboInputs.toString());
			System.out.printf(
				"===============================================================%n%n");
			ConceptRecognMlp mlp = new ConceptRecognMlp(
				featureComboInputs, featureComboOutputs,
				RecognitionLevel.ANNOTATION_TAG);
			mlp.init();
			TagLevelTrainer trainer = new TagLevelTrainer(mlp,
				corpus, featureComboInputs, featureComboOutputs,
				nbrCuiSlices, connPool);
			trainer.train(isUseEarlyStopping, isPersistWgts, isPersistInstances);
		    }
		} else {
		    FeatureCombinationInputs featureComboInputs = FeatureCombinationInputs.CORPORA_COGENCY_PLUS_IOC;
		    System.out.printf(
			    "===============================================================%n");
		    System.out.printf(
			    "Training for feature combination: %1$s %n",
			    featureComboInputs.toString());
		    System.out.printf(
			    "===============================================================%n%n");
		    ConceptRecognMlp mlp = new ConceptRecognMlp(
			    featureComboInputs, featureComboOutputs,
			    RecognitionLevel.ANNOTATION_TAG);
		    mlp.init();
		    TagLevelTrainer trainer = new TagLevelTrainer(mlp, corpus,
			    featureComboInputs, featureComboOutputs,
			    nbrCuiSlices, connPool);
		    trainer.train(isUseEarlyStopping, isPersistWgts, isPersistInstances);
		}
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}
