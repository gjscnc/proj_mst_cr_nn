/**
 * 
 */
package edu.mst.conceptrecogn;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.ontol.names.ConceptName;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Methods for marshaling candidate concepts.
 * 
 * @author gjs
 *
 */
public class CandidateNameDao {

    private ConceptNameDao nameDao = new ConceptNameDao();
    private ConceptNameWordDao wordDao = new ConceptNameWordDao();

    public SortedSet<CandidateName> getCandidates(int predicateWordId,
	    CandidateNameType candidateType, JdbcConnectionPool connPool)
	    throws SQLException {

	SortedSet<CandidateName> candidates = new TreeSet<CandidateName>();

	SortedSet<Long> namesWithWord = wordDao
		.getNameIdsWithWord(predicateWordId, connPool);
	Iterator<Long> namesUidIter = namesWithWord.iterator();
	while (namesUidIter.hasNext()) {
	    int conceptNameUid = namesUidIter.next().intValue();
	    ConceptName name = nameDao.marshal(conceptNameUid, connPool);
	    long conceptUid = name.getConceptUid();
	    CandidateName candidate = new CandidateName(conceptNameUid, conceptUid, candidateType);
	    candidate.getWords().addAll(wordDao.marshalWordsForName(conceptNameUid, connPool));
	}
	return candidates;
    }

}
